<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'ru'}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="lang"/>

<html>
<head>
<title><fmt:message key="airline"/></title>
<meta charset="UTF-8">
<link rel="icon" href="images/travel.png" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/adminApplicationss.css">

<style>
	.colorText {
		color: #ffe647;
		font-weight: bold;
		font-size: 15px;
	}
</style>
</head>
<body>

	<c:if test="${sessionScope.user.getLogin() != 'admin' }">
		<script type="text/javascript">
			document.location.replace("index.jsp");
		</script>
	</c:if>
	
	<!-- HEADER -->
	<header class="header">
		<div class="container">
			<div class="image-language">
				<form method="POST">
					<input name="language" type="image" value="ru" src="images\rus.png" ${language == 'ru' ? 'selected' : ''}>
					<input name="language" type="image" value="en" src="images\en.png" ${language == 'en' ? 'selected' : ''}>
				</form>
			</div>
			<h1><fmt:message key="airline2"/></h1>
		</div>
	</header>
	<!-- /HEADER -->
	<!-- NAVIGATION -->
	<nav class="page-navigation">
		<div class="container">
			<img src="images\plane.png">
			<ul>
				<li><a href="admin.jsp"><fmt:message key="home"/></a></li>
			</ul>
			<div class="image-admin">
				<form action="ControllerServlet" method="POST">
					<input type="hidden" name="command" value="exit" />
					<input type="image" src="images\exit.png">
				</form>
			</div>
			<div class="account">
				<input type="text" name="account" class="colorText" value="${sessionScope.user.getLogin() }" readonly>
			</div>
			<div class="image-admin2">
				<img src="images\admin.png">
			</div>
		</div>
	</nav>
	<!-- /NAVIGATION -->
	<!-- MAIN SECTION -->
	<main>

	<div class="container">
		<h2 class="collection-title">
			<fmt:message key="title1"/>
		</h2>
	</div>
	<div class="container">
		<c:if test="${sessionScope.Error != null }">
			<font color="red">${sessionScope.Error }</font>
			<c:remove scope="session" var="Error" />
		</c:if>
		<c:if test="${sessionScope.noError != null }">
			<font color="green">${sessionScope.noError }</font>
			<c:remove scope="session" var="noError" />
		</c:if>
		<br>
		<br>
		<table id="myTable">
			<tr>
				<th>№ <fmt:message key="application"/></th>
				<th><fmt:message key="dispatcher"/></th>
				<th><fmt:message key="date"/></th>
				<th><fmt:message key="application2"/></th>
				<th><fmt:message key="status"/></th>
			</tr>
			<c:forEach var="current" items="${sessionScope.app}">
			<tr>
				<td>${current.id}</td>
				<td>${current.loginDisp}</td>
				<td>${current.sendDate}</td>
				<td>${current.text}</td>
				<td>${current.status}</td>
			</tr>
			</c:forEach>
		</table>
		
		<form action="ControllerServlet" method="POST" class="parameters">
			<input type="hidden" name="command" value="changeStatus" /> <br>
			<input type="text" name="appId" id="appId" maxlength="4" placeholder="№ <fmt:message key="application"/>" required>
			<select id="status" name="status">
				<option value="Выполнена"><fmt:message key="done"/></option>
				<option value="Отклонена"><fmt:message key="rejected"/></option>
			</select>
			<input type="submit" name="submitButton" value="<fmt:message key="change"/>">
		</form>
	</div>
	</main>
	<!-- /MAIN SECTION -->
	<!-- fOOTER -->
	<footer class="footer">
		<div class="container">
			<p align="center">Kate Yankovska © 2018. All rights reserved</p>
		</div>
	</footer>
	<!-- /fOOTER -->
</body>
</html>