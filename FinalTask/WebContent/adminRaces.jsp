<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'ru'}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="lang"/>

<html>
<head>
<title><fmt:message key="airline"/></title>
<meta charset="UTF-8">
<link rel="icon" href="images/travel.png" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/adminsRacesssss.css">
<link rel="stylesheet" type="text/css" href="css/radiobtnAdminRaces.css">

<style>
	.colorText {
		color: #ffe647;
		font-weight: bold;
		font-size: 15px;
	}
</style>
</head>
<body>
	
	<c:if test="${sessionScope.user.getLogin() != 'admin' }">
		<script type="text/javascript">
			document.location.replace("index.jsp");
		</script>
	</c:if>
	
	<!-- HEADER -->
	<header class="header">
		<div class="container">
			<div class="image-language">
				<form method="POST">
					<input name="language" type="image" value="ru" src="images\rus.png" ${language == 'ru' ? 'selected' : ''}>
					<input name="language" type="image" value="en" src="images\en.png" ${language == 'en' ? 'selected' : ''}>
				</form>
			</div>
			<h1><fmt:message key="airline2"/></h1>
		</div>
	</header>
	<!-- /HEADER -->
	<!-- NAVIGATION -->
	<nav class="page-navigation">
		<div class="container">
			<img src="images\plane.png">
			<ul>
				<li><a href="admin.jsp"><fmt:message key="home"/></a></li>
			</ul>
			<div class="image-admin">
				<form action="ControllerServlet" method="POST">
					<input type="hidden" name="command" value="exit" />
					<input type="image" src="images\exit.png">
				</form>
			</div>
			<div class="account">
				<input type="text" name="account" class="colorText" value="${sessionScope.user.getLogin() }" readonly>
			</div>
			<div class="image-admin2">
				<img src="images\admin.png">
			</div>
		</div>
	</nav>
	<!-- /NAVIGATION -->
	<!-- MAIN SECTION -->
	<main>

	<div class="container">
		<h2 class="collection-title">
			<fmt:message key="adminbutton1"/> <small><fmt:message key="addition"/>, <fmt:message key="deleting"/>,
				<fmt:message key="editing"/></small>
		</h2>
		
		<c:if test="${sessionScope.Error != null }">
			<font color="red">${sessionScope.Error }</font>
			<c:remove scope="session" var="Error" />
		</c:if>
		<c:if test="${sessionScope.noError != null }">
			<font color="green">${sessionScope.noError }</font>
			<c:remove scope="session" var="noError" />
		</c:if>
		<c:if test="${sessionScope.inputError != null }">
			<font color="red">${sessionScope.inputError }</font>
			<c:remove scope="session" var="inputError" />
		</c:if>
		<br>
		<br>
	</div>

	<!--                     ПРОСМОТР РЕЙСОВ                          -->

	<div class="container">
		<label class="containerCheckBox"><fmt:message key="direction"/><input
			type="radio" name="option" value="Я знаю направление" id="radioKnow">
			<span class="checkmark"></span>
		</label> 
		<label class="containerCheckBox"><fmt:message key="flightNum"/> <input
			type="radio" name="option" value="Я знаю номер рейса"
			id="radioDontKnow"> <span class="checkmark"></span>
		</label>

		<form action="ControllerServlet" method="GET" class="parameters"
			id="formFromToDate">
			<input type="hidden" name="command" value="inputFlight" /> <br>
			<div class="autocomplete" style="width:310px;">
    			<input id="fromPlace" type="text" name="from" maxlength="40" placeholder="<fmt:message key="inputFrom"/>" autocomplete="off">	
 				</div>
 			<div class="autocomplete" style="width:300px;">
 				<input id="toPlace" type="text" name="to" maxlength="40" placeholder="<fmt:message key="inputTo"/>" autocomplete="off">
			</div>
			<input type="date" name="date1" required> <input
				type="submit" name="submitButton1" value="<fmt:message key="find"/>">
		</form>
		<form action="ControllerServlet" method="GET" class="parameters"
			id="formRace">
			<input type="hidden" name="command" value="inputFlightNumber" /> <br>
			<input type="text" name="race" id="raceNum" maxlength="6" placeholder="№ <fmt:message key="flight"/>" pattern="^[ 0-9]+$"
				required style="width:300px;">
			<input type="submit" name="submitButton2" value="<fmt:message key="find"/>">
		</form>
		<br>
		<br>
		<table id="myTable">
			<tr>
				<th><fmt:message key="flightNum2"/></th>
				<th><fmt:message key="from"/></th>
				<th><fmt:message key="to"/></th>
				<th><fmt:message key="dateDeparture"/></th>
				<th><fmt:message key="dateArrival"/></th>
				<th><fmt:message key="nameFlight"/></th>
				<th><fmt:message key="flightStatus"/></th>
			</tr>
			<c:forEach var="current" items="${flights}">
				<tr>
					<td>${current.id}</td>
					<td>${current.fromCity}, ${current.fromCountry}</td>
					<td>${current.toCity}, ${current.toCountry}</td>
					<td>${current.flightDate}</td>
					<td>${current.flightArrival}</td>
					<td>${current.raceName}</td>
					<td>${current.status}</td>
				</tr>
			</c:forEach>
		</table>

		<label class="containerCheckBox3"><fmt:message key="addFlight"/> <input
			type="radio" name="optionParam" value="Добавить рейс" id="addradio1">
			<span class="checkmark3"></span>
		</label> 
		<label class="containerCheckBox3"><fmt:message key="deleteFlight"/> <input
			type="radio" name="optionParam" value="Удалить рейс" id="deleteradio1">
			<span class="checkmark3"></span>
		</label> 
		<label class="containerCheckBox3"><fmt:message key="editFlight"/> <input
			type="radio" name="optionParam" value="Редактировать рейс"
			id="updateradio1"> <span class="checkmark3"></span>
		</label>

		<!--                     ДОБАВЛЕНИЕ РЕЙСА                          -->

		<form action="ControllerServlet" method="POST" class="paramUpdate" id="add1">
			<input type="hidden" name="command" value="addFlight" /> <br>
			<h4><fmt:message key="text1"/></h4>
			<input type="text" name="fromCity2" id="fromCity2" maxlength="30" placeholder="<fmt:message key="fromCity"/>" pattern="^[A-Za-zА-Яа-яЁё\s]+$" required> 
			<input type="text" name="fromCountry2" id="fromCountry2" maxlength="30" placeholder="<fmt:message key="fromCountry"/>" pattern="^[A-Za-zА-Яа-яЁё\s]+$" required> 
			<br>
			<br>
			<input type="text" name="toCity2" id="toCity2" maxlength="30" placeholder="<fmt:message key="toCity"/>" pattern="^[A-Za-zА-Яа-яЁё\s]+$" required> 
			<input type="text" name="toCountry2" id="toCountry2" maxlength="30" placeholder="<fmt:message key="toCountry"/>" pattern="^[A-Za-zА-Яа-яЁё\s]+$" required> 
			<br>
			<br>
			<pre><span style="font-size: 17px">   <fmt:message key="text2"/>      <fmt:message key="text3"/></span></pre>
			<input type="datetime-local" name="date2" id="date2" required>
			<input type="datetime-local" name="arrival" id="arrival" required>
			<br>
			<pre><span style="font-size: 17px">  <fmt:message key="text4"/></span></pre>
			<select name="dispatcher" size=4 required>
				<c:forEach var="current" items="${sessionScope.form2}">
					<option>${current.login}</option>
				</c:forEach>
			</select>
			<pre>     <input type="submit" name="submitButton2" id="submitButton2" value="<fmt:message key="submit"/>"></pre>
		</form>	

		<!--                     УДАЛЕНИЕ РЕЙСА                          -->

		<form action="ControllerServlet" method="POST" class="paramUpdate" id="delete1">
			<input type="hidden" name="command" value="deleteFlight" /> <br>
			<h4><fmt:message key="text5"/></h4>
			<input type="text" name="race2" id="race2" maxlength="6" placeholder="№ <fmt:message key="flight"/>" pattern="^[ 0-9]+$"
				required> <input type="submit" name="submitButton3"
				id="submitButton2" value="<fmt:message key="submit"/>">
		</form>

		<!--                     ИЗМЕНЕНИЕ РЕЙСА                        -->

		<form action="ControllerServlet" method="POST" class="paramUpdate" id="change1">
			<input type="hidden" name="command" value="updateFlight" /> <br>
			<h3 class="collection-title-h3"><fmt:message key="text6"/></h3>
			<p><fmt:message key="text7"/><p>
				<input type="text" name="race3" id="race3" maxlength="6" placeholder="№ <fmt:message key="flight"/>" pattern="^[ 0-9]+$" required>
				<input type="datetime-local" name="date3" id="date3" required>
				<input type="datetime-local" name="date4" id="date4" required>
				<input type="submit" name="submitButton4" id="submitButton4" value="<fmt:message key="submit"/>">
		</form>
		
		<form action="ControllerServlet" method="POST" class="paramUpdate" id="change2">
			<input type="hidden" name="command" value="updateUser" /> <br>
			<h3 class="collection-title-h3"><fmt:message key="text8"/></h3>
			<p><fmt:message key="text9"/><p>
			<input type="text" name="race4" id="race4" maxlength="6" placeholder="№ <fmt:message key="flight"/>" pattern="^[ 0-9]+$" required> 
			<pre><span style="font-size: 17px">  <fmt:message key="text4"/></span></pre>
			<select name="dispatcher" size=4 required>
				<c:forEach var="current" items="${sessionScope.form2}">
					<option>${current.login}</option>
				</c:forEach>
			</select>
			<br>
			<br>
			<pre>     <input type="submit" name="submitButton4" id="submitButton4" value="<fmt:message key="submit"/>"></pre>
		</form>
	</div>
	</main>
	<!-- /MAIN SECTION -->
	<!-- fOOTER -->
	<footer class="footer">
		<div class="container">
			<p align="center">Kate Yankovska © 2018. All rights reserved</p>
		</div>
	</footer>
	<!-- /fOOTER -->
	
	<script type="text/javascript" src="js/inputText.js"></script>
	<script type="text/javascript" src="js/functssAdminRaces.js"></script>
</body>
</html>