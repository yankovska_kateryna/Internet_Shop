<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
	<title>Airline</title>
	<meta charset="UTF-8">
	<link rel="icon" href="images/travel.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/adminsRegistrations.css">
	<link rel="stylesheet" type="text/css" href="css/adminSignUp.css">
	<style>
		.colorText {
			color: #ffe647;
			font-weight: bold;
			font-size: 15px;
		}
	</style>
</head>
<body>

	<c:if test="${sessionScope.user.getLogin() != 'admin' }">
		<script type="text/javascript">
			document.location.replace("index.jsp");
		</script>
	</c:if>

	<!-- HEADER -->
	<header class="header">
		<div class="container">
			<div class="image-language">
				<a href="#"><img src="images\rus.png"></a>
				<a href="#"><img src="images\en.png"></a>
			</div>
			<h1>Авиакомпания</h1>
		</div>		 
	</header>
	<!-- /HEADER -->
	<!-- NAVIGATION -->
	<nav class="page-navigation">
		<div class="container">
			<img src="images\plane.png">
			<ul>
				<li><a href="admin.jsp">Домой</a></li>
			</ul>
			<div class="image-admin">
				<form action="ControllerServlet" method="POST">
					<input type="hidden" name="command" value="exit" />
					<input type="image" src="images\exit.png">
				</form>
			</div>
			<div class="account">
				<input type="text" name="account" class="colorText" value="${sessionScope.user.getLogin() }" readonly>
			</div>
			<div class="image-admin2">
				<img src="images\admin.png">
			</div>
		</div>	
	</nav>
	<!-- /NAVIGATION -->
	<!-- MAIN SECTION -->
	<main>
		<div class="container">
			<h2 class="collection-title">Регистрация диспетчеров</h2>
		</div>

		<form action="ControllerServlet" method="POST">
			<input type="hidden" name="command" value="registration"/>
  			<div class="container">
    			<p>Заполните форму для создания аккаунта.</p>
    			<hr>
				<div class="signup">
    				<label><b>Имя</b></label>
    				<input type="text" placeholder="Введите Имя" name="name" pattern="^[A-Za-zА-Яа-яЁё]+$" required>

    				<label><b>Фамилия</b></label>
    				<input type="text" placeholder="Введите Фамилию" name="surname" pattern="^[A-Za-zА-Яа-яЁё]+$" maxlength="30" required>

    				<label><b>Email</b></label>
    				<input type="email" placeholder="Введите Email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" maxlength="30" required>

    				<label><b>Номер Телефона</b></label>
    				<input type="tel" placeholder="Введите Номер Телефона" name="tel" pattern="^[ 0-9]+$" maxlength="12" required>

    				<label><b>Логин</b></label>
    				<input type="text" placeholder="Введите Логин" name="login" pattern = "[A-Za-z0-9]{6,}" maxlength="20" required>

    				<label><b>Пароль</b></label>
    				<input type="password" placeholder="Введите Пароль" name="psw" pattern=".{5,}" maxlength="20" required>
    			</div>

    			<div class="clearfix">
      				<a href="admin.jsp"><button type="button" class="cancelbtn">Отменить</button></a>
      				<button type="submit" class="signupbtn">Зарегестрировать</button>
    			</div>
    			<c:if test="${sessionScope.errorRegistration != null }">
					<font color="red">${sessionScope.errorRegistration }</font>
					<c:remove scope="session" var="errorRegistration"/>
				</c:if>
  			</div>
		</form>
	</main>
	<!-- /MAIN SECTION -->
	<!-- fOOTER -->
	<footer class="footer">
		<div class="container">
			<p align="center">Kate Yankovska © 2018. All rights reserved</p>
		</div>
	</footer>
	<!-- /fOOTER -->
</body>
</html>