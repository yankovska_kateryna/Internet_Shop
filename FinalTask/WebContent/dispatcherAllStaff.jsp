<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
	<title>Airline</title>
	<meta charset="UTF-8">
	<link rel="icon" href="images/travel.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/dispatcherAllStaff.css">
	<style>
		.colorText {
			color: #ffe647;
			font-weight: bold;
			font-size: 15px;
		}
		select {
			width: 300px;
		}
	</style>
</head>
<body>

	<c:if test="${sessionScope.user.getLogin() == 'admin' || sessionScope.user.getLogin() == null}">
		<script type="text/javascript">
			document.location.replace("index.jsp");
		</script>
	</c:if>
	
	<!-- HEADER -->
	<header class="header">
		<div class="container">
			<div class="image-language">
				<a href="#"><img src="images\rus.png"></a> <a href="#"><img
					src="images\en.png"></a>
			</div>
			<h1>Авиакомпания</h1>
		</div>
	</header>
	<!-- /HEADER -->
	<!-- NAVIGATION -->
	<nav class="page-navigation">
		<div class="container">
			<img src="images\plane.png">
			<ul>
				<li><a href="dispatcher.jsp">Домой</a></li>
			</ul>
			<div class="image-admin">
				<form action="ControllerServlet" method="POST">
					<input type="hidden" name="command" value="exit" />
					<input type="image" src="images\exit.png">
				</form>
			</div>
			<div class="account">
				<input type="text" name="account" class="colorText" value="${sessionScope.user.getLogin() }" readonly>
			</div>
			<div class="image-admin2">
				<img src="images\admin.png">
			</div>
		</div>
	</nav>
	<!-- /NAVIGATION -->
	<!-- MAIN SECTION -->
	<main>
		<div class="container">
			<h2 class="collection-title">
				Сотрудники
			</h2>
			
			<c:if test="${sessionScope.inputError != null }">
				<font color="red">${sessionScope.inputError }</font>
				<c:remove scope="session" var="inputError" />
			</c:if>
			<br>
			<br>

  				<!-- ПОИСК СОТРУДНИКОВ ПО ДОЛЖНОСТЯМ -->
				<form action="ControllerServlet" method="GET" class="parameters" id="formDispStaff">
					<input type="hidden" name="command" value="showStaff" />
					<h3 class="collection-title-h3">Поиск сотрудников по должностям</h3>
					<select name="position" required>
						<option>Пилот</option>
						<option>Штурман</option>
						<option>Радист</option>
						<option>Стюардесса</option>
					</select>
					<input type="submit" name="submitButton" value="Найти" id="ok">
				</form> 
				
				<br>
				<br>
				<table id="myTable">
					<tr>
						<th>№ сотрудника</th>
						<th>Имя</th>
						<th>Фамилия</th>
						<th>Должность</th>
						<th>Номер телефона</th>
					</tr>
					<c:forEach var="current" items="${staff}">
					<tr>
						<td>${current.id}</td>
						<td>${current.name}</td>
						<td>${current.surname}</td>
						<td>${current.position}</td>
						<td>${current.telNum}</td>
					</tr>
				</c:forEach>
				</table>
		</div>
	</main>
	<!-- /MAIN SECTION -->
	<!-- fOOTER -->
	<footer class="footer">
		<div class="container">
			<p align="center">Kate Yankovska © 2018. All rights reserved</p>
		</div>
	</footer>
	<!-- /fOOTER -->
	<script type="text/javascript" src="js/functionTab.js"></script>
</body>
</html>