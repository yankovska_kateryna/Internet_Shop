<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix = "ex" uri = "/WEB-INF/custom.tld"%>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'ru'}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="lang"/>
    
<!DOCTYPE html>
<html>
<head>
	<title><fmt:message key="airline"/></title>
	<meta charset="UTF-8">
	<link rel="icon" href="images/travel.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/styleIndexx1.css">
	<link rel="stylesheet" type="text/css" href="css/radiobtnIndex.css">
	<link rel="stylesheet" type="text/css" href="css/sliderIndex.css">
	<link rel="stylesheet" type="text/css" href="css/buttonToTop.css">

</head>
<body>
	
	<!-- HEADER -->
	<header class="header">
		<div class="container">
			<div class="image-lang">
				<form method="POST">
					<input name="language" type="image" value="ru" src="images\rus.png" ${language == 'ru' ? 'selected' : ''}>
					<input name="language" type="image" value="en" src="images\en.png" ${language == 'en' ? 'selected' : ''}>
				</form>
			</div>
			<h1><fmt:message key="airline2"/></h1>
		</div> 
	</header>
	<!-- /HEADER -->
	<!-- NAVIGATION -->
	<nav class="page-navigation">
		<div class="container">
			<img src="images\plane.png">
			<ul>
				<li><a href="#company"><fmt:message key="aboutCompany"/></a></li>
				<li><a href="#timetable"><fmt:message key="timetable"/></a></li>
				<li><a href="#contacts"><fmt:message key="contacts"/></a></li>
			</ul>

			<div class="image-admin">
				<a href="indexLogin.jsp"><img src="images\login.png"></a>
			</div>
		</div>	
	</nav>
	<!-- /NAVIGATION -->
	<!-- MAIN SECTION -->
	<main>
		<!-- SLIDER -->
		<div class="slideshow-container">

  			<!-- Full-width images with number and caption text -->
  			<div class="mySlides fade">
  				<div class="numbertext">1 / 4</div>
   				<img src="images\paris.jpg" style="width:100%">
  			</div>

 	 		<div class="mySlides fade">
   				<div class="numbertext">2 / 4</div>
    			<img src="images\rim.jpg" style="width:100%">
 			</div>

  			<div class="mySlides fade">
   				<div class="numbertext">3 / 4</div>
    			<img src="images\india.jpg" style="width:100%">
  			</div>

  			<div class="mySlides fade">
    			<div class="numbertext">4 / 4</div>
    			<img src="images\england.jpg" style="width:100%">
  			</div>

  			<!-- Next and previous buttons -->
  			<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
  			<a class="next" onclick="plusSlides(1)">&#10095;</a>
		</div>
		<br>

		<!-- The dots/circles -->
		<div style="text-align:center">
  			<span class="dot" onclick="currentSlide(1)"></span> 
  			<span class="dot" onclick="currentSlide(2)"></span> 
  			<span class="dot" onclick="currentSlide(3)"></span> 
  			<span class="dot" onclick="currentSlide(4)"></span>
		</div>

		<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>

		<div class="container">
			<h2 class="collection-title"><a name="company"><fmt:message key="aboutCompany"/></a></h2>
			<ex:Title message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."/>
		</div>

		<div class="container">
			<h2 class="collection-title"><a name="timetable"><fmt:message key="timetable"/></a> <small><fmt:message key="text10"/></small></h2>
		</div>

		<div class="container">
			<label class="containerCheckBox"><fmt:message key="direction"/>
				<input type="radio" name="option" value="Я знаю направление" id="radioKnow">
				<span class="checkmark"></span>
			</label>
			<label class="containerCheckBox"><fmt:message key="flights"/>
				<input type="radio" name="option" value="Я знаю номер рейса" id="radioDontKnow">
				<span class="checkmark"></span>
			</label>
			<label class="containerCheckBox"><fmt:message key="date"/>
				<input type="radio" name="option" value="Я знаю дату рейса" id="radioKnowDate">
				<span class="checkmark"></span>
			</label>
			
			<!-- ПО НАПРАВЛЕНИЮ -->
			<form action="ControllerServlet" method="GET" class="parameters" id="formFromToDate">
				<input type="hidden" name="command" value="inputFlight"/>
				<br>
				<div class="autocomplete" style="width:300px;">
    				<input id="fromPlace" type="text" maxlength="60" name="from" placeholder="<fmt:message key="inputFrom"/>" autocomplete="off">	
 				</div>
 				<div class="autocomplete" style="width:300px;">
 					<input id="toPlace" type="text" name="to" maxlength="60" placeholder="<fmt:message key="inputTo"/>" autocomplete="off">
				</div>
				<input type="date" name="date1" required>
				<input type="submit" name="submitButton1" value="<fmt:message key="find"/>">
			</form>
			
			<!-- ПО НОМЕРУ РЕЙСА -->
			<form action="ControllerServlet" method="GET" class="parameters" id="formRace">
				<input type="hidden" name="command" value="inputFlightNumber"/>
				<br>
				<input type="text" name="race" id="raceNum" maxlength="6" placeholder="№ <fmt:message key="flight"/>" pattern="^[ 0-9]+$"
				required style="width:200px;">
				<input type="submit" name="submitButton2" value="<fmt:message key="find"/>">	
			</form>
			
			<!-- ПО ДАТЕ РЕЙСА -->
			<form action="ControllerServlet" method="GET" class="parameters" id="formDate">
				<input type="hidden" name="command" value="inputFlightDate"/>
				<br>
				<input type="date" name="date3" required>
				<input type="submit" name="submitButton3" value="<fmt:message key="find"/>">	
			</form>
				<br>
				<br>
					<c:if test="${sessionScope.inputError != null }">
						<font color="red">${sessionScope.inputError }</font>
						<c:remove scope="session" var="inputError"/>
					</c:if>
				<br>
				
				<p><fmt:message key="sort"/>: </p>
				<label class="containerCheckBox2"><fmt:message key="number"/>
					<input type="radio" name="option2" value="По номеру" onclick="sortTableName(0)" >
					<span class="checkmark2"></span>
				</label>

				<label class="containerCheckBox2"><fmt:message key="name"/>
					<input type="radio" name="option2" value="По названию" onclick="sortTableName(5)">
					<span class="checkmark2"></span>
				</label>

				<table id="myTable">
  					<tr>
  						<th><fmt:message key="flightNum2"/></th>
    					<th><fmt:message key="from"/></th>
    					<th><fmt:message key="to"/></th>
    					<th><fmt:message key="dateDeparture"/></th>
    					<th><fmt:message key="dateArrival"/></th>
    					<th><fmt:message key="nameFlight"/></th>
    					<th><fmt:message key="flightStatus"/></th>
  					</tr>
  					<c:forEach var="current" items="${flights}">
  						<tr>
  							<td>${current.id}</td>
    						<td>${current.fromCity}, ${current.fromCountry}</td>
    						<td>${current.toCity}, ${current.toCountry}</td>
    						<td>${current.flightDate}</td>
    						<td>${current.flightArrival}</td>
    						<td>${current.raceName}</td>
    						<td>${current.status}</td>
 						 </tr>
  					</c:forEach>
				</table>
				
				<br>
				<br>
				<!-- ПОДСЧЕТ СУММЫ -->
				<form action="ControllerServlet" method="GET" class="parameters" id="formDate">
					<input type="hidden" name="command" value="inputCountFlight"/>
					<br>
					<div class="autocomplete" style="width:300px;">
    					<input id="fromPlace" type="text" maxlength="60" name="from" placeholder="<fmt:message key="inputFrom"/>" autocomplete="off">	
 					</div>
 					<div class="autocomplete" style="width:300px;">
 						<input id="toPlace" type="text" name="to" maxlength="60" placeholder="<fmt:message key="inputTo"/>" autocomplete="off">
					</div>
					<input type="submit" name="submitButton4" value="<fmt:message key="find"/>">	
				</form>
				<br>
				<table id="myTable">
  					<tr>
    					<th><fmt:message key="from"/></th>
    					<th><fmt:message key="to"/></th>
    					<th><fmt:message key="nameFlight"/></th>
    					<th>Количество рейсов</th>
  					</tr>
  					<c:forEach var="current" items="${flightsCount}">
  						<tr>
    						<td>${current.fromCity}, ${current.fromCountry}</td>
    						<td>${current.toCity}, ${current.toCountry}</td>
    						<td>${current.raceName}</td>
    						<td>${current.count}</td>
 						 </tr>
  					</c:forEach>
				</table>
				
		</div>

		<div class="container">
			<h2 class="collection-title"><a name="contacts"><fmt:message key="contacts"/></a></h2>
			<div id="map" style="width:960px; height:400px; background:white"></div><br>
			<p align="center"><b><fmt:message key="place"/>: </b></p>
			<p align="center"><fmt:message key="city"/><br>
			<fmt:message key="street"/><br>
			<fmt:message key="country"/><br></p>
			<p align="center"><b><fmt:message key="tel"/>:</b> +380963736931<br></p>
			<p align="center"><b>E-mail:</b> airlines.mailpost@gmail.com<br></p>
		</div>

	</main>
	<!-- /MAIN SECTION -->
	<!-- fOOTER -->
	<footer class="footer">
		<div class="container">
			<p align="center">Kate Yankovska © 2018. All rights reserved</p>
		</div>
	</footer>
	<!-- /fOOTER -->

	<script type="text/javascript" src="js/funcIndex.js"></script>
	<script type="text/javascript" src="js/inputText.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDur6NOY5NQ5XjbtkJZnLTxzYY01XlXOKY&callback=myMap"></script>
</body>
</html>