<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
	<title>Airline</title>
	<meta charset="UTF-8">
	<link rel="icon" href="images/travel.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/dispatcherStafff.css">
	<link rel="stylesheet" type="text/css" href="css/radiobtnIndex.css">
	<style>
		.colorText {
			color: #ffe647;
			font-weight: bold;
			font-size: 15px;
		}
		select {
			width: 300px;
		}
		.btn {
  			border: 2px solid black;
  			background-color: white;
  			color: black;
  			padding: 14px 28px;
  			font-size: 16px;
  			cursor: pointer;
		}
		/* Blue */
		.info {
 		 	border-color: #2196F3;
  			color: dodgerblue
		}

		.info:hover {
  			background: #2196F3;
  			color: white;
		}

		/* Orange */
		.warning {
  			border-color: #ff9800;
  			color: orange;
		}

		.warning:hover {
  			background: #ff9800;
  			color: white;
		}
	</style>
</head>
<body>

	<c:if test="${sessionScope.user.getLogin() == 'admin' || sessionScope.user.getLogin() == null}">
		<script type="text/javascript">
			document.location.replace("index.jsp");
		</script>
	</c:if>
	
	<!-- HEADER -->
	<header class="header">
		<div class="container">
			<div class="image-language">
				<a href="#"><img src="images\rus.png"></a> <a href="#"><img
					src="images\en.png"></a>
			</div>
			<h1>Авиакомпания</h1>
		</div>
	</header>
	<!-- /HEADER -->
	<!-- NAVIGATION -->
	<nav class="page-navigation">
		<div class="container">
			<img src="images\plane.png">
			<ul>
				<li><a href="dispatcher.jsp">Домой</a></li>
			</ul>
			<div class="image-admin">
				<form action="ControllerServlet" method="POST">
					<input type="hidden" name="command" value="exit" />
					<input type="image" src="images\exit.png">
				</form>
			</div>
			<div class="account">
				<input type="text" name="account" class="colorText" value="${sessionScope.user.getLogin() }" readonly>
			</div>
			<div class="image-admin2">
				<img src="images\admin.png">
			</div>
		</div>
	</nav>
	<!-- /NAVIGATION -->
	<!-- MAIN SECTION -->
	<main>
		<div class="container">
			<h2 class="collection-title">
				Формирование бригады на рейс
			</h2>
			<div class="container2">
				<a href="dispatcherAllStaff.jsp"><button class="btn info" style="width: 300px">Сотрудники</button></a>
				<br><br>
				<a href="dispatcherFlights.jsp"><button class="btn warning" style="width: 300px">Рейсы</button></a>
			</div>
			<br>
			<br>
			
			<c:if test="${sessionScope.inputError != null }">
				<font color="red">${sessionScope.inputError }</font>
				<c:remove scope="session" var="inputError" />
			</c:if>
			<c:if test="${sessionScope.noError != null }">
				<font color="green">${sessionScope.noError }</font>
				<c:remove scope="session" var="noError" />
			</c:if>
			<br>
			<br>

 			<!-- РАСПРЕДЕЛЕНИЕ СОТРУДНИКОВ ПО РЕЙСАМ -->
			<form action="ControllerServlet" method="POST" class="parameters" id="putStaffonRace">
				<input type="hidden" name="command" value="putStaff" />
				<h3 class="collection-title-h3">Распределение сотрудников по рейсам</h3>
				<pre><span style="font-size: 17px">    Выбор № рейса:</span></pre>
				<select size=6 name="raceId" required>
					<c:forEach var="current" items="${sessionScope.form}">
						<option>${current}</option>
					</c:forEach>
				</select>
				<br>
				<br>
				<input type="text" name="staffId" id="staffId" placeholder="№ сотрудника" pattern="^[ 0-9]+$"
				required>
				<input type="submit" name="submitButton" value="Подтвердить">
			</form>
			<br>
			
			 <!-- БРИГАДА ПО РЕЙСАМ -->
				
			<form action="ControllerServlet" method="GET" class="parameters" id="formDispAllStaff">
				<input type="hidden" name="command" value="outputAllStaffFlight" />
				<h3 class="collection-title-h3">Бригада по № рейса</h3>
				<p>Отправить результат поиска администратору:</p>
				<label class="containerCheckBox2">Формат .txt
					<input type="radio" name="option2" value="txt">
					<span class="checkmark2"></span>
				</label>

				<label class="containerCheckBox2">Формат .xls
					<input type="radio" name="option2" value="xls">
					<span class="checkmark2"></span>
				</label>
				<br>
				<input type="password" placeholder="Введите пароль" name="psw" pattern=".{5,}" maxlength="20">
				<br>
				<br>
				<input type="text" name="race" id="race" placeholder="№ рейса" maxlength="6" pattern="^[ 0-9]+$"
				required>
				<input type="submit" name="submitButton2" value="Найти" style="width: 150px">	
			</form>
			<br>
				<c:if test="${id != null }">		
					<p> Бригада на рейс № ${id} : </p>
				</c:if>
				<table id="myTable">
  					<tr>
  						<th>№ сотрудника</th>
    					<th>Имя</th>
    					<th>Фамилия</th>
    					<th>Должность</th>
  					</tr>
  					<c:forEach var="current1" items="${flightsStaff}">
  						<tr>
  							<td>${current1.id}</td>
    						<td>${current1.name}</td>
    						<td>${current1.surname}</td>
    						<td>${current1.position}</td>
 						 </tr>
  					</c:forEach>
				</table>
			<br>
		</div>
	</main>
	<!-- /MAIN SECTION -->
	<!-- fOOTER -->
	<footer class="footer">
		<div class="container">
			<p align="center">Kate Yankovska © 2018. All rights reserved</p>
		</div>
	</footer>
	<!-- /fOOTER -->
	<script type="text/javascript" src="js/functionTab.js"></script>
</body>
</html>