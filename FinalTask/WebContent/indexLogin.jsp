<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'ru'}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="lang"/>

<!DOCTYPE html>
<html>
<head>
	<title>Airline</title>
	<meta charset="UTF-8">
	<link rel="icon" href="images/travel.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/indexEntrance.css">
	<link rel="stylesheet" type="text/css" href="css/indexLogin3.css">
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
	<!-- HEADER -->
	<header class="header">
		<div class="container">
			<div class="image-language">
				<form method="POST">
					<input name="language" type="image" value="ru" src="images\rus.png" ${language == 'ru' ? 'selected' : ''}>
					<input name="language" type="image" value="en" src="images\en.png" ${language == 'en' ? 'selected' : ''}>
				</form>
			</div>
			<h1><fmt:message key="airline2"/></h1>
		</div>	
	</header>
	<!-- /HEADER -->
	<!-- NAVIGATION -->
	<nav class="page-navigation">
		<div class="container">
			<img src="images\plane.png">
			<ul>
				<li><a href="index.jsp#company"><fmt:message key="aboutCompany"/></a></li>
				<li><a href="index.jsp#timetable"><fmt:message key="timetable"/></a></li>
				<li><a href="index.jsp#contacts"><fmt:message key="contacts"/></a></li>
			</ul>
		</div>	
	</nav>
	<!-- /NAVIGATION -->
	<!-- MAIN SECTION -->
	<main>

		<div class="container">
			<h2 class="collection-title"><a name="timetable"><fmt:message key="entrance"/></a></h2>
		</div>

		<div class="container">
		  <div class = "form">
			<form action="ControllerServlet" method="POST">
				<input type="hidden" name="command" value="login"/>
  				<div class="imgcontainer">
    				<img src="images/man.png" alt="Avatar" class="avatar">
  				</div>

  				<div class="container2">
   		 			<label><b><fmt:message key="login"/></b></label>
    				<input type="text" maxlength="30" placeholder="<fmt:message key="enterL"/>" name="uname" required>

    				<label><b><fmt:message key="psw"/></b></label>
    				<input type="password" maxlength="20" placeholder="<fmt:message key="enterP"/>" name="password" required>
    				
    				<div class="g-recaptcha" data-sitekey="6LfeY0MUAAAAAOMRoa0QsITjau-s6s1qws5aZOPW" data-callback="recaptcha_callback"></div>

    				<input type="submit" id="button1" value="<fmt:message key="logIn"/>" disabled="disabled">
  				</div>
  				
  				<div class="container2" style="background-color:#f1f1f1">
   					<a href = "index.jsp"><button type="button" class="cancelbtn"><fmt:message key="cancel"/></button></a>
   					<c:if test="${sessionScope.errorLogin != null }">
							<font color="red">${sessionScope.errorLogin }</font>
							<c:remove scope="session" var="errorLogin"/>
					</c:if>
  				</div>
			</form>	
		  </div>
			
			<script type="text/javascript">
  				function recaptcha_callback() {
  					document.getElementById("button1").disabled = false;
  				}; 
  			</script>
		</div>

	</main>
	<!-- /MAIN SECTION -->
	
	<!-- fOOTER -->
	<footer class="footer">
		<div class="container">
			<p align="center">Kate Yankovska © 2018. All rights reserved</p>
		</div>
	</footer>
	<!-- /fOOTER -->

	<script type="text/javascript" src="js/funcIndex.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDur6NOY5NQ5XjbtkJZnLTxzYY01XlXOKY&callback=myMap"></script>
</body>
</html>