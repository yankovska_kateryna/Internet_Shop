//FORMELEMENTS
document.getElementById("add").style.display = "none";
document.getElementById("delete").style.display = "none";
document.getElementById("updateTelNum").style.display = "none";
document.getElementById("updateSurname").style.display = "none";

document.getElementById("addPerson").onclick = function() {
    document.getElementById("delete").style.display = "none";
    document.getElementById("add").style.display = "inline";
    document.getElementById("updateTelNum").style.display = "none";
    document.getElementById("updateSurname").style.display = "none";
}

document.getElementById("deletePerson").onclick = function() {
    document.getElementById("delete").style.display = "inline"; 
    document.getElementById("add").style.display = "none";
    document.getElementById("updateTelNum").style.display = "none";
    document.getElementById("updateSurname").style.display = "none";
}

document.getElementById("updatePerson").onclick = function() {
    document.getElementById("add").style.display = "none";
    document.getElementById("delete").style.display = "none";
    document.getElementById("updateTelNum").style.display = "inline";
    document.getElementById("updateSurname").style.display = "inline";
}