//FORMELEMENTS1
document.getElementById("raceNum").style.display = "none";

document.getElementById("radioDontKnow").onclick = function() {
    document.getElementById("fromPlace").style.display = "none"; 
    document.getElementById("toPlace").style.display = "none";
    document.getElementById("raceNum").style.display = "inline"; 
}

document.getElementById("radioKnow").onclick = function() {
    document.getElementById("fromPlace").style.display = "inline"; 
    document.getElementById("toPlace").style.display = "inline"; 
    document.getElementById("raceNum").style.display = "none";
}

//TABLE
function sortTableName(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("myTable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc"; 
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount ++;      
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

//BUTTON

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}


//FORMELEMENTS2
document.getElementById("fromPlace2").style.display = "none";
document.getElementById("toPlace2").style.display = "none";
document.getElementById("date2").style.display = "none";
document.getElementById("submitButton2").style.display = "none";
document.getElementById("raceNum2").style.display = "none";
document.getElementById("fromPlace3").style.display = "none";
document.getElementById("toPlace3").style.display = "none";
document.getElementById("date3").style.display = "none";
document.getElementById("raceNum3").style.display = "none";
document.getElementById("submitButton3").style.display = "none";

document.getElementById("add").onclick = function() {
    document.getElementById("fromPlace2").style.display = "inline"; 
    document.getElementById("toPlace2").style.display = "inline";
    document.getElementById("date2").style.display = "inline";
    document.getElementById("submitButton2").style.display = "inline";
    document.getElementById("raceNum2").style.display = "none";
    document.getElementById("fromPlace3").style.display = "none";
    document.getElementById("toPlace3").style.display = "none";
    document.getElementById("date3").style.display = "none";
    document.getElementById("raceNum3").style.display = "none";
    document.getElementById("submitButton3").style.display = "none";
}

document.getElementById("delete").onclick = function() {
    document.getElementById("fromPlace2").style.display = "none"; 
    document.getElementById("toPlace2").style.display = "none";
    document.getElementById("date2").style.display = "none";
    document.getElementById("submitButton2").style.display = "inline";
    document.getElementById("raceNum2").style.display = "inline";
    document.getElementById("fromPlace3").style.display = "none";
    document.getElementById("toPlace3").style.display = "none";
    document.getElementById("date3").style.display = "none";
    document.getElementById("raceNum3").style.display = "none";
    document.getElementById("submitButton3").style.display = "none";
}

document.getElementById("update").onclick = function() {
    document.getElementById("fromPlace2").style.display = "none"; 
    document.getElementById("toPlace2").style.display = "none";
    document.getElementById("date2").style.display = "none";
    document.getElementById("submitButton2").style.display = "none";
    document.getElementById("raceNum2").style.display = "none";
    document.getElementById("fromPlace3").style.display = "inline";
    document.getElementById("toPlace3").style.display = "inline";
    document.getElementById("date3").style.display = "inline";
    document.getElementById("raceNum3").style.display = "inline";
    document.getElementById("submitButton3").style.display = "inline"; 
}

//FORMELEMENTS3
document.getElementById("SName2").style.display = "none";
document.getElementById("Name2").style.display = "none";
document.getElementById("Position2").style.display = "none";
document.getElementById("Email2").style.display = "none";
document.getElementById("Tel2").style.display = "none";
document.getElementById("submitButton4").style.display = "none";
document.getElementById("persSName").style.display = "none";
document.getElementById("persPos").style.display = "none";
document.getElementById("submitButton5").style.display = "none";



document.getElementById("addPerson").onclick = function() {
    document.getElementById("SName2").style.display = "inline";
    document.getElementById("Name2").style.display = "inline";
    document.getElementById("Position2").style.display = "inline";
    document.getElementById("Email2").style.display = "inline";
    document.getElementById("Tel2").style.display = "inline";
    document.getElementById("submitButton4").style.display = "inline";
    document.getElementById("persSName").style.display = "none";
    document.getElementById("persPos").style.display = "none";
    document.getElementById("submitButton5").style.display = "none";
}

document.getElementById("deletePerson").onclick = function() {
    document.getElementById("SName2").style.display = "none";
    document.getElementById("Name2").style.display = "none";
    document.getElementById("Position2").style.display = "none";
    document.getElementById("Email2").style.display = "none";
    document.getElementById("Tel2").style.display = "none";
    document.getElementById("submitButton4").style.display = "none";
    document.getElementById("persSName").style.display = "inline";
    document.getElementById("persPos").style.display = "inline";
    document.getElementById("submitButton5").style.display = "inline";
}

document.getElementById("updatePerson").onclick = function() {
    document.getElementById("SName2").style.display = "none";
    document.getElementById("Name2").style.display = "none";
    document.getElementById("Position2").style.display = "none";
    document.getElementById("Email2").style.display = "none";
    document.getElementById("Tel2").style.display = "none";
    document.getElementById("submitButton4").style.display = "inline";
    document.getElementById("persSName").style.display = "inline";
    document.getElementById("persPos").style.display = "inline";
    document.getElementById("submitButton5").style.display = "none";
}