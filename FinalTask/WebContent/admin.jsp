<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'ru'}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="lang"/>

<!DOCTYPE html>
<html>
<head>
	<title><fmt:message key="airline"/></title>
	<meta charset="UTF-8">
	<link rel="icon" href="images/travel.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/styleAdminss.css">
	<style>
		.colorText {
			color: #ffe647;
			font-weight: bold;
			font-size: 15px;
		}
	</style>
</head>
<body>
	
	<c:if test="${sessionScope.user.getLogin() != 'admin' }">
		<script type="text/javascript">
			document.location.replace("index.jsp");
		</script>
	</c:if>

	<!-- HEADER -->
	<header class="header">
		<div class="container">
			<div class="image-language">
				<form method="POST">
					<input name="language" type="image" value="ru" src="images\rus.png" ${language == 'ru' ? 'selected' : ''}>
					<input name="language" type="image" value="en" src="images\en.png" ${language == 'en' ? 'selected' : ''}>
				</form>
			</div>
			<h1><fmt:message key="airline2"/></h1>
		</div>		 
	</header>
	<!-- /HEADER -->
	<!-- NAVIGATION -->
	<nav class="page-navigation">
		<div class="container">
			<div class="image-admin">
				<form action="ControllerServlet" method="POST">
					<input type="hidden" name="command" value="exit" />
					<input type="image" src="images\exit.png">
				</form>
			</div>
			<div class="account">
				<input type="text" name="account" class="colorText" value="${sessionScope.user.getLogin() }" readonly>
			</div>
			<div class="image-admin2">
				<img src="images\admin.png">
			</div>
		</div>	
	</nav>
	<!-- /NAVIGATION -->
	<!-- MAIN SECTION -->
	<main>
		<div class="container">
			<form action="ControllerServlet" method="POST">
				<input type="hidden" name="command" value="allUsers" /> <br>
				<button class="button"><span><fmt:message key="adminbutton1"/></span></button>
			</form>
			<a href="adminWorkers.jsp"><button class="button"><span><fmt:message key="adminbutton2"/></span></button></a>
			<a href="adminRegistration.jsp"><button class="button"><span><fmt:message key="adminbutton3"/></span></button></a>
			<form action="ControllerServlet" method="POST">
				<input type="hidden" name="command" value="inputAppTable" />
				<button class="button"><span><fmt:message key="adminbutton4"/></span></button>
			</form>
		</div>
	</main>
	<!-- /MAIN SECTION -->
	<!-- fOOTER -->
	<footer class="footer">
		<div class="container">
			<p align="center">Kate Yankovska © 2018. All rights reserved</p>
		</div>
	</footer>
	<!-- /fOOTER -->
</body>
</html>