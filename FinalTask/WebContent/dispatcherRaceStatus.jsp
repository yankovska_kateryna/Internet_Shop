<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
	<title>Airline</title>
	<meta charset="UTF-8">
	<link rel="icon" href="images/travel.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/dispatchRacesStatusss.css">
	<style>
		.colorText {
			color: #ffe647;
			font-weight: bold;
			font-size: 15px;
		}
	</style>
</head>
<body>

	<c:if test="${sessionScope.user.getLogin() == 'admin' || sessionScope.user.getLogin() == null}">
		<script type="text/javascript">
			document.location.replace("index.jsp");
		</script>
	</c:if>
	
	<!-- HEADER -->
	<header class="header">
		<div class="container">
			<div class="image-language">
				<a href="#"><img src="images\rus.png"></a> <a href="#"><img
					src="images\en.png"></a>
			</div>
			<h1>Авиакомпания</h1>
		</div>
	</header>
	<!-- /HEADER -->
	<!-- NAVIGATION -->
	<nav class="page-navigation">
		<div class="container">
			<img src="images\plane.png">
			<ul>
				<li><a href="dispatcher.jsp">Домой</a></li>
			</ul>
			<div class="image-admin">
				<form action="ControllerServlet" method="POST">
					<input type="hidden" name="command" value="exit" />
					<input type="image" src="images\exit.png">
				</form>
			</div>
			<div class="account">
				<input type="text" name="account" class="colorText" value="${sessionScope.user.getLogin() }" readonly>
			</div>
			<div class="image-admin2">
				<img src="images\admin.png">
			</div>
		</div>
	</nav>
	<!-- /NAVIGATION -->
	<!-- MAIN SECTION -->
	<main>
		<div class="container">
			<h2 class="collection-title">
				Изменение стуса рейсов
			</h2>
			
			<c:if test="${sessionScope.Error != null }">
				<font color="red">${sessionScope.Error }</font>
				<c:remove scope="session" var="Error" />
			</c:if>
			<c:if test="${sessionScope.noError != null }">
				<font color="green">${sessionScope.noError }</font>
				<c:remove scope="session" var="noError" />
			</c:if>
			<br>
			<br>
		</div>
		<div class="container2">
			<form action="ControllerServlet" method="POST" class="paramUpdate" id="formRaceStatus">
				<input type="hidden" name="command" value="formFlightStatus" />
				<div class="selectFlight">
					<pre style="font-size: 17px">            Выберете № рейса:</pre>
					<select size=5 name="raceId" required>
						<c:forEach var="current" items="${sessionScope.form}">
							<option>${current}</option>
						</c:forEach>
					</select>
				</div>
				<br>
				<br>
				<select size=12 name="status" required>
					<option>Идет посадка</option>
					<option>Рейс отменен</option>
					<option>Регистрация на рейс началась</option>
					<option>Самолет вылетит согласно расписанию</option>
					<option>Рейс уже улетел</option>
					<option>Выход на посадку открыт</option>
					<option>Регистрация закрывается</option>
					<option>Регистрация на рейс завершена</option>
					<option>Самолет прилетит согласно расписанию</option>
					<option>Рейс задерживается</option>
					<option>Самолет в пути</option>
					<option>Самолет уже приземлился</option>
				</select>
				<br>
				<br>
				<input type="submit" name="submitButton" id="submitButton" value="Подтвердить">
			</form>
		</div>
	</main>
	<!-- /MAIN SECTION -->
	<!-- fOOTER -->
	<footer class="footer">
		<div class="container">
			<p align="center">Kate Yankovska © 2018. All rights reserved</p>
		</div>
	</footer>
	<!-- /fOOTER -->
</body>
</html>