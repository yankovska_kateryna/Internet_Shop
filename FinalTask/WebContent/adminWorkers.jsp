<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<title>Airline</title>
<meta charset="UTF-8">
<link rel="icon" href="images/travel.png" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/adminsWorkerss.css">
<link rel="stylesheet" type="text/css" href="css/radiobtnAdminRaces.css">

<style>
	.colorText {
		color: #ffe647;
		font-weight: bold;
		font-size: 15px;
	}
</style>
</head>
<body>

	<c:if test="${sessionScope.user.getLogin() != 'admin' }">
		<script type="text/javascript">
			document.location.replace("index.jsp");
		</script>
	</c:if>

	<!-- HEADER -->
	<header class="header">
		<div class="container">
			<div class="image-language">
				<img src="images\rus.png"> <img src="images\en.png">
			</div>
			<h1>Авиакомпания</h1>
		</div>
	</header>
	<!-- /HEADER -->
	<!-- NAVIGATION -->
	<nav class="page-navigation">
		<div class="container">
			<img src="images\plane.png">
			<ul>
				<li><a href="admin.jsp">Домой</a></li>
			</ul>
			<div class="image-admin">
				<form action="ControllerServlet" method="POST">
					<input type="hidden" name="command" value="exit" />
					<input type="image" src="images\exit.png">
				</form>
			</div>
			<div class="account">
				<input type="text" name="account" class="colorText" value="${sessionScope.user.getLogin() }" readonly>
			</div>
			<div class="image-admin2">
				<img src="images\admin.png">
			</div>
		</div>
	</nav>
	<!-- /NAVIGATION -->
	<!-- MAIN SECTION -->
	<main>

	<div class="container">
		<h2 class="collection-title">
			Управление персоналом <small>Добавление, удаление работников,
				редактирование данных</small>
		</h2>
		<c:if test="${sessionScope.inputError != null }">
			<font color="red">${sessionScope.inputError }</font>
			<c:remove scope="session" var="inputError" />
		</c:if>
		<c:if test="${sessionScope.Error != null }">
			<font color="red">${sessionScope.Error }</font>
			<c:remove scope="session" var="Error" />
		</c:if>
		<c:if test="${sessionScope.noError != null }">
			<font color="green">${sessionScope.noError }</font>
			<c:remove scope="session" var="noError" />
		</c:if>
		<br>
		<br>
	</div>
	
		<!--                     ПРОСМОТР РАБОЧИХ                          -->

	<div class="container">
		<form action="ControllerServlet" method="GET" class="parameters" id="formStaff">
			<input type="hidden" name="command" value="findStaff" /> 
			<input type="text" name="name" id="name" placeholder="Имя" maxlength="30" pattern="^[A-Za-zА-Яа-яЁё]+$" required> 
			<input type="text" name="surname" id="surname" placeholder="Фамилия" maxlength="30" pattern="^[A-Za-zА-Яа-яЁё]+$" required> 
			<select name="position">
				<option>Пилот</option>
				<option>Штурман</option>
				<option>Радист</option>
				<option>Стюардесса</option>
			</select>
			<input type="submit" name="submitButton" value="Найти">
		</form>
		<br>
		<table id="myTable">
			<tr>
				<th>№ сотрудника</th>
				<th>Имя</th>
				<th>Фамилия</th>
				<th>Должность</th>
				<th>Номер телефона</th>
			</tr>
			<c:forEach var="current" items="${staff}">
				<tr>
					<td>${current.id}</td>
					<td>${current.name}</td>
					<td>${current.surname}</td>
					<td>${current.position}</td>
					<td>${current.telNum}</td>
				</tr>
			</c:forEach>
		</table>
	
		<label class="containerCheckBox3">Добавить работника 
			<input type="radio" name="optionPerson" value="Добавить работника" id="addPerson"> 
				<span class="checkmark3"></span>
		</label> 
		<label class="containerCheckBox3">Удалить работника 
			<input type="radio" name="optionPerson" value="Удалить работника" id="deletePerson"> 
			<span class="checkmark3"></span>
		</label> 
		<label class="containerCheckBox3">Редактировать данные 
			<input type="radio" name="optionPerson" value="Редактировать данные" id="updatePerson"> 
			<span class="checkmark3"></span>
		</label>
		
		<!--                     ДОБАВЛЕНИЕ РАБОТНИКА                          -->

		<form action="ControllerServlet" method="POST" class="personalUpdate" id="add">
			<p>Введите данные для добавления работника</p>
			<input type="hidden" name="command" value="addStaff" />
			<input type="text" name="name2" id="name2" placeholder="Имя" maxlength="30" pattern="^[A-Za-zА-Яа-яЁё]+$" required> 
			<input type="text" name="surname2" id="surname2" placeholder="Фамилия" maxlength="30" pattern="^[A-Za-zА-Яа-яЁё]+$" required> 
			<select name="position2">
				<option>Пилот</option>
				<option>Штурман</option>
				<option>Радист</option>
				<option>Стюардесса</option>
			</select>
			<input type="text" name="telNum" id="telNum" placeholder="Номер телефона" maxlength="12" pattern="^[ 0-9]+$" required> 
			<input type="submit" name="submitButton2" id="submitButton2" value="Подтвердить">
		</form>
		
		
		<!--                     УДАЛЕНИЕ РАБОТНИКА                          -->

		<form action="ControllerServlet" method="POST" class="personalUpdate" id="delete">
			<p>Введите № сотрудника для удаления</p>
			<input type="hidden" name="command" value="deleteStaff" />
			<input type="text" name="id" id="id" placeholder="№ сотрудника" maxlength="6" pattern="^[ 0-9]+$" required autocomplete="off"> 
			<input type="submit" name="submitButton3" id="submitButton3" value="Подтвердить">
		</form>
		
		<!--                     РЕДАКТИРОВАНИЕ РАБОТНИКА                          -->
	
		<form action="ControllerServlet" method="POST" class="personalUpdate" id="updateSurname">
			<h3 class="collection-title-h3">Изменить фамилию сотрудника</h3>
			<p>Введите № сотрудника для редактирования фамилии</p>
			<input type="hidden" name="command" value="updateStaffSurname" />
			<input type="text" name="id2" id="id2" placeholder="№ сотрудника" maxlength="6" pattern="^[ 0-9]+$" required> 
			<input type="text" name="surname3" id="surname3" placeholder="Фамилия" maxlength="30" pattern="^[A-Za-zА-Яа-яЁё]+$" required>
			<input type="submit" name="submitButton4" id="submitButton4" value="Подтвердить">
		</form>
		
		<form action="ControllerServlet" method="POST" class="personalUpdate" id="updateTelNum">
		<h3 class="collection-title-h3">Изменить номер телефона сотрудника</h3>
			<p>Введите № сотрудника для редактирования номерa телефона</p>
			<input type="hidden" name="command" value="updateStaffTelNum" />
			<input type="text" name="id3" id="id3" placeholder="№ сотрудника" maxlength="6" pattern="^[ 0-9]+$" required> 
			<input type="text" name="telNum2" id="telNum2" placeholder="Номер телефона" maxlength="12" pattern="^[ 0-9]+$" required>
			<input type="submit" name="submitButton5" id="submitButton5" value="Подтвердить">
		</form>
	</div>
	
	</main>
	<!-- /MAIN SECTION -->
	<!-- fOOTER -->
	<footer class="footer">
		<div class="container">
			<p align="center">Kate Yankovska © 2018. All rights reserved</p>
		</div>
	</footer>
	<!-- /fOOTER -->

	<script type="text/javascript" src="js/functionsAdminWorkers.js"></script>
</body>
</html>