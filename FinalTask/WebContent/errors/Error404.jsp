<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<title>Error404</title>
	<style>
		.fig {
			text-align: center;
		}
	</style>
</head>
<body>
	<p align="center"><a href="index.jsp">Back to Home Page</a></p>
	<p class="fig">
		<img alt="Error404" src="images/404.gif">
	</p>
</body>
</html>