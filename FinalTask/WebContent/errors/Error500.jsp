<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<title>Error500</title>
	<style>
		.fig {
			text-align: center;
		}
	</style>
</head>
<body>
	<p align="center"><a href="index.jsp">Back to Home Page</a></p>
	<p class="fig">
		<img alt="Error500" src="images/500.jpg">
	</p>
</body>
</html>