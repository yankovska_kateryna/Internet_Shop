package ua.khpi.yankovska.finalTask.commands.dispatcherStaff;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.FlightDAO;
import ua.khpi.yankovska.finalTask.db.LogicDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Flight;
import ua.khpi.yankovska.finalTask.entity.User;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which add personnel to flights.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandAddStaffToFlight implements Command {

	private static final String DD_MM_YYYY_HH_MM = "dd-MM-yyyy HH:mm";
	private static final String OUTPUT_FORMATTING = "Output formatting";
	private static final String ERRORS_ERROR500_JSP = "errors/Error500.jsp";
	private static final String DISPATCHER_STAFF_JSP = "dispatcherStaff.jsp";
	private static final String INPUT_ERROR = "inputError";
	private static final Logger LOG = Logger.getLogger(CommandAddStaffToFlight.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to add personnel to flights.
	 *
	 * @return a dispatcherStaff.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		session.removeAttribute("form");
		session.removeAttribute("noError");

		int raceId = Integer.parseInt(request.getParameter("raceId"));
		int staffId = Integer.parseInt(request.getParameter("staffId"));

		String attribute = request.getParameter("1");
		LOG.trace(attribute);

		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();

		List<Integer> list = null;
		List<Flight> listDate = null;
		Flight inputDate = null;
		boolean update = false;

		boolean input = false;
		List<Flight> list2 = null;
		boolean delete = false;

		Date dateFlight = null;
		Date dateNow = new Date();
		List<Flight> changeFlight = null;

		try {
			Connection con = dbManager.getConnection();
			con.setAutoCommit(false);

			// DELETE OLD FLIGHT
			FlightDAO flightDAO = new FlightDAO(con);
			list2 = flightDAO.findOldFlights();

			LOG.trace("+++++++++++");
			LOG.trace(list2);
			if (!list2.isEmpty()) {
				for (int i = 0; i < list2.size(); i++) {
					delete = flightDAO.delete(list2.get(i).getId());
				}
			}
			LOG.trace(delete);
			////

			LogicDAO logicDAO = new LogicDAO(con);
			list = logicDAO.findFlightsForUser((User) session.getAttribute("user"));

			if (list.isEmpty()) {
				LOG.trace("Transaction flight-staff status rollback");
				Util.doRollBack(con);
				session.setAttribute(INPUT_ERROR, "���� �� ��� ������");
				return DISPATCHER_STAFF_JSP;
			}
			listDate = flightDAO.findDatesOfFlightsByStaffId(staffId);
			inputDate = flightDAO.findInputDateOfFlight(raceId);

			if (inputDate == null) {
				LOG.trace("Transaction flight-staff status rollback");
				Util.doRollBack(con);
				session.setAttribute(INPUT_ERROR, "���� �� ��� ������");
				return DISPATCHER_STAFF_JSP;
			}

			LOG.trace(listDate);
			LOG.trace(inputDate);

			if (listDate.isEmpty()) {
				input = true;
			} else {
				DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				DateFormat outputFormat = new SimpleDateFormat(DD_MM_YYYY_HH_MM);
				for (int i = 0; i < listDate.size(); i++) {
					try {
						Date date = inputFormat.parse(listDate.get(i).getFlightDate());
						listDate.get(i).setFlightDate(outputFormat.format(date));
						date = inputFormat.parse(listDate.get(i).getFlightArrival());
						listDate.get(i).setFlightArrival(outputFormat.format(date));
					} catch (ParseException e) {
						LOG.trace(OUTPUT_FORMATTING);
						return ERRORS_ERROR500_JSP;
					}
				}

				try {
					Date date = inputFormat.parse(inputDate.getFlightDate());
					inputDate.setFlightDate(outputFormat.format(date));
					date = inputFormat.parse(inputDate.getFlightArrival());
					inputDate.setFlightArrival(outputFormat.format(date));
				} catch (ParseException e) {
					LOG.trace(OUTPUT_FORMATTING);
					return ERRORS_ERROR500_JSP;
				}

				try {
					Date fDate = new SimpleDateFormat(DD_MM_YYYY_HH_MM).parse(inputDate.getFlightDate());
					Date aDate = new SimpleDateFormat(DD_MM_YYYY_HH_MM).parse(inputDate.getFlightArrival());
					for (int i = 0; i < listDate.size(); i++) {

						if (listDate.size() == i + 1) {
							Date fDate2 = new SimpleDateFormat(DD_MM_YYYY_HH_MM)
									.parse(listDate.get(i).getFlightDate());
							Date aDate2 = new SimpleDateFormat(DD_MM_YYYY_HH_MM)
									.parse(listDate.get(i).getFlightArrival());
							LOG.trace(fDate2);
							LOG.trace(aDate2);
							LOG.trace(fDate);
							LOG.trace(aDate);
							if (fDate.after(aDate2) && (fDate.getTime() / 1000 - aDate2.getTime() / 1000) > 10800) {
								input = true;
								LOG.trace("Input next flight to staff not earlier than 5 hours");
							}
						} else {
							Date fDate2 = new SimpleDateFormat(DD_MM_YYYY_HH_MM)
									.parse(listDate.get(i + 1).getFlightDate());
							Date aDate2 = new SimpleDateFormat(DD_MM_YYYY_HH_MM)
									.parse(listDate.get(i).getFlightArrival());
							LOG.trace("fDate2" + fDate2);
							LOG.trace("aDate2" + aDate2);
							LOG.trace("fDate" + fDate);
							LOG.trace("aDate" + aDate);
							if (fDate.after(aDate2) && aDate.before(fDate2)
									&& (fDate.getTime() / 1000 - aDate2.getTime() / 1000) > 10800
									&& (fDate2.getTime() / 1000 - aDate.getTime() / 1000) > 10800) {
								input = true;
								LOG.trace("Input next flight to staff not earlier than 5 hours");
							}
						}

						if (input == true) {
							break;
						} else if (i + 1 == listDate.size()) {
							break;
						}
					}
				} catch (ParseException e) {
					LOG.trace(OUTPUT_FORMATTING);
					return ERRORS_ERROR500_JSP;
				}
			}
			LOG.trace(input);

			if (list.contains(raceId) && input == true) {

				changeFlight = flightDAO.getFlightByFlightNumber(raceId);
				dateFlight = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(changeFlight.get(0).getFlightDate());
				LOG.trace(dateFlight);

				if ((dateFlight.getTime() / 1000) <= (dateNow.getTime() / 1000)) {
					session.setAttribute(INPUT_ERROR, "���� � ������");
					session.setAttribute("form", list);
					return DISPATCHER_STAFF_JSP;
				}

				if (((dateFlight.getTime() / 1000) - (dateNow.getTime() / 1000)) <= 120) {
					session.setAttribute(INPUT_ERROR, "������ �������� �������� �� 2 ������ �� ������");
					session.setAttribute("form", list);
					return DISPATCHER_STAFF_JSP;
				}

				update = logicDAO.setStaffForFlights(raceId, staffId);

				if (!update) {
					LOG.trace("Transaction flight-staff status rollback");
					Util.doRollBack(con);
					session.setAttribute(INPUT_ERROR, "�������� �� �������");
					session.setAttribute("form", list);
					return DISPATCHER_STAFF_JSP;
				}
			} else {
				LOG.trace("Transaction flight-staff adding rollback");
				Util.doRollBack(con);
				session.setAttribute(INPUT_ERROR, "�������� �� �������");
				session.setAttribute("form", list);
				return DISPATCHER_STAFF_JSP;
			}
			con.commit();
			Util.close(con);
		} catch (Exception e) {
			session.setAttribute(INPUT_ERROR, "��������� ������ � ������ ���� ������");
			LOG.error("Error in db " + e.getMessage());
			return ERRORS_ERROR500_JSP;
		}
		LOG.debug("Finishing DB operation");
		session.setAttribute("form", list);
		session.setAttribute("noError", "��������� �������� �� ����");

		session.removeAttribute(INPUT_ERROR);

		return DISPATCHER_STAFF_JSP;
	}

}
