package ua.khpi.yankovska.finalTask.commands.dispatcherStaff;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.FlightDAO;
import ua.khpi.yankovska.finalTask.db.StaffDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Flight;
import ua.khpi.yankovska.finalTask.entity.Staff;
import ua.khpi.yankovska.finalTask.entity.User;
import ua.khpi.yankovska.finalTask.filewriter.CreatorExcel;
import ua.khpi.yankovska.finalTask.filewriter.CreatorTXT;
import ua.khpi.yankovska.finalTask.mail.CreateMailContent;
import ua.khpi.yankovska.finalTask.mail.Message;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which get brigade from the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandOutputBrigade implements Command {

	private static final String INPUT_ERROR = "inputError";
	private static final String DISPATCHER_STAFF_JSP = "dispatcherStaff.jsp";
	private static final Logger LOG = Logger.getLogger(CommandOutputBrigade.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to get brigade from the database.
	 *
	 * @return a dispatcherStaff.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		int id;

		try {
			id = Integer.parseInt(request.getParameter("race"));
		} catch (NumberFormatException e) {
			LOG.warn("Incorrect input");
			session.setAttribute(INPUT_ERROR, "�������� ����!");
			return DISPATCHER_STAFF_JSP;
		}

		String radiobtn = request.getParameter("option2");
		LOG.trace(radiobtn);

		LOG.trace("Getting position: " + id);

		LOG.debug("Starting db operation");

		DBManager dbManager = DBManager.getInstance();
		List<Staff> list = null;
		List<Flight> list2 = null;
		boolean delete = false;

		try {
			Connection con = dbManager.getConnection();

			// DELETE OLD FLIGHT
			FlightDAO flightDAO = new FlightDAO(con);
			list2 = flightDAO.findOldFlights();

			LOG.trace(list2);
			if (!list2.isEmpty()) {
				for (int i = 0; i < list2.size(); i++) {
					delete = flightDAO.delete(list2.get(i).getId());
				}
			}
			LOG.trace(delete);
			////

			StaffDAO staffDAO = new StaffDAO(con);
			list = staffDAO.getSpecialFlightStaff(id);
			Util.close(con);
		} catch (Exception e) {
			session.setAttribute(INPUT_ERROR, "��������� ������ � ������ ���� ������");
			LOG.error("Error in db");
			return "errors/Error500.jsp";
		}

		if (list.isEmpty()) {
			session.setAttribute(INPUT_ERROR, "� ���������, ������� ��� ����� ������� ����� ���");
			LOG.trace("List of staff is empty");
			return DISPATCHER_STAFF_JSP;
		} else {
			session.removeAttribute(INPUT_ERROR);

			request.setAttribute("flightsStaff", list);
			request.setAttribute("id", id);

			User user = (User) session.getAttribute("user");

			String dispatcherMail = user.getEmail();
			LOG.trace(dispatcherMail);
			String adminEmail = "airlines.mailpost@gmail.com";
			String password = request.getParameter("psw");
			LOG.trace(password);

			if (password != null && password != "" && radiobtn != null) {
				if (radiobtn.equals("txt")) {

					File f = new File("C:\\Users\\User\\Desktop\\Java\\FinalTask\\flights.txt");
					CreatorTXT ctxt = new CreatorTXT();
					try {
						ctxt.create(list, f);
					} catch (IOException e1) {
						LOG.error("Cannot create file");
					}

					Thread t = new Thread(() -> {
						try {
							Message.sendFileMessage(dispatcherMail, password, adminEmail,
									CreateMailContent.createFileContent(user.getLogin(), id), f);
							Thread.sleep(1000);
						} catch (Exception e) {
							// noting to do
						}
						if (f.delete()) {
							LOG.info("File was deleted");
						}
					});
					t.start();
				} else if (radiobtn.equals("xls")) {

					File f2 = new File("C:\\Users\\User\\Desktop\\Java\\FinalTask\\flights.xls");
					CreatorExcel cexcel = new CreatorExcel();
					cexcel.create(list, f2);

					Thread t = new Thread(() -> {
						try {
							Message.sendFileMessage(dispatcherMail, password, adminEmail,
									CreateMailContent.createFileContent(user.getLogin(), id), f2);
							Thread.sleep(1000);
						} catch (Exception e) {
							// noting to do
						}
						if (f2.delete()) {
							LOG.info("File was deleted");
						}
					});
					t.start();
				}
			}

			LOG.trace("List of flightsStaff is full");
			return DISPATCHER_STAFF_JSP;
		}
	}
}
