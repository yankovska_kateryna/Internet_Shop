package ua.khpi.yankovska.finalTask.commands.dispatcherStaff;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.StaffDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Staff;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which get personnel by job from the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandOutputStaffByJob implements Command {

	private static final String INPUT_ERROR = "inputError";
	private static final String DISPATCHER_ALL_STAFF_JSP = "dispatcherAllStaff.jsp";
	private static final Logger LOG = Logger.getLogger(CommandOutputStaffByJob.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to get personnel by job from the
	 * database.
	 *
	 * @return a dispatcherAllStaff.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		String position;

		try {
			position = request.getParameter("position").trim();

			if (position == null) {
				session.setAttribute(INPUT_ERROR, "����������, ��������� ��� ��������!");
				return DISPATCHER_ALL_STAFF_JSP;
			} else if (position == "") {
				session.setAttribute(INPUT_ERROR, "����������, ������� ��������!");
				return DISPATCHER_ALL_STAFF_JSP;
			}
		} catch (NullPointerException e) {
			LOG.warn("Incorrect input");
			session.setAttribute(INPUT_ERROR, "�������� ����!");
			return DISPATCHER_ALL_STAFF_JSP;

		}
		position = position.replaceAll("[\\s]{1,}", "");

		position = position.substring(0, 1).toUpperCase() + position.substring(1);

		LOG.trace("Getting position: " + position);

		LOG.debug("Starting db operation");

		DBManager dbManager = DBManager.getInstance();
		List<Staff> list = null;

		try {
			Connection con = dbManager.getConnection();
			StaffDAO staffDAO = new StaffDAO(con);
			list = staffDAO.getStaffByJob(position);
			Util.close(con);
		} catch (Exception e) {
			session.setAttribute(INPUT_ERROR, "��������� ������ � ������ ���� ������");
			LOG.error("Error in db");
			return "errors/Error500.jsp";
		}

		if (list.isEmpty()) {
			session.setAttribute(INPUT_ERROR, "����������� ���! �������� ���������!");
			LOG.trace("List of flights is empty");
			return DISPATCHER_ALL_STAFF_JSP;
		} else {
			session.removeAttribute(INPUT_ERROR);

			request.setAttribute("staff", list);

			LOG.trace("List of flights is full");
			return DISPATCHER_ALL_STAFF_JSP;
		}
	}
}
