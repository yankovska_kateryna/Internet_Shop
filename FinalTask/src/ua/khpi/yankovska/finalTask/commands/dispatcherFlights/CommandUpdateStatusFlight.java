package ua.khpi.yankovska.finalTask.commands.dispatcherFlights;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.FlightDAO;
import ua.khpi.yankovska.finalTask.db.LogicDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Flight;
import ua.khpi.yankovska.finalTask.entity.User;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which update flight's status in the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandUpdateStatusFlight implements Command {

	private static final String DISPATCHER_RACE_STATUS_JSP = "dispatcherRaceStatus.jsp";
	private static final String ERROR = "Error";
	private static final Logger LOG = Logger.getLogger(CommandUpdateStatusFlight.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to update flight's status in the
	 * database.
	 *
	 * @return a dispatcherRaceStatus.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		session.removeAttribute("noError");

		session.removeAttribute("form");

		int raceId = Integer.parseInt(request.getParameter("raceId"));
		String status = request.getParameter("status");

		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();

		List<Integer> list = null;
		boolean update = false;

		List<Flight> list2 = null;
		boolean delete = false;

		try {
			Connection con = dbManager.getConnection();
			con.setAutoCommit(false);

			// DELETE OLD FLIGHT
			FlightDAO flightDAO = new FlightDAO(con);
			list2 = flightDAO.findOldFlights();

			if (!list2.isEmpty()) {
				for (int i = 0; i < list2.size(); i++) {
					delete = flightDAO.delete(list2.get(i).getId());
				}
			}
			LOG.trace(delete);
			////

			LogicDAO logicDAO = new LogicDAO(con);
			list = logicDAO.findFlightsForUser((User) session.getAttribute("user"));

			if (list.contains(raceId)) {
				update = logicDAO.updateFlightStatus(raceId, status);
				if (!update) {
					LOG.trace("Transaction dispatcher-flight status rollback");
					Util.doRollBack(con);
					session.setAttribute(ERROR, "������ ����� �� ��� ��������");
					session.setAttribute("form", list);
					return DISPATCHER_RACE_STATUS_JSP;
				}
			} else {
				LOG.trace("Transaction dispatcher-flight status rollback");
				Util.doRollBack(con);
				session.setAttribute(ERROR, "���� �� ������");
				session.setAttribute("form", list);
				return DISPATCHER_RACE_STATUS_JSP;
			}
			con.commit();
			Util.close(con);
		} catch (Exception e) {
			session.setAttribute(ERROR, "��������� ������ � ������ ���� ������");
			LOG.error("Error in db");
			return "errors/Error500.jsp";
		}
		LOG.debug("Finishing DB operation");
		session.setAttribute("form", list);

		session.setAttribute("noError", "������ ����� ������� ��������");
		session.removeAttribute(ERROR);

		return DISPATCHER_RACE_STATUS_JSP;
	}
}
