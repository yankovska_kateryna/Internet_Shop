package ua.khpi.yankovska.finalTask.commands;

import java.util.HashMap;
import java.util.Map;

import ua.khpi.yankovska.finalTask.commands.adminFlights.CommandAddFlight;
import ua.khpi.yankovska.finalTask.commands.adminFlights.CommandDeleteFlight;
import ua.khpi.yankovska.finalTask.commands.adminFlights.CommandUpdateFlight;
import ua.khpi.yankovska.finalTask.commands.adminFlights.CommandUpdateFlightUser;
import ua.khpi.yankovska.finalTask.commands.adminStaff.CommandAddStaff;
import ua.khpi.yankovska.finalTask.commands.adminStaff.CommandDeleteStaff;
import ua.khpi.yankovska.finalTask.commands.adminStaff.CommandFindStaff;
import ua.khpi.yankovska.finalTask.commands.adminStaff.CommandUpdateStaffSurname;
import ua.khpi.yankovska.finalTask.commands.adminStaff.CommandUpdateStaffTelNum;
import ua.khpi.yankovska.finalTask.commands.application.CommandAddApplication;
import ua.khpi.yankovska.finalTask.commands.application.CommandShowTable;
import ua.khpi.yankovska.finalTask.commands.application.CommandUpdateStatus;
import ua.khpi.yankovska.finalTask.commands.authorization.CommandLogin;
import ua.khpi.yankovska.finalTask.commands.authorization.CommandRegistration;
import ua.khpi.yankovska.finalTask.commands.dispatcherFlights.CommandStaffFlights;
import ua.khpi.yankovska.finalTask.commands.dispatcherFlights.CommandUpdateStatusFlight;
import ua.khpi.yankovska.finalTask.commands.dispatcherStaff.CommandAddStaffToFlight;
import ua.khpi.yankovska.finalTask.commands.dispatcherStaff.CommandOutputBrigade;
import ua.khpi.yankovska.finalTask.commands.dispatcherStaff.CommandOutputStaffByJob;
import ua.khpi.yankovska.finalTask.commands.formInput.CommandExit;
import ua.khpi.yankovska.finalTask.commands.formInput.CommandFlightsForDispatchers;
import ua.khpi.yankovska.finalTask.commands.formInput.CommandFlightsForDispatchersInStaff;
import ua.khpi.yankovska.finalTask.commands.indexFlights.CommandCountFlights;
import ua.khpi.yankovska.finalTask.commands.indexFlights.CommandIndexFlights;
import ua.khpi.yankovska.finalTask.commands.indexFlights.CommandIndexFlightsByDate;
import ua.khpi.yankovska.finalTask.commands.indexFlights.CommandIndexFlightsByNumber;
import ua.khpi.yankovska.finalTask.commands.formInput.CommandAllDispatchers;

/**
 * Class which keep all commands in HashMap<>
 * 
 * @author Kate Yankovska
 *
 */

public class CommandList {

	private static Map<String, Command> map = new HashMap<>();

	static {
		map.put("login", new CommandLogin());
		map.put("registration", new CommandRegistration());
		map.put("inputFlight", new CommandIndexFlights());
		map.put("inputFlightNumber", new CommandIndexFlightsByNumber());
		map.put("inputFlightDate", new CommandIndexFlightsByDate());
		map.put("addFlight", new CommandAddFlight());
		map.put("deleteFlight", new CommandDeleteFlight());
		map.put("updateFlight", new CommandUpdateFlight());
		map.put("findStaff", new CommandFindStaff());
		map.put("addStaff", new CommandAddStaff());
		map.put("deleteStaff", new CommandDeleteStaff());
		map.put("updateStaffSurname", new CommandUpdateStaffSurname());
		map.put("updateStaffTelNum", new CommandUpdateStaffTelNum());
		map.put("inputAppTable", new CommandShowTable());
		map.put("changeStatus", new CommandUpdateStatus());
		map.put("updateUser", new CommandUpdateFlightUser());
		map.put("formFlightStatus", new CommandUpdateStatusFlight());
		map.put("addApp", new CommandAddApplication());
		map.put("showStaff", new CommandOutputStaffByJob());
		map.put("putStaff", new CommandAddStaffToFlight());
		map.put("getFlightStaff", new CommandStaffFlights());
		map.put("outputAllStaffFlight", new CommandOutputBrigade());

		map.put("allUsers", new CommandAllDispatchers());
		map.put("allFlights", new CommandFlightsForDispatchers());
		map.put("allFlights2", new CommandFlightsForDispatchersInStaff());
		map.put("exit", new CommandExit());
		
		map.put("inputCountFlight", new CommandCountFlights());
	}

	public static Command getCommand(String command) {
		if (map.containsKey(command)) {
			return map.get(command);
		}
		return map.get("XXX");
	}

}
