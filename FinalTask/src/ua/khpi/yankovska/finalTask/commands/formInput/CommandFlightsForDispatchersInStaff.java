package ua.khpi.yankovska.finalTask.commands.formInput;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.FlightDAO;
import ua.khpi.yankovska.finalTask.db.LogicDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Flight;
import ua.khpi.yankovska.finalTask.entity.User;

/**
 * Class which implements from interface {@link Command}.
 * It has only one method which get dispatchers' flights from the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandFlightsForDispatchersInStaff implements Command{
	
	private static final String INPUT_ERROR = "inputError";
	private static final Logger LOG = Logger.getLogger(CommandFlightsForDispatchersInStaff.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to get dispatchers' flights from the database.
     *
     * @return a dispatcherStaff.jsp result page if request is fulfilled
     * 	or an Error500.jsp page if there is an exception.
     * 
    */
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);
		
		session.removeAttribute("form");
		
		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();

		List<Integer> list = null;
		List<Flight> list2 = null;
		boolean delete = false;
		
		try {
			Connection con = dbManager.getConnection();
			
			
			//DELETE OLD FLIGHT
			FlightDAO flightDAO = new FlightDAO(con);
			list2 = flightDAO.findOldFlights();
			
			if (!list2.isEmpty()) {
				for (int i = 0; i < list2.size(); i++) {
					delete = flightDAO.delete(list2.get(i).getId());
				}	
			}
			LOG.trace(delete);
			////
			
			
			LogicDAO logicDAO = new LogicDAO(con);
			list = logicDAO.findFlightsForUser((User) session.getAttribute("user"));
			Util.close(con);
		} catch (Exception e) {
			session.setAttribute(INPUT_ERROR, "��������� ������ � ������ ���� ������");
			LOG.error("Error in db");
			return "errors/Error500.jsp";
		}
		LOG.trace(list);
		LOG.debug("Finishing DB operation");
		
		if (list.isEmpty()) {
			session.setAttribute(INPUT_ERROR, "� ���������� ��� �������� �������!");
			LOG.trace("List of flights is empty");
			return "dispatcherStaff.jsp";
		} else {
			session.removeAttribute(INPUT_ERROR);

			session.setAttribute("form", list);
			LOG.trace(session.getAttribute("form"));
				
			LOG.trace("List of applications is full");
			return "dispatcherStaff.jsp";	
		}
	}

}
