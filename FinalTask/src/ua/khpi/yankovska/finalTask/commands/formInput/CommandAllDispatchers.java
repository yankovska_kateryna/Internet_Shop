package ua.khpi.yankovska.finalTask.commands.formInput;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.UserDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.User;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which get all dispatchers from the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandAllDispatchers implements Command {

	private static final String FORM2 = "form2";
	private static final Logger LOG = Logger.getLogger(CommandAllDispatchers.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to get all dispatchers from the
	 * database.
	 *
	 * @return an adminRaces.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession(false);

		session.removeAttribute(FORM2);

		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();

		List<User> list = null;

		try {
			Connection con = dbManager.getConnection();
			UserDAO userDAO = new UserDAO(con);
			list = userDAO.findAll();
			Util.close(con);
		} catch (Exception e) {
			session.setAttribute("Error", "��������� ������ � ������ ���� ������");
			LOG.error("Error in db");
			return "errors/Error500.jsp";
		}
		LOG.trace(list);
		LOG.debug("Finishing DB operation");

		session.setAttribute(FORM2, list);
		LOG.trace(session.getAttribute(FORM2));

		LOG.trace("List of applications is full");
		return "adminRaces.jsp";

	}

}
