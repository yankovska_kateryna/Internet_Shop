package ua.khpi.yankovska.finalTask.commands.formInput;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.khpi.yankovska.finalTask.commands.Command;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which cleans data of the user session.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandExit implements Command {

	/**
	 * Returns a jsp page. Fulfills the request to clean data of the user session.
	 *
	 * @return an index.jsp.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		Enumeration<String> str = session.getAttributeNames();

		while (str.hasMoreElements()) {
			String string = str.nextElement();
			session.removeAttribute(string);
		}
		return "index.jsp";
	}

}
