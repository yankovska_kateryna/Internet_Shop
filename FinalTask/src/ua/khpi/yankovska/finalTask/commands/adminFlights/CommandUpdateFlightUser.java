package ua.khpi.yankovska.finalTask.commands.adminFlights;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.FlightDAO;
import ua.khpi.yankovska.finalTask.db.LogicDAO;
import ua.khpi.yankovska.finalTask.db.UserDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Flight;
import ua.khpi.yankovska.finalTask.entity.User;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which update flight's dispatcher who is responsible for the flight in the
 * database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandUpdateFlightUser implements Command {

	private static final String ADMIN_RACES_JSP = "adminRaces.jsp";
	private static final String ERROR = "Error";
	private static final Logger LOG = Logger.getLogger(CommandUpdateFlightUser.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to update flight's dispatcher who is
	 * responsible for the flight in the database.
	 * 
	 * @return an adminRaces.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		session.removeAttribute("noError");

		int flightId = Integer.parseInt(request.getParameter("race4"));
		String disp = request.getParameter("dispatcher");

		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();

		boolean update = false;
		List<Flight> list = null;
		boolean delete = false;

		try {
			Connection con = dbManager.getConnection();
			con.setAutoCommit(false);

			// Delete old flights
			FlightDAO flightDAO = new FlightDAO(con);
			list = flightDAO.findOldFlights();

			if (!list.isEmpty()) {
				for (int i = 0; i < list.size(); i++) {
					delete = flightDAO.delete(list.get(i).getId());
				}
			}
			LOG.trace(delete);
			//////

			UserDAO userDAO = new UserDAO(con);
			User user = userDAO.getUser(disp);

			if (user == null) {
				LOG.trace("Transaction find dispatcher rollback");
				Util.doRollBack(con);
				session.setAttribute(ERROR, "���� �� ��� �������");
				return ADMIN_RACES_JSP;
			}

			LogicDAO logicDAO = new LogicDAO(con);
			update = logicDAO.update(flightId, user);
			LOG.trace(update);

			if (!update) {
				LOG.trace("Transaction dispatcher-flight rollback");
				Util.doRollBack(con);
				session.setAttribute(ERROR, "���� �� ��� �������");
				return ADMIN_RACES_JSP;
			}

			con.commit();
			Util.close(con);
		} catch (Exception e) {
			LOG.error("Error in updating flight");
			return "errors/Error500.jsp";
		}
		LOG.debug("Finishing DB operation");
		session.setAttribute("noError", "���� �������");
		session.removeAttribute(ERROR);
		return ADMIN_RACES_JSP;
	}
}
