package ua.khpi.yankovska.finalTask.commands.adminFlights;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.FlightDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Flight;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which update flight's date/time departure and date/time arrival in the
 * database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandUpdateFlight implements Command {

	private static final String ADMIN_RACES_JSP = "adminRaces.jsp";
	private static final String ERROR = "Error";
	private static final String ERRORS_ERROR500_JSP = "errors/Error500.jsp";
	private static final Logger LOG = Logger.getLogger(CommandUpdateFlight.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to update flight's date/time
	 * departure and date/time arrival in the database.
	 * 
	 * @return an adminRaces.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession(false);

		session.removeAttribute("noError");

		int id = Integer.parseInt(request.getParameter("race3"));
		String flightDate = request.getParameter("date3");
		String flightArrival = request.getParameter("date4");

		LOG.trace("Getting id/flightDate/flightArrival: " + id + "/" + flightDate + "/" + flightArrival);

		Date date = new Date();
		LOG.trace(date);
		Date fDate = null;
		try {
			fDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse(flightDate);
			// && ((fDate.getTime()/1000-date.getTime()/1000) > 1800)
			if (fDate.after(date) && (((fDate.getTime() / 1000) - (date.getTime() / 1000)) > 120)) {
				LOG.trace("Flight can be updated");
			} else {
				LOG.trace("Flight can not be updated");
				session.setAttribute(ERROR, "���� �� ����� ���� �������");
				return ADMIN_RACES_JSP;
			}
		} catch (ParseException e1) {
			LOG.error("Parsing date problem");
			return ERRORS_ERROR500_JSP;
		}

		Date aDate = null;
		try {
			aDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse(flightArrival);
			// && ((aDate.getTime()/1000-fDate.getTime()/1000)>=1800)
			if (aDate.after(fDate)) {
				LOG.trace("Flight can be updated");
			} else {
				LOG.trace("Flight can not be updated");
				session.setAttribute(ERROR, "���� �� ����� ������ ����� 30 �����");
				return ADMIN_RACES_JSP;
			}
		} catch (ParseException e1) {
			LOG.error("Parsing date problem");
			return ERRORS_ERROR500_JSP;
		}

		LOG.error(date + "/" + fDate);

		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();
		boolean updating = false;

		List<Flight> list = null;
		boolean delete = false;

		List<Flight> changeFlight = null;

		LOG.trace("++++++++++");
		try {
			Connection con = dbManager.getConnection();
			FlightDAO flightDAO = new FlightDAO(con);

			LOG.trace("--------");
			// Delete old flights
			list = flightDAO.findOldFlights();

			LOG.trace(list);

			if (!list.isEmpty()) {
				for (int i = 0; i < list.size(); i++) {
					delete = flightDAO.delete(list.get(i).getId());
				}
			}
			LOG.trace(delete);
			//////

			Date dateFlight = null;
			Date dateArrival = null;

			changeFlight = flightDAO.getFlightByFlightNumber(id);
			dateFlight = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(changeFlight.get(0).getFlightDate());
			LOG.trace(dateFlight);
			LOG.trace(fDate);

			dateArrival = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(changeFlight.get(0).getFlightArrival());
			if ((dateArrival.getTime() / 1000 - dateFlight.getTime() / 1000) != (aDate.getTime() / 1000
					- fDate.getTime() / 1000)) {
				session.setAttribute(ERROR, "������������ ������ �� ����� ���� ��������� ��� ���������");
				return ADMIN_RACES_JSP;
			}

			if (fDate.before(dateFlight) && aDate.before(dateArrival)) {
				session.setAttribute(ERROR, "���� �� ����� ���� ������� �� ������");
				return ADMIN_RACES_JSP;
			}

			if ((fDate.getTime() / 1000 - dateFlight.getTime() / 1000) < 1800) {
				session.setAttribute(ERROR, "���� �� ����� ���� ������� ����� ��� �� 30 �����");
				return ADMIN_RACES_JSP;
			}

			if ((dateFlight.getTime() / 1000 <= date.getTime() / 1000)) {
				session.setAttribute(ERROR, "���� � ������");
				return ADMIN_RACES_JSP;
			}

			if (((dateFlight.getTime() / 1000) - (date.getTime() / 1000)) < 120) {
				session.setAttribute(ERROR, "���� �� ����� ���� ������� �� 2 ������ �� ������");
				return ADMIN_RACES_JSP;
			}
			updating = flightDAO.updateDates(flightDate, flightArrival, id);
			LOG.trace(updating);
			Util.close(con);
		} catch (Exception e) {
			LOG.error("Error in updating flight");
			return ERRORS_ERROR500_JSP;
		}
		LOG.debug("Finishing DB operation");

		if (updating == false) {
			session.setAttribute(ERROR, "����� ��� ����� ������� ��� � ����");
			return ADMIN_RACES_JSP;
		} else {
			session.setAttribute("noError", "���� �������");
			session.removeAttribute(ERROR);
			return ADMIN_RACES_JSP;
		}
	}

}
