package ua.khpi.yankovska.finalTask.commands.adminFlights;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.FlightDAO;
import ua.khpi.yankovska.finalTask.db.Util;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which delete flight from the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandDeleteFlight implements Command {

	private static final Logger LOG = Logger.getLogger(CommandDeleteFlight.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to delete flight from the database.
	 *
	 * @return an adminRaces.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		session.removeAttribute("noError");

		int id = Integer.parseInt(request.getParameter("race2"));

		LOG.trace("Getting id: " + id);
		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();
		boolean deleting = false;

		try {
			Connection con = dbManager.getConnection();
			FlightDAO flightDAO = new FlightDAO(con);
			deleting = flightDAO.delete(id);
			Util.close(con);
		} catch (Exception e) {
			LOG.error("Error in deleting flight");
			return "errors/Error500.jsp";
		}
		LOG.debug("Finishing DB operation");

		if (deleting == false) {
			session.setAttribute("Error", "����� ��� ����� ������� ��� � ����");
			return "adminRaces.jsp";
		} else {
			session.setAttribute("noError", "���� ��� ������");
			session.removeAttribute("Error");
			return "adminRaces.jsp";
		}
	}
}
