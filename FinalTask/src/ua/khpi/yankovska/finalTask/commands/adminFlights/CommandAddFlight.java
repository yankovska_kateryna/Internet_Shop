package ua.khpi.yankovska.finalTask.commands.adminFlights;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.FlightDAO;
import ua.khpi.yankovska.finalTask.db.LogicDAO;
import ua.khpi.yankovska.finalTask.db.UserDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Flight;
import ua.khpi.yankovska.finalTask.entity.User;

/**
 * Class which implements from interface {@link Command}. It has method which
 * add flight to the database and method which work with input names.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandAddFlight implements Command {

	private static final String ADMIN_RACES_JSP = "adminRaces.jsp";
	private static final String REG_EXP = "[\\s]{2,}";
	private static final String ERRORS_ERROR500_JSP = "errors/Error500.jsp";
	private static final String ERROR = "Error";
	
	private static final Logger LOG = Logger.getLogger(CommandAddFlight.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to add flight to the database.
	 *
	 * @return an adminRaces.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		session.removeAttribute("noError");

		String fromCity = request.getParameter("fromCity2").trim();
		String fromCountry = request.getParameter("fromCountry2").trim();
		String toCity = request.getParameter("toCity2").trim();
		String toCountry = request.getParameter("toCountry2").trim();
		String flightDate = request.getParameter("date2");
		String dispatcher = request.getParameter("dispatcher");
		String flightArrival = request.getParameter("arrival");

		fromCity = fromCity.replaceAll(REG_EXP, " ");
		fromCountry = fromCountry.replaceAll(REG_EXP, " ");
		toCity = toCity.replaceAll(REG_EXP, " ");
		toCountry = toCountry.replaceAll(REG_EXP, " ");

		fromCity = fromCity.substring(0, 1).toUpperCase() + fromCity.substring(1);
		fromCountry = fromCountry.substring(0, 1).toUpperCase() + fromCountry.substring(1);
		toCity = toCity.substring(0, 1).toUpperCase() + toCity.substring(1);
		toCountry = toCountry.substring(0, 1).toUpperCase() + toCountry.substring(1);

		fromCity = capitalize(fromCity);
		fromCountry = capitalize(fromCountry);
		toCity = capitalize(toCity);
		toCountry = capitalize(toCountry);

		if (fromCity.equals(toCity)) {
			session.setAttribute(ERROR, "�������� ������� �� ����� ���� �����������");
			LOG.warn("Cities are the same");
			return ADMIN_RACES_JSP;
		}

		LOG.trace("Getting fromCity/fromCountry/toCity/toCountry/flightDate/dispatcher: " + fromCity + "/" + fromCountry
				+ "/" + toCity + "/" + toCountry + "/" + flightDate + "/" + dispatcher);

		Date date = new Date();
		LOG.trace(date);
		Date fDate = null;
		try {
			fDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse(flightDate);
			// && ((fDate.getTime()/1000-date.getTime()/1000) > 18000)
			if (fDate.after(date) && ((fDate.getTime()/1000)-(date.getTime()/1000) > 180)) {
				LOG.trace("Flight can be added");
			} else {
				LOG.trace("Flight can not be added");
				session.setAttribute(ERROR,
						"���� �� ����� ���� �������� � ������� ���� ��� � ����������� ������� 3 ����� �� ������");
				return ADMIN_RACES_JSP;
			}
		} catch (ParseException e1) {
			LOG.error("Parsing date problem");
			return ERRORS_ERROR500_JSP;
		}

		LOG.error(date + "/" + fDate);

		Date aDate = null;
		try {
			aDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse(flightArrival);
			// && ((aDate.getTime()/1000-fDate.getTime()/1000)>=1800)
			if (aDate.after(fDate) && (((aDate.getTime()/1000)-(fDate.getTime()/1000))>=300)) {
				LOG.trace("Flight can be added");
			} else {
				LOG.trace("Flight can not be added");
				session.setAttribute(ERROR, "���� �� ����� ������ ����� 5 �����");
				return ADMIN_RACES_JSP;
			}
		} catch (ParseException e1) {
			LOG.error("Parsing date problem");
			return ERRORS_ERROR500_JSP;
		}

		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();
		boolean adding = false;
		boolean userflightAdding = false;

		try {
			Connection con = dbManager.getConnection();
			con.setAutoCommit(false);

			UserDAO userDAO = new UserDAO(con);
			User user = userDAO.getUser(dispatcher);

			if (user == null) {
				LOG.trace("Transaction find dispatcher rollback");
				Util.doRollBack(con);
				session.setAttribute(ERROR, "��������� �� ������");
				return ADMIN_RACES_JSP;
			}

			FlightDAO flightDAO = new FlightDAO(con);
			adding = flightDAO
					.create(Flight.createFlight(fromCity, fromCountry, toCity, toCountry, flightDate, flightArrival));

			if (!adding) {
				LOG.trace("Transaction add flight rollback");
				Util.doRollBack(con);
				session.setAttribute(ERROR, "���� �� ��������");
				return ADMIN_RACES_JSP;
			}

			int maxFlightNum = flightDAO.getMaxFlight();

			if (maxFlightNum == 0) {
				LOG.trace("Transaction add flight rollback");
				Util.doRollBack(con);
				session.setAttribute(ERROR, "���� �� ��������");
				return ADMIN_RACES_JSP;
			}

			LogicDAO logicDAO = new LogicDAO(con);
			userflightAdding = logicDAO.setFlightsForUser(user, maxFlightNum);

			if (!userflightAdding) {
				LOG.trace("Transaction dispatcher-flight rollback");
				Util.doRollBack(con);
				session.setAttribute(ERROR, "���� �� ��� ��������");
				return ADMIN_RACES_JSP;
			}

			con.commit();
			Util.close(con);
		} catch (Exception e) {
			LOG.error("Error in adding flight");
			return ERRORS_ERROR500_JSP;
		}
		LOG.debug("Finishing DB operation");
		session.setAttribute("noError", "���� ��������");
		session.removeAttribute(ERROR);
		return ADMIN_RACES_JSP;
	}

	/**
	 * Returns a string. If the input contains from two words make this two words
	 * upper case.
	 *
	 * @return an adminRaces.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	private static String capitalize(String text) {
		StringBuilder sb = new StringBuilder();
		sb.append(Character.toString(text.charAt(0)).toUpperCase());

		for (int i = 1; i < text.length(); i++) {
			if (Character.toString(text.charAt(i - 1)).equals(" ")) {
				sb.append(Character.toString(text.charAt(i)).toUpperCase());
			} else {
				sb.append(Character.toString(text.charAt(i)));
			}
		}
		return sb.toString();
	}
}
