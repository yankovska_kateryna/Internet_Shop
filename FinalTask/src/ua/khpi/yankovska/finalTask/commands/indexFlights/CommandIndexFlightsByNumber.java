package ua.khpi.yankovska.finalTask.commands.indexFlights;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.FlightDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Flight;
import ua.khpi.yankovska.finalTask.entity.User;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which get flights by input parameters from the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandIndexFlightsByNumber implements Command {

	private static final String INPUT_ERROR = "inputError";
	private static final Logger LOG = Logger.getLogger(CommandIndexFlightsByNumber.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to get get flights by input
	 * parameters from the database.
	 *
	 * @return .jsp result page depending on what role a person enters the system if
	 *         request is fulfilled or an Error500.jsp page if there is an
	 *         exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		int id;

		try {
			id = Integer.parseInt(request.getParameter("race"));
		} catch (NumberFormatException e) {
			LOG.warn("Incorrect input");
			session.setAttribute(INPUT_ERROR, "�������� ����");
			return whoIsWho(session);
		}

		LOG.trace("Getting id: " + id);

		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();
		List<Flight> list = null;

		List<Flight> list2 = null;
		boolean delete = false;

		try {
			Connection con = dbManager.getConnection();
			FlightDAO flightDAO = new FlightDAO(con);

			// DELETE OLD FLIGHT
			list2 = flightDAO.findOldFlights();

			if (!list2.isEmpty()) {
				for (int i = 0; i < list2.size(); i++) {
					delete = flightDAO.delete(list2.get(i).getId());
				}
			}
			LOG.trace(delete);
			////

			list = flightDAO.getFlightByFlightNumber(id);
			LOG.trace(list);
			Util.close(con);
		} catch (Exception e) {
			session.setAttribute(INPUT_ERROR, "��������� ������ � ������ ���� ������");
			LOG.error("Error in db");

			return "errors/Error500.jsp";
		}
		LOG.debug("Finishing DB operation");

		if (list.isEmpty()) {
			session.setAttribute(INPUT_ERROR, "������� �� ����������! ����������, �������� ���������!");
			LOG.trace("There is no flight");

			return whoIsWho(session);
		} else {
			DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			for (int i = 0; i < list.size(); i++) {
				try {
					Date date = inputFormat.parse(list.get(i).getFlightDate());
					list.get(i).setFlightDate(outputFormat.format(date));
					date = inputFormat.parse(list.get(i).getFlightArrival());
					list.get(i).setFlightArrival(outputFormat.format(date));
				} catch (ParseException e) {
					LOG.trace("Output formatting");
					return "errors/Error500.jsp";
				}
			}

			session.removeAttribute(INPUT_ERROR);
			request.setAttribute("flights", list);
			LOG.trace("Flight is there");

			return whoIsWho(session);
		}
	}

	private String whoIsWho(HttpSession session) {
		LOG.trace(session.getAttribute("user"));
		User user = (User) session.getAttribute("user");
		if (user == null) {
			return "index.jsp";
		} else if (user.getLogin().equals("admin")) {
			return "adminRaces.jsp";
		} else {
			return "dispatcherFlights.jsp";
		}
	}
}
