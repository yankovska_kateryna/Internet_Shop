package ua.khpi.yankovska.finalTask.commands.indexFlights;

import java.sql.Connection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.FlightDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Flight;

public class CommandCountFlights implements Command{
	private static final String INPUT_ERROR = "inputError";
	private static final Logger LOG = Logger.getLogger(CommandCountFlights.class.getName());
	private static final String PATTERN_CITY = "\\w+[\\,|\\.|\\s]";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		String fromCity = null;
		String toCity = null;

		try {
			fromCity = request.getParameter("from");
			toCity = request.getParameter("to");
			if (fromCity == null || toCity == null) {
				session.setAttribute(INPUT_ERROR, "��������� ��� ����!");
				return "index.jsp";
			} else if (fromCity == "" || toCity == "") {
				session.setAttribute(INPUT_ERROR, "������� ������!");
				return "index.jsp";
			}
		} catch (NullPointerException e) {
			LOG.warn("Incorrect input");
			session.setAttribute(INPUT_ERROR, "�������� ����");
			return "index.jsp";
		}

		LOG.trace("Getting raceNumber/fromCity/toCity: " + fromCity + "/" + toCity);

		Pattern p = Pattern.compile(PATTERN_CITY, Pattern.UNICODE_CHARACTER_CLASS);
		Matcher m = p.matcher(fromCity);

		while (m.find()) {
			fromCity = m.group();
			fromCity = fromCity.substring(0, fromCity.length() - 1);
		}

		m = p.matcher(toCity);

		while (m.find()) {
			toCity = m.group();
			toCity = toCity.substring(0, toCity.length() - 1);
		}

		LOG.trace("Getting fromCity/toCity: " + "/" + fromCity + "/" + toCity);

		if (fromCity.equals(toCity)) {
			session.setAttribute(INPUT_ERROR, "�������� ������� ������ ���� �� ����� ���� ����������");
			LOG.warn("Cities are the same");
			return "index.jsp";
		}

		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();

		List<Flight> list = null;
		List<Flight> list2 = null;
		boolean delete = false;

		try {
			Connection con = dbManager.getConnection();
			FlightDAO flightDAO = new FlightDAO(con);

			// DELETE OLD FLIGHT
			list2 = flightDAO.findOldFlights();

			if (!list2.isEmpty()) {
				for (int i = 0; i < list2.size(); i++) {
					delete = flightDAO.delete(list2.get(i).getId());
				}
			}
			LOG.trace(delete);
			////

			list = flightDAO.findCountFlights(fromCity, toCity);
			LOG.trace(list);
			Util.close(con);
		} catch (Exception e) {
			session.setAttribute(INPUT_ERROR, "��������� ������ � ������ ���� ������");
			LOG.error("Error in db");
			return "errors/Error500.jsp";
		}

		LOG.debug("Finishing DB operation");

		if (list.isEmpty()) {
			session.setAttribute(INPUT_ERROR, "������� �� ����������! ����������, �������� ���������!");
			LOG.trace("List of flights is empty");
			return "index.jsp";
		} 
		
		LOG.trace(list);
		
		session.removeAttribute(INPUT_ERROR);

		request.setAttribute("flightsCount", list);

		LOG.trace("List of flights is full");
		return "index.jsp";
	}
}
