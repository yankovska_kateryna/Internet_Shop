package ua.khpi.yankovska.finalTask.commands.indexFlights;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.FlightDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Flight;
import ua.khpi.yankovska.finalTask.entity.User;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which get flights by input parameters from the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandIndexFlights implements Command {

	private static final String INPUT_ERROR = "inputError";
	private static final Logger LOG = Logger.getLogger(CommandIndexFlights.class.getName());
	private static final String PATTERN_CITY = "\\w+[\\,|\\.|\\s]";

	/**
	 * Returns a jsp page. Fulfills the request to get flights by input parameters
	 * from the database.
	 *
	 * @return .jsp result page depending on what role a person enters the system if
	 *         request is fulfilled or an Error500.jsp page if there is an
	 *         exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		String fromCity = null;
		String toCity = null;
		String flightDate = null;

		try {
			fromCity = request.getParameter("from");
			toCity = request.getParameter("to");
			flightDate = request.getParameter("date1");
			if (fromCity == null || toCity == null || flightDate == null) {
				session.setAttribute(INPUT_ERROR, "��������� ��� ����!");
				return whoIsWho(session);
			} else if (fromCity == "" || toCity == "" || flightDate == "") {
				session.setAttribute(INPUT_ERROR, "������� ������!");
				return whoIsWho(session);
			}
		} catch (NullPointerException e) {
			LOG.warn("Incorrect input");
			session.setAttribute(INPUT_ERROR, "�������� ����");
			return whoIsWho(session);
		}

		LOG.trace("Getting raceNumber/fromCity/toCity/flightDate: " + fromCity + "/" + toCity + "/" + flightDate);

		Pattern p = Pattern.compile(PATTERN_CITY, Pattern.UNICODE_CHARACTER_CLASS);
		Matcher m = p.matcher(fromCity);

		while (m.find()) {
			fromCity = m.group();
			fromCity = fromCity.substring(0, fromCity.length() - 1);
		}

		m = p.matcher(toCity);

		while (m.find()) {
			toCity = m.group();
			toCity = toCity.substring(0, toCity.length() - 1);
		}

		LOG.trace("Getting raceNumber/fromCity/toCity/flightDate: " + "/" + fromCity + "/" + toCity + "/" + flightDate);

		if (fromCity.equals(toCity)) {
			session.setAttribute(INPUT_ERROR, "�������� ������� ������ ���� �� ����� ���� ����������");
			LOG.warn("Cities are the same");
			return whoIsWho(session);
		}

		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();

		List<Flight> list = null;
		List<Flight> list2 = null;
		boolean delete = false;

		try {
			Connection con = dbManager.getConnection();
			FlightDAO flightDAO = new FlightDAO(con);

			// DELETE OLD FLIGHT
			list2 = flightDAO.findOldFlights();

			if (!list2.isEmpty()) {
				for (int i = 0; i < list2.size(); i++) {
					delete = flightDAO.delete(list2.get(i).getId());
				}
			}
			LOG.trace(delete);
			////

			list = flightDAO.findAllFlights(fromCity, toCity, flightDate);
			LOG.trace(list);
			Util.close(con);
		} catch (Exception e) {
			session.setAttribute(INPUT_ERROR, "��������� ������ � ������ ���� ������");
			LOG.error("Error in db");
			return "errors/Error500.jsp";
		}

		LOG.debug("Finishing DB operation");

		if (list.isEmpty()) {
			session.setAttribute(INPUT_ERROR, "������� �� ����������! ����������, �������� ���������!");
			LOG.trace("List of flights is empty");
			return whoIsWho(session);
		} else {
			DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			for (int i = 0; i < list.size(); i++) {
				try {
					Date date = inputFormat.parse(list.get(i).getFlightDate());
					Date date1 = inputFormat.parse(list.get(i).getFlightArrival());
					list.get(i).setFlightDate(outputFormat.format(date));
					list.get(i).setFlightArrival(outputFormat.format(date1));
				} catch (ParseException e) {
					LOG.trace("Output formatting");
					return "errors/Error500.jsp";
				}
			}
			session.removeAttribute(INPUT_ERROR);

			request.setAttribute("flights", list);

			LOG.trace("List of flights is full");
			return whoIsWho(session);
		}
	}

	private String whoIsWho(HttpSession session) {
		LOG.trace(session.getAttribute("user"));
		User user = (User) session.getAttribute("user");
		if (user == null) {
			return "index.jsp";
		} else if (user.getLogin().equals("admin")) {
			return "adminRaces.jsp";
		} else {
			return "dispatcherRaces.jsp";
		}
	}
}
