package ua.khpi.yankovska.finalTask.commands.adminStaff;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.StaffDAO;
import ua.khpi.yankovska.finalTask.db.Util;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which update personnel's telephone number in the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandUpdateStaffTelNum implements Command {

	private static final Logger LOG = Logger.getLogger(CommandUpdateStaffTelNum.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to update personnel's telephone
	 * number in the database.
	 *
	 * @return an adminWorkers.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		session.removeAttribute("noError");

		int id = Integer.parseInt(request.getParameter("id3"));
		String telNum = request.getParameter("telNum2").trim();

		telNum = telNum.replaceAll("[\\s]{1,}", "");

		LOG.trace("Getting id/telNum: " + id + "/" + telNum);

		LOG.debug("Starting db operation");

		DBManager dbManager = DBManager.getInstance();
		boolean updatingTelNum = false;

		try {
			Connection con = dbManager.getConnection();
			StaffDAO staffDAO = new StaffDAO(con);
			updatingTelNum = staffDAO.updateStaffTelNum(telNum, id);

			LOG.trace(updatingTelNum);
			Util.close(con);
		} catch (Exception e) {
			LOG.error("Error in updating staff telephone number");
			return "errors/Error500.jsp";
		}

		LOG.debug("Finishing DB operation");

		if (updatingTelNum == false) {
			session.setAttribute("Error", "���������� ��� ����� ������� ��� � ����");
			return "adminWorkers.jsp";
		} else {
			session.setAttribute("noError", "��������� �������");
			session.removeAttribute("Error");
			return "adminWorkers.jsp";
		}
	}

}
