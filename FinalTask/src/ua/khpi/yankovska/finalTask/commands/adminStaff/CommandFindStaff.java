package ua.khpi.yankovska.finalTask.commands.adminStaff;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.StaffDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Staff;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which get personnel from the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandFindStaff implements Command {

	private static final String INPUT_ERROR = "inputError";
	private static final String ADMIN_WORKERS_JSP = "adminWorkers.jsp";
	private static final String REG_EXP = "[\\s]{1,}";
	private static final Logger LOG = Logger.getLogger(CommandFindStaff.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to get personnel by parameters from
	 * the database.
	 *
	 * @return an adminWorkers.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		String name = request.getParameter("name").trim();
		String surname = request.getParameter("surname").trim();
		String position = request.getParameter("position").trim();

		name = name.replaceAll(REG_EXP, "");
		surname = surname.replaceAll(REG_EXP, "");
		position = position.replaceAll(REG_EXP, "");

		name = name.substring(0, 1).toUpperCase() + name.substring(1);
		surname = surname.substring(0, 1).toUpperCase() + surname.substring(1);
		position = position.substring(0, 1).toUpperCase() + position.substring(1);

		LOG.trace("Getting name/surname/position: " + name + "/" + surname + "/" + position);

		if (name == null || surname == null || position == null) {
			session.setAttribute(INPUT_ERROR, "����������, ��������� ��� ��������!");
			return ADMIN_WORKERS_JSP;
		} else if (name == "" || surname == "" || position == "") {
			session.setAttribute(INPUT_ERROR, "����������, ������� ������!");
			return ADMIN_WORKERS_JSP;
		}

		LOG.debug("Starting db operation");

		DBManager dbManager = DBManager.getInstance();
		List<Staff> list = null;

		try {
			Connection con = dbManager.getConnection();
			StaffDAO staffDAO = new StaffDAO(con);
			list = staffDAO.getStaffByParameters(position, surname, name);
			Util.close(con);
		} catch (Exception e) {
			session.setAttribute(INPUT_ERROR, "��������� ������ � ������ ���� ������");
			LOG.error("Error in db");
			return "errors/Error500.jsp";
		}

		if (list.isEmpty()) {
			session.setAttribute(INPUT_ERROR, "������ ���������� ���! �������� ���������!");
			LOG.trace("List of flights is empty");
			return ADMIN_WORKERS_JSP;
		} else {
			session.removeAttribute(INPUT_ERROR);

			request.setAttribute("staff", list);

			LOG.trace("List of flights is full");
			return ADMIN_WORKERS_JSP;
		}
	}
}
