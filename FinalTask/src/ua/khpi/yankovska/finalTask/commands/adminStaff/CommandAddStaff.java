package ua.khpi.yankovska.finalTask.commands.adminStaff;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.StaffDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Staff;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which add personnel to the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandAddStaff implements Command {

	private static final String ADMIN_WORKERS_JSP = "adminWorkers.jsp";
	private static final String REG_EXP = "[\\s]{1,}";
	private static final String ERROR = "Error";
	private static final Logger LOG = Logger.getLogger(CommandAddStaff.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to add personnel to the database.
	 *
	 * @return an adminWorkers.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		session.removeAttribute("noError");

		String name = request.getParameter("name2").trim();
		String surname = request.getParameter("surname2").trim();
		String position = request.getParameter("position2").trim();
		String telNum = request.getParameter("telNum").trim();

		name = name.replaceAll(REG_EXP, "");
		surname = surname.replaceAll(REG_EXP, "");
		telNum = telNum.replaceAll(REG_EXP, "");

		name = name.substring(0, 1).toUpperCase() + name.substring(1);
		surname = surname.substring(0, 1).toUpperCase() + surname.substring(1);

		LOG.trace("Getting name/surname/position/telNum: " + name + "/" + surname + "/" + position + "/" + telNum);

		if (name == null || surname == null || position == null || telNum == null) {
			session.setAttribute(ERROR, "��������� ��� ����!");
			return ADMIN_WORKERS_JSP;
		} else if (name == "" || surname == "" || position == "" || telNum == "") {
			session.setAttribute(ERROR, "������� ������!");
			return ADMIN_WORKERS_JSP;
		}

		LOG.debug("Starting db operation");

		DBManager dbManager = DBManager.getInstance();
		boolean adding = false;

		try {
			Connection con = dbManager.getConnection();
			StaffDAO staffDAO = new StaffDAO(con);
			adding = staffDAO.create(Staff.createStaff(name, surname, position, telNum));
			Util.close(con);
		} catch (Exception e) {
			LOG.error("Error in adding staff");
			return "errors/Error500.jsp";
		}

		LOG.debug("Finishing DB operation");

		if (adding == false) {
			session.setAttribute(ERROR, "����� �������� ������ �� �����");
			return ADMIN_WORKERS_JSP;
		} else {
			session.setAttribute("noError", "��������� ��������");
			session.removeAttribute(ERROR);

			return ADMIN_WORKERS_JSP;
		}
	}
}
