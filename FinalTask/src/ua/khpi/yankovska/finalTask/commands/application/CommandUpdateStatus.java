package ua.khpi.yankovska.finalTask.commands.application;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.ApplicationDAO;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Application;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which update statuses of applications.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandUpdateStatus implements Command {

	private static final String ADMIN_APPLICATION_JSP = "adminApplication.jsp";
	private static final String ERROR = "Error";
	private static final Logger LOG = Logger.getLogger(CommandUpdateStatus.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to update statuses of applications.
	 *
	 * @return an adminApplication.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		session.removeAttribute("app");
		session.removeAttribute("noError");

		int id = Integer.parseInt(request.getParameter("appId"));
		String status = request.getParameter("status");

		LOG.trace(status + " " + id);

		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();

		int count = 0;
		boolean update = false;
		List<Application> list = null;

		try {
			Connection con = dbManager.getConnection();

			ApplicationDAO appDAO = new ApplicationDAO(con);

			list = appDAO.findAll();
			LOG.trace(list);
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getId() == id) {
					update = appDAO.update(status, id);
					LOG.trace(update);
					list = appDAO.findAll();
					if (list.isEmpty()) {
						LOG.trace("List is empty");
						session.setAttribute(ERROR, "�������� ������ ���");
						return ADMIN_APPLICATION_JSP;
					}

					if (!update) {
						LOG.trace("Transaction update status application rollback");
						session.setAttribute(ERROR, "������ ������ �� ��������");
						session.setAttribute("app", list);
						return ADMIN_APPLICATION_JSP;
					}
					count++;
					break;
				}
			}

			if (count != 1) {
				LOG.trace("Transaction application status rollback");
				session.setAttribute(ERROR, "������ � ����� ������� ���!");
				session.setAttribute("app", list);
				return ADMIN_APPLICATION_JSP;
			}
			Util.close(con);
		} catch (Exception e) {
			session.setAttribute(ERROR, "��������� ������ � ������ ���� ������");
			session.setAttribute("app", list);
			return "errors/Error500.jsp";
		}

		LOG.debug("Finishing DB operation");
		session.setAttribute("app", list);
		session.setAttribute("noError", "������ ������ ��������");
		session.removeAttribute(ERROR);

		return ADMIN_APPLICATION_JSP;
	}

}
