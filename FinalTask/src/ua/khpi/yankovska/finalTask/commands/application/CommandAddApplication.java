package ua.khpi.yankovska.finalTask.commands.application;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.ApplicationDAO;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Application;
import ua.khpi.yankovska.finalTask.entity.User;
import ua.khpi.yankovska.finalTask.mail.CreateMailContent;
import ua.khpi.yankovska.finalTask.mail.Message;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which add application to the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandAddApplication implements Command {

	private static final Logger LOG = Logger.getLogger(CommandAddApplication.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to add application to the database.
	 *
	 * @return an dispatcherApplication.jsp result page if request is fulfilled or
	 *         an Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		session.removeAttribute("noError");

		String password = request.getParameter("psw");
		String comment = request.getParameter("comment");

		LOG.trace(password);

		Date dateNow = new Date();
		String date = new SimpleDateFormat("yyyy-MM-dd").format(dateNow);

		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();
		boolean adding = false;

		User user = (User) session.getAttribute("user");

		try {
			Connection con = dbManager.getConnection();
			ApplicationDAO applicationDAO = new ApplicationDAO(con);
			adding = applicationDAO.create(Application.createApplication(user.getId(), user.getLogin(), date, comment));
			Util.close(con);
		} catch (Exception e) {
			LOG.error("Error in adding application");
			return "errors/Error500.jsp";
		}

		LOG.debug("Finishing DB operation");

		if (adding == false) {
			session.setAttribute("Error", "������ �� ����������");
			return "dispatcherApplication.jsp";
		}

		session.setAttribute("noError", "������ ����������");
		session.removeAttribute("Error");

		String dispatcherMail = user.getEmail();
		LOG.trace(dispatcherMail);
		String adminEmail = "airlines.mailpost@gmail.com";
		LOG.trace(password);

		if (password != null && password != "") {
			Thread t = new Thread(() -> {
				try {
					Message.sendTextMessageToAdmin(dispatcherMail, password, adminEmail,
							CreateMailContent.createApplicationContent(user.getLogin(), date, comment));
				} catch (NamingException e) {
					// noting to do
				}
			});
			t.start();
		}

		return "dispatcherApplication.jsp";
	}
}
