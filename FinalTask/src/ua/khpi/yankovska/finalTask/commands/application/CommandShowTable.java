package ua.khpi.yankovska.finalTask.commands.application;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.ApplicationDAO;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.Application;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which get all applications which are not yet processed in the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandShowTable implements Command {

	private static final String ERROR = "Error";
	private static final Logger LOG = Logger.getLogger(CommandShowTable.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to get all applications which are
	 * not yet processed in the database.
	 *
	 * @return an adminApplication.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		LOG.debug("Starting DB operation");

		DBManager dbManager = DBManager.getInstance();

		List<Application> list = null;

		try {
			Connection con = dbManager.getConnection();
			ApplicationDAO appDAO = new ApplicationDAO(con);
			list = appDAO.findAll();
			Util.close(con);
		} catch (Exception e) {
			session.setAttribute(ERROR, "��������� ������ � ������ ���� ������");
			LOG.error("Error in db");
			return "errors/Error500.jsp";
		}
		LOG.trace(list);
		LOG.debug("Finishing DB operation");

		if (list.isEmpty()) {
			session.setAttribute(ERROR, "�������� ������ ���!");
			LOG.trace("List of applications is empty");
			return "adminApplication.jsp";
		} else {
			session.removeAttribute(ERROR);

			session.setAttribute("app", list);
			LOG.trace(session.getAttribute("app"));

			LOG.trace("List of applications is full");
			return "adminApplication.jsp";
		}
	}

}
