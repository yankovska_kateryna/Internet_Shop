package ua.khpi.yankovska.finalTask.commands.authorization;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.UserDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.User;
import ua.khpi.yankovska.finalTask.mail.CreateMailContent;
import ua.khpi.yankovska.finalTask.mail.Message;
import ua.khpi.yankovska.finalTask.util.Password;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which register the dispatcher.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandRegistration implements Command {

	private static final String REG_EXP = "[\\s]{1,}";
	private static final Logger LOG = Logger.getLogger(CommandRegistration.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to add new dispatcher to the
	 * database.
	 *
	 * @return an admin.jsp is fulfilled, an adminRegistrtion.jsp if input data is
	 *         incorrect or an Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		String name = request.getParameter("name").trim();
		String surname = request.getParameter("surname").trim();
		String telNum = request.getParameter("tel").trim();
		String email = request.getParameter("email").trim();
		String login = request.getParameter("login").trim();
		String password = request.getParameter("psw").trim();

		String savePassword = password;
		String saveEmail = email;
		String saveLogin = login;

		try {
			password = Password.hash(password);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
			LOG.error("Error with haashing password");
			return "errors/Error500.jsp";
		}

		name = name.replaceAll(REG_EXP, "");
		surname = surname.replaceAll(REG_EXP, "");
		telNum = telNum.replaceAll(REG_EXP, "");
		email = email.replaceAll(REG_EXP, "");
		login = login.replaceAll(REG_EXP, "");
		password = password.replaceAll(REG_EXP, "");

		name = name.substring(0, 1).toUpperCase() + name.substring(1);
		surname = surname.substring(0, 1).toUpperCase() + surname.substring(1);

		LOG.trace("Getting name/surname/telNum/email/login: " + name + "/" + surname + "/" + telNum + "/" + email + "/"
				+ login);
		LOG.debug("Starting DB operation");
		boolean adding = false;
		DBManager dbManager = DBManager.getInstance();

		try {
			Connection con = dbManager.getConnection();
			UserDAO userDAO = new UserDAO(con);
			adding = userDAO.create(User.createUser(name, surname, telNum, email, login, password));
			Util.close(con);
		} catch (Exception e) {
			LOG.error("Error in registration");
			return "errors/Error500.jsp";
		}
		LOG.debug("Finishing DB operation");

		if (adding == false) {
			session.setAttribute("errorRegistration", "������������ � ������ ������� ��� ����������");
			return "adminRegistration.jsp";
		} else {
			session.removeAttribute("errorRegistration");

			Thread t = new Thread(() -> {
				try {
					Message.sendTextMessage(saveEmail,
							CreateMailContent.createRegistrationContent(saveLogin, savePassword));
				} catch (NamingException e) {
					// noting to do
				}
			});
			t.start();

			return "admin.jsp";
		}
	}
}
