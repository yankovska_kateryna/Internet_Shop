package ua.khpi.yankovska.finalTask.commands.authorization;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.UserDAO;
import ua.khpi.yankovska.finalTask.db.Util;
import ua.khpi.yankovska.finalTask.entity.User;
import ua.khpi.yankovska.finalTask.util.Password;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which check user's login and password.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandLogin implements Command {

	private static final String ERROR_LOGIN = "errorLogin";
	private static final Logger LOG = Logger.getLogger(CommandLogin.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to check user's login and password
	 * with the database.
	 *
	 * @return an jsp result page depending on what role a person enters the system
	 *         if request is fulfilled or an Error500.jsp page if there is an
	 *         exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		LOG.debug("Command starts");

		HttpSession session = request.getSession(true);

		String login = request.getParameter("uname");
		String password = request.getParameter("password");
		try {
			password = Password.hash(password);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
			LOG.error("Error with haashing password");
			return "errors/Error500.jsp";
		}

		LOG.trace("Getting login: " + login + password);
		LOG.debug("Starting DB operation");
		DBManager dbManager = DBManager.getInstance();

		User user;
		try {
			Connection con = dbManager.getConnection();
			UserDAO userDAO = new UserDAO(con);
			user = userDAO.getUser(login);
			Util.close(con);
		} catch (Exception e) {
			session.setAttribute("error", "��������� ������ � ������ ���� ������");
			LOG.error("Error in db");
			return "errors/Error500.jsp";
		}

		LOG.debug("Finishing DB operation");
		LOG.trace("Gets user: " + user);

		if (user == null) {
			session.setAttribute(ERROR_LOGIN, "����� �� ������");
			return "indexLogin.jsp";
		} else if (!user.getPassword().equals(password)) {
			LOG.warn("User: " + user + " is invalid");
			session.setAttribute(ERROR_LOGIN, "������ �� ������");
			return "indexLogin.jsp";
		} else {

			session.removeAttribute(ERROR_LOGIN);

			session.setAttribute("user", user);

			if (user.getLogin().equals("admin")) {
				return "admin.jsp";
			}

			return "dispatcher.jsp";
		}
	}
}
