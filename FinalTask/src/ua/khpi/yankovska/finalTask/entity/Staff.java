package ua.khpi.yankovska.finalTask.entity;

public class Staff extends Entity {

	/**
	 * Class staff with properties <b>name</b>, <b>surname</b>, <b>telNum</b>,
	 * <b>position</b> extends {@link Entity}
	 * 
	 * @author Kate Yankovska
	 * 
	 */

	private static final long serialVersionUID = 1L;

	private String name;
	private String surname;
	private String position;
	private String telNum;

	public Staff() {
		super();
	}

	public String getName() {
		return name;
	}

	/**
	 * Procedure for determining {@link Staff#name}
	 * 
	 * @param name
	 *            - staff name
	 */

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	/**
	 * Procedure for determining {@link Staff#surname}
	 * 
	 * @param surname
	 *            - staff surname
	 */

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPosition() {
		return position;
	}

	/**
	 * Procedure for determining {@link Staff#position}
	 * 
	 * @param position
	 *            - staff position
	 */

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTelNum() {
		return telNum;
	}

	/**
	 * Procedure for determining {@link Staff#telNum}
	 * 
	 * @param telNum
	 *            - staff telephone number
	 */

	public void setTelNum(String telNum) {
		this.telNum = telNum;
	}

	@Override
	public String toString() {
		return "Staff [id=" + super.getId() + ", name=" + name + ", surname=" + surname + ", position=" + position
				+ ", telNum=" + telNum + "]";
	}

	public static Staff createStaff(String name, String surname, String position, String telNum) {
		Staff staff = new Staff();
		staff.setName(name);
		staff.setSurname(surname);
		staff.setPosition(position);
		staff.setTelNum(telNum);

		return staff;
	}
}
