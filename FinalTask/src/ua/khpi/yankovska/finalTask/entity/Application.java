package ua.khpi.yankovska.finalTask.entity;

public class Application extends Entity {

	/**
	 * Class application with properties <b>idDisp</b>, <b>loginDisp</b>,
	 * <b>sendDate</b>, <b>text</b>, <b>status</b> extends {@link Entity}
	 * 
	 * @author Kate Yankovska
	 * 
	 */

	private static final long serialVersionUID = 1L;
	private int idDisp;
	private String loginDisp;
	private String sendDate;
	private String text;
	private String status;

	public Application() {
		super();
	}

	public int getIdDisp() {
		return idDisp;
	}

	/**
	 * Procedure for determining {@link Application#idDisp}
	 * 
	 * @param idDisp
	 *            - dispatcher's id who sent this application
	 */

	public void setIdDisp(int idDisp) {
		this.idDisp = idDisp;
	}

	public String getSendDate() {
		return sendDate;
	}

	/**
	 * Procedure for determining {@link Application#sendDate}
	 * 
	 * @param sendDate
	 *            - date of sending application
	 */

	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}

	public String getText() {
		return text;
	}

	/**
	 * Procedure for determining {@link Application#text}
	 * 
	 * @param text
	 *            - dispatcher's sending text
	 */

	public void setText(String text) {
		this.text = text;
	}

	public String getStatus() {
		return status;
	}

	/**
	 * Procedure for determining {@link Application#status}
	 * 
	 * @param status
	 *            - application status
	 */

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLoginDisp() {
		return loginDisp;
	}

	/**
	 * Procedure for determining {@link Application#loginDisp}
	 * 
	 * @param loginDisp
	 *            - dispatcher's login who sent this application
	 */

	public void setLoginDisp(String loginDisp) {
		this.loginDisp = loginDisp;
	}

	@Override
	public String toString() {
		return "Application [ id=" + super.getId() + ", idDisp=" + idDisp + ", date=" + sendDate + ", loginDisp="
				+ loginDisp + ", text=" + text + ", status=" + status + "]";
	}

	public static Application createApplication(int idDisp, String loginDisp, String sendDate, String text) {
		Application app = new Application();
		app.setIdDisp(idDisp);
		app.setLoginDisp(loginDisp);
		app.setSendDate(sendDate);
		app.setText(text);
		app.setStatus("������� ������������");
		return app;
	}

}
