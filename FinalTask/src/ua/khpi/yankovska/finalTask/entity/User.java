package ua.khpi.yankovska.finalTask.entity;

public class User extends Entity {

	/**
	 * Class user with properties <b>name</b>, <b>surname</b>, <b>telNum</b>,
	 * <b>email</b>, <b>login</b>,<b>password</b> extends {@link Entity}
	 * 
	 * @author Kate Yankovska
	 * 
	 */

	private static final long serialVersionUID = 1L;

	private String name;
	private String surname;
	private String telNum;
	private String email;
	private String login;
	private String password;

	public User() {
		super();
	}

	public String getName() {
		return name;
	}

	/**
	 * Procedure for determining {@link User#name}
	 * 
	 * @param name
	 *            - user name
	 */

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	/**
	 * Procedure for determining {@link User#surname}
	 * 
	 * @param surname
	 *            - user surname
	 */

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getTelNum() {
		return telNum;
	}

	/**
	 * Procedure for determining {@link User#telNum}
	 * 
	 * @param telNum
	 *            - user telephone number
	 */

	public void setTelNum(String telNum) {
		this.telNum = telNum;
	}

	public String getEmail() {
		return email;
	}

	/**
	 * Procedure for determining {@link User#email}
	 * 
	 * @param email
	 *            - user email
	 */

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	/**
	 * Procedure for determining {@link User#login}
	 * 
	 * @param login
	 *            - user login
	 */

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	/**
	 * Procedure for determining {@link User#password}
	 * 
	 * @param password
	 *            - user password
	 */

	public void setPassword(String password) {
		this.password = password;
	}

	public static User createUser(String name, String surname, String telNum, String email, String login,
			String password) {
		User user = new User();
		user.setName(name);
		user.setSurname(surname);
		user.setTelNum(telNum);
		user.setEmail(email);
		user.setLogin(login);
		user.setPassword(password);

		return user;
	}

	@Override
	public String toString() {
		return login;
	}
}
