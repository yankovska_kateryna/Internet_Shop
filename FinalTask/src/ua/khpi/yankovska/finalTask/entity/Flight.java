package ua.khpi.yankovska.finalTask.entity;

public class Flight extends Entity {

	/**
	 * Class flight with properties <b>fromCity</b>, <b>fromCountry</b>,
	 * <b>toCity</b>, <b>toCountry</b>, <b>flightDate</b>, <b>flightArrival</b>,
	 * <b>raceName</b>, <b>status</b> extends {@link Entity}
	 * 
	 * @author Kate Yankovska
	 * 
	 */

	private static final long serialVersionUID = 1L;

	private String fromCity;
	private String fromCountry;
	private String toCity;
	private String toCountry;
	private String flightDate;
	private String flightArrival;
	private String raceName;
	private String status;
	private int count;

	public Flight() {
		super();
	}

	public String getFromCity() {
		return fromCity;
	}

	/**
	 * Procedure for determining {@link Flight#fromCity}
	 * 
	 * @param fromCity
	 *            - flight from city
	 */

	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}

	public String getFromCountry() {
		return fromCountry;
	}

	/**
	 * Procedure for determining {@link Flight#fromCountry}
	 * 
	 * @param fromCountry
	 *            - flight from country
	 */

	public void setFromCountry(String fromCountry) {
		this.fromCountry = fromCountry;
	}

	public String getToCity() {
		return toCity;
	}

	/**
	 * Procedure for determining {@link Flight#toCity}
	 * 
	 * @param toCity
	 *            - flight to city
	 */

	public void setToCity(String toCity) {
		this.toCity = toCity;
	}

	public String getToCountry() {
		return toCountry;
	}

	/**
	 * Procedure for determining {@link Flight#toCountry}
	 * 
	 * @param toCountry
	 *            - flight to country
	 */

	public void setToCountry(String toCountry) {
		this.toCountry = toCountry;
	}

	public String getFlightDate() {
		return flightDate;
	}

	/**
	 * Procedure for determining {@link Flight#flightDate}
	 * 
	 * @param flightDate
	 *            - flight departure date
	 */

	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	public String getRaceName() {
		return raceName;
	}

	/**
	 * Procedure for determining {@link Flight#raceName}
	 * 
	 * @param raceName
	 *            - flight name
	 */

	public void setRaceName(String raceName) {
		this.raceName = raceName;
	}

	public String getStatus() {
		return status;
	}

	/**
	 * Procedure for determining {@link Flight#status}
	 * 
	 * @param status
	 *            - flight status
	 */

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFlightArrival() {
		return flightArrival;
	}

	/**
	 * Procedure for determining {@link Flight#flightArrival}
	 * 
	 * @param flightArrival
	 *            - flight arrival date
	 */

	public void setFlightArrival(String flightArrival) {
		this.flightArrival = flightArrival;
	}
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "Flight [id=" + super.getId() + ", fromCity=" + fromCity + ", fromCountry=" + fromCountry + ", toCity="
				+ toCity + ", toCountry=" + toCountry + ", flightDate=" + flightDate + ", flightArrival="
				+ flightArrival + ", raceName=" + raceName + ", status=" + status + ", count=" + count + "]";
	}

	public static Flight createFlight(String fromCity, String fromCountry, String toCity, String toCountry,
			String flightDate, String flightArrival) {
		Flight flight = new Flight();
		flight.setFromCity(fromCity);
		flight.setFromCountry(fromCountry);
		flight.setToCity(toCity);
		flight.setToCountry(toCountry);
		flight.setFlightDate(flightDate);
		flight.setFlightArrival(flightArrival);
		flight.setRaceName(fromCity + "-" + toCity);
		return flight;
	}
}
