package ua.khpi.yankovska.finalTask.entity;

import java.io.Serializable;

public class Entity implements Serializable, Cloneable {

	/**
	 * Class entity with properties <b>id</b>
	 * 
	 * @author Kate Yankovska
	 * 
	 */

	private static final long serialVersionUID = 1L;
	private int id;

	public Entity() {

	}

	public Entity(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	/**
	 * Procedure for determining {@link Entity#id}
	 * 
	 * @param id
	 *            - entity id
	 */

	public void setId(int id) {
		this.id = id;
	}
}
