package ua.khpi.yankovska.finalTask.customTag;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * Class which extends from {@link SimpleTagSupport}. Creating of the own tag.
 * 
 * @author Kate Yankovska
 *
 */

public class TitleTag extends SimpleTagSupport {
	private String message;

	/**
	 * Setter method
	 */

	public void setMessage(String msg) {
		this.message = msg;
	}

	StringWriter sw = new StringWriter();

	@Override
	public void doTag() throws JspException, IOException {
		if (message != null) {
			/* Use message from attribute */
			JspWriter out = getJspContext().getOut();
			out.println(message);
		} else {
			/* use message from the body */
			getJspBody().invoke(sw);
			getJspContext().getOut().println(sw.toString());
		}
	}
}
