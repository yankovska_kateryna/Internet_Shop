package ua.khpi.yankovska.finalTask.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.entity.User;

/**
 * Class that realizes own logic methods
 * 
 * @author Kate Yankovska
 *
 */

public class LogicDAO {

	private static final Logger LOG = Logger.getLogger(LogicDAO.class.getName());

	protected Connection con;

	/**
	 * Constructor
	 * 
	 * @param connection
	 */

	public LogicDAO(Connection connection) {
		this.con = connection;
	}

	/**
	 * Set flights for dispatchers
	 * 
	 * @param user
	 * @param flightId
	 * @return true - operation done, false - no
	 */

	public boolean setFlightsForUser(User user, int flightId) {
		PreparedStatement ps = null;

		try {
			LOG.debug("Set user - flight");
			con.setAutoCommit(false);

			ps = con.prepareStatement(Requests.SQL_SET_USER_FLIGHT);
			ps.setInt(1, user.getId());
			ps.setInt(2, flightId);

			if (ps.executeUpdate() > 0) {
				con.commit();
				return true;
			}

			Util.doRollBack(con);
			return false;
		} catch (SQLException e) {
			Util.doRollBack(con);
			LOG.trace("RollBack");
			LOG.warn("Error in inserting user_flight. " + e.getMessage());
		} finally {
			Util.close(ps);
		}

		return false;
	}

	/**
	 * Update flights by flight's id and disptcher's id
	 * 
	 * @param flightId
	 * @param user
	 * @return true - operation done, false - no
	 */

	public boolean update(int flightId, User user) {
		PreparedStatement ps = null;
		try {
			LOG.debug("Update flight");

			con.setAutoCommit(false);

			ps = con.prepareStatement(Requests.SQL_UPDATE_USER_FLIGHT);
			ps.setInt(1, user.getId());
			ps.setInt(2, flightId);

			if (ps.executeUpdate() > 0) {
				con.commit();
				return true;
			}
			LOG.trace("Flight is not updated");
			Util.doRollBack(con);
			return false;
		} catch (SQLException e) {
			LOG.warn("Exception in update(flight): " + e.getMessage());
		} finally {
			Util.close(ps);
		}

		return false;
	}

	/**
	 * Get dispatcher's flight id
	 * 
	 * @param user
	 * @return list of integer
	 * @throws Exception
	 */

	public List<Integer> findFlightsForUser(User user) throws Exception {
		List<Integer> flightsNum = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Finding all flights for dispatchers");
			ps = con.prepareStatement(Requests.SQL_GET_USER_FLIGHTS);
			ps.setInt(1, user.getId());

			rs = ps.executeQuery();

			while (rs.next()) {
				flightsNum.add(rs.getInt("flight_id"));
			}

			return flightsNum;
		} catch (SQLException e) {
			LOG.warn("Exception in findFlightsForUser to the user(user_flight): " + e.getMessage());
		} finally {
			Util.close(rs);
			Util.close(ps);
		}
		return null;
	}

	/**
	 * Update flight status
	 * 
	 * @param flightId
	 * @param status
	 * @return true - operation done, false - not
	 */

	public boolean updateFlightStatus(int flightId, String status) {
		PreparedStatement ps = null;
		try {
			LOG.debug("Update status");

			con.setAutoCommit(false);

			ps = con.prepareStatement(Requests.SQL_UPDATE_STATUS_USER_FLIGHT);
			ps.setString(1, status);
			ps.setInt(2, flightId);

			if (ps.executeUpdate() > 0) {
				return true;
			}
			return false;
		} catch (SQLException e) {
			LOG.warn("Exception in update status: " + e.getMessage());
		} finally {
			Util.close(ps);
		}

		return false;
	}

	/**
	 * Set staff to flight
	 * 
	 * @param flightId
	 * @param staffId
	 * @return true - operation done, false - not
	 */

	public boolean setStaffForFlights(int flightId, int staffId) {
		PreparedStatement ps = null;

		try {
			LOG.debug("Set flight - staff");
			con.setAutoCommit(false);

			ps = con.prepareStatement(Requests.SQL_SET_FLIGHT_STAFF);
			ps.setInt(1, flightId);
			ps.setInt(2, staffId);

			if (ps.executeUpdate() > 0) {
				con.commit();
				return true;
			}

			Util.doRollBack(con);
			return false;
		} catch (SQLException e) {
			Util.doRollBack(con);
			LOG.trace("RollBack");
			LOG.warn("Error in inserting flight_staff. " + e.getMessage());
		} finally {
			Util.close(ps);
		}

		return false;
	}

}
