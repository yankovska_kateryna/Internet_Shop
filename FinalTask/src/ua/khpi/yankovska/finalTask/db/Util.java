package ua.khpi.yankovska.finalTask.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.entity.Application;
import ua.khpi.yankovska.finalTask.entity.Flight;
import ua.khpi.yankovska.finalTask.entity.Staff;
import ua.khpi.yankovska.finalTask.entity.User;

/**
 * Util methods
 * 
 * @author Kate Yankovska
 *
 */

public class Util {

	private static final Logger LOG = Logger.getLogger(Util.class.getName());

	/**
	 * Extract user from database
	 * 
	 * @param rs
	 * @return object User
	 * @throws SQLException
	 */

	public static User extractUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setId(rs.getInt("id"));
		user.setName(rs.getString("name"));
		user.setSurname(rs.getString("surname"));
		user.setTelNum(rs.getString("telNum"));
		user.setEmail(rs.getString("email"));
		user.setLogin(rs.getString("login"));
		user.setPassword(rs.getString("password"));
		return user;
	}

	/**
	 * Extract flight from database
	 * 
	 * @param rs
	 * @return object Flight
	 * @throws SQLException
	 */

	public static Flight extractFlight(ResultSet rs) throws SQLException {
		Flight flight = new Flight();
		flight.setId(rs.getInt("id"));
		flight.setFromCity(rs.getString("fromCity"));
		flight.setFromCountry(rs.getString("fromCountry"));
		flight.setToCity(rs.getString("toCity"));
		flight.setToCountry(rs.getString("toCountry"));
		flight.setFlightDate(rs.getString("flightDate"));
		flight.setFlightArrival(rs.getString("flightArrival"));
		flight.setRaceName(rs.getString("raceName"));
		flight.setStatus(rs.getString("status"));
		return flight;
	}

	/**
	 * Extract staff's flights from database
	 * 
	 * @param rs
	 * @return object Flight
	 * @throws SQLException
	 */

	public static Flight extractFlightForStaff(ResultSet rs) throws SQLException {
		Flight flight = new Flight();
		flight.setId(rs.getInt("id"));
		flight.setFromCity(rs.getString("fromCity"));
		flight.setFromCountry(rs.getString("fromCountry"));
		flight.setToCity(rs.getString("toCity"));
		flight.setToCountry(rs.getString("toCountry"));
		flight.setFlightDate(rs.getString("flightDate"));
		flight.setFlightArrival(rs.getString("flightArrival"));
		flight.setRaceName(rs.getString("raceName"));
		return flight;
	}
	
	public static Flight extractFlightCount(ResultSet rs) throws SQLException {
		Flight flight = new Flight();
		flight.setId(rs.getInt("id"));
		flight.setFromCity(rs.getString("fromCity"));
		flight.setFromCountry(rs.getString("fromCountry"));
		flight.setToCity(rs.getString("toCity"));
		flight.setToCountry(rs.getString("toCountry"));
		flight.setFlightDate(rs.getString("flightDate"));
		flight.setFlightArrival(rs.getString("flightArrival"));
		flight.setRaceName(rs.getString("raceName"));
		flight.setCount(rs.getInt("count(raceName)"));
		return flight;
	}

	/**
	 * Extract staff from database
	 * 
	 * @param rs
	 * @return object Staff
	 * @throws SQLException
	 */

	public static Staff extractStaff(ResultSet rs) throws SQLException {
		Staff staff = new Staff();
		staff.setId(rs.getInt("id"));
		staff.setName(rs.getString("name"));
		staff.setSurname(rs.getString("surname"));
		staff.setPosition(rs.getString("position"));
		staff.setTelNum(rs.getString("telNum"));
		return staff;
	}

	/**
	 * Extract application from database
	 * 
	 * @param rs
	 * @return object Application
	 * @throws SQLException
	 */

	public static Application extractApplication(ResultSet rs) throws SQLException {
		Application application = new Application();
		application.setId(rs.getInt("id"));
		application.setIdDisp(rs.getInt("id_disp"));
		application.setLoginDisp(rs.getString("login"));
		application.setSendDate(rs.getString("sendDate"));
		application.setText(rs.getString("text"));
		application.setStatus(rs.getString("status"));
		return application;
	}

	/**
	 * Close objects which have open connection
	 * 
	 * @param ac
	 */

	public static void close(AutoCloseable ac) {
		if (ac != null) {
			try {
				ac.close();
			} catch (Exception e) {
				LOG.error("Error in closing " + ac);
			}
		}
	}

	/**
	 * Transaction rollback
	 * 
	 * @param con
	 */

	public static void doRollBack(Connection con) {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException e) {
				LOG.error("Rollback error" + e);
			}
		}
	}
}
