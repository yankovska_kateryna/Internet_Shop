package ua.khpi.yankovska.finalTask.db;

/**
 * Class with SQL requests
 * 
 * @author Kate Yankovska
 *
 */

public class Requests {

	// USER
	public static final String SQL_CREATE_USER = "INSERT INTO users VALUES(DEFAULT, ?, ?, ?, ?, ?, ?)";
	public static final String SQL_GET_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
	public static final String SQL_GET_USERS = "SELECT * FROM users WHERE id > 1000";

	// FLIGHT
	public static final String SQL_GET_FLIGHTS_BY_PARAMETERS = "SELECT flights.*, user_flight.status "
			+ "FROM flights INNER JOIN user_flight WHERE user_flight.flight_id = flights.id AND"
			+ " flights.fromCity = ? AND flights.toCity = ? AND DATE(flights.flightDate) = ?";
	public static final String SQL_GET_FLIGHTS_BY_RACENUMBER = "SELECT flights.*, user_flight.status "
			+ "FROM flights INNER JOIN user_flight WHERE user_flight.flight_id = flights.id AND flights.id = ?";
	public static final String SQL_GET_FLIGHTS_BY_DATE = "SELECT flights.*, user_flight.status "
			+ "FROM flights INNER JOIN user_flight WHERE user_flight.flight_id = flights.id AND"
			+ " DATE(flights.flightDate) = ?";
	public static final String SQL_CREATE_FLIGHT = "INSERT INTO flights VALUES(DEFAULT, ?, ?, ?, ?, ?, ?, ?)";
	public static final String SQL_GET_FLIGHTS = "SELECT * FROM flights";
	public static final String SQL_UPDATE_FLIGHT = "UPDATE flights SET flightDate = ?, flightArrival = ? WHERE id = ?";
	public static final String SQL_DELETE_FLIGHT = "DELETE FROM flights WHERE id = ?";
	public static final String SQL_GET_MAX_FLIGHT_NUM = "SELECT max(id) FROM flights";

	// STAFF
	public static final String SQL_CREATE_STAFF = "INSERT INTO staff VALUES(DEFAULT, ?, ?, ?, ?)";
	public static final String SQL_GET_STAFF = "SELECT * FROM staff";
	public static final String SQL_UPDATE_STAFF_SURNAME = "UPDATE staff SET surname = ? WHERE id = ?";
	public static final String SQL_UPDATE_STAFF_TELNUM = "UPDATE staff SET telNum = ? WHERE id = ?";
	public static final String SQL_DELETE_STAFF = "DELETE FROM staff WHERE id = ?";
	public static final String SQL_GET_STAFF_BY_PARAMETERS = "SELECT staff.id, staff.name, "
			+ "staff.surname, staff.position, staff.telNum "
			+ "FROM staff WHERE staff.position = ? AND staff.surname = ? AND staff.name = ?";
	public static final String SQL_GET_STAFF_BY_JOB = "SELECT staff.id, staff.name, staff.surname, "
			+ "staff.position, staff.telNum FROM staff " + "WHERE staff.position = ?";

	// APPLICATION
	public static final String SQL_GET_APPLICATION = "SELECT application.id, users.login, application.sendDate, application.id_disp, "
			+ "application.text, application.status FROM application INNER JOIN users "
			+ "ON users.id = application.id_disp WHERE status = '������� ������������'";
	public static final String SQL_UPDATE_APPLICATION_STATUS = "UPDATE application SET status = ? WHERE id = ?";
	public static final String SQL_CREATE_APPLICATION = "INSERT INTO application VALUES(DEFAULT, ?, ?, ?, ?)";

	// USER_FLIGHT
	public static final String SQL_SET_USER_FLIGHT = "INSERT INTO user_flight VALUES(?, ?, '���� ���������������')";
	public static final String SQL_UPDATE_USER_FLIGHT = "UPDATE user_flight SET user_id = ? WHERE flight_id = ?";
	public static final String SQL_GET_USER_FLIGHTS = "SELECT user_flight.flight_id FROM user_flight INNER JOIN flights ON user_flight.flight_id = flights.id WHERE "
			+ "flights.flightArrival >= NOW() AND user_id = ?";
	public static final String SQL_UPDATE_STATUS_USER_FLIGHT = "UPDATE user_flight SET status = ? WHERE flight_id = ?";

	// FLIGHT_STAFF
	public static final String SQL_SET_FLIGHT_STAFF = "INSERT INTO flight_staff VALUES(?, ?)";
	public static final String SQL_GET_DATE_STAFF = "SELECT * FROM flights INNER "
			+ "JOIN flight_staff ON flights.id = flight_staff.flight_id WHERE staff_Id = ? ORDER BY flightDate";
	public static final String SQL_GET_INPUT_FLIGHT_DATE = "SELECT * FROM flights WHERE id = ?";
	public static final String SQL_GET_STAFF_FLIGHT_BY_ID = "SELECT * FROM flights INNER JOIN "
			+ "flight_staff ON flights.id = flight_staff.flight_id WHERE staff_id = ?";
	public static final String SQL_GET_SPECIAL_FLIGHT_STAFF = "select * from staff inner join flight_staff "
			+ "on staff_id = id where flight_id = ?";

	public static final String SQL_GET_OLD_FLIGHTS = "SELECT * FROM flights WHERE flights.flightArrival < NOW()";
	
	// COUNT FLIGHTS
	public static final String SQL_COUNT_THE_SAME_FLIGHTS = "SELECT COUNT(raceName), flights.* FROM flights where fromCity = ? and toCity = ? GROUP BY(raceName)";

}
