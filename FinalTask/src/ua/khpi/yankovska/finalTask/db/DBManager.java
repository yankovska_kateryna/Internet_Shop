package ua.khpi.yankovska.finalTask.db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;

/**
 * Singleton database manager
 * 
 * @author Kate Yankovska
 *
 */

public class DBManager {

	private static DBManager instance;

	/**
	 * Constructor
	 */

	private DBManager() {

	}

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	/**
	 * Get connection
	 * 
	 * @return object Connection
	 * @throws SQLException
	 * @throws NamingException
	 */

	public Connection getConnection() throws SQLException, NamingException {
		Context envCtx = (Context) (new InitialContext().lookup("java:comp/env"));
		DataSource ds = (DataSource) envCtx.lookup("jdbc/finalTask");
		Connection con = ds.getConnection();
		return con;
	}
}
