package ua.khpi.yankovska.finalTask.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.entity.Staff;

/**
 * Class that extends {@link AbstractDAO<T>} and realizes override methods and
 * own methods
 * 
 * @author Kate Yankovska
 *
 */

public class StaffDAO extends AbstractDAO<Staff> {

	private static final Logger LOG = Logger.getLogger(StaffDAO.class.getName());

	/**
	 * Constructor
	 * 
	 * @param connection
	 */

	public StaffDAO(Connection connection) {
		super(connection);
	}

	@Override
	public List<Staff> findAll() {
		List<Staff> staff = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Finding all staff");
			ps = con.prepareStatement(Requests.SQL_GET_STAFF);

			rs = ps.executeQuery();

			while (rs.next()) {
				staff.add(Util.extractStaff(rs));
			}

			return staff;
		} catch (SQLException e) {
			LOG.warn("Exception in findAll(staff): " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}
		return null;
	}

	@Override
	public boolean delete(int id) {
		PreparedStatement ps = null;

		try {
			LOG.debug("Deleting all staff");
			ps = con.prepareStatement(Requests.SQL_DELETE_STAFF);
			ps.setInt(1, id);

			if (ps.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			LOG.warn("Exception in delete(staff): " + e.getMessage());
		} finally {
			close(ps);
		}

		return false;
	}

	@Override
	public boolean create(Staff staff) {
		PreparedStatement ps = null;

		try {
			LOG.debug("Inserting staff");
			ps = con.prepareStatement(Requests.SQL_CREATE_STAFF);
			ps.setString(1, staff.getName());
			ps.setString(2, staff.getSurname());
			ps.setString(3, staff.getPosition());
			ps.setString(4, staff.getTelNum());

			if (ps.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			LOG.warn("Exception in create(staff): " + e.getMessage());
		} finally {
			close(ps);
		}

		return false;
	}

	@Override
	public boolean update(String surname, int id) {
		PreparedStatement ps = null;
		try {
			LOG.debug("Updating surname staff");
			ps = con.prepareStatement(Requests.SQL_UPDATE_STAFF_SURNAME);
			ps.setString(1, surname);
			ps.setInt(2, id);

			if (ps.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			LOG.warn("Exception in update surname (staff): " + e.getMessage());
		} finally {
			close(ps);
		}

		return false;
	}

	/**
	 * Update staff telephone number
	 * 
	 * @param telNum
	 * @param id
	 * @return true - operation done, false - not
	 */

	public boolean updateStaffTelNum(String telNum, int id) {
		PreparedStatement ps = null;
		try {
			LOG.debug("Updating staff telephone number");
			ps = con.prepareStatement(Requests.SQL_UPDATE_STAFF_TELNUM);
			ps.setString(1, telNum);
			ps.setInt(2, id);

			if (ps.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			LOG.warn("Exception in updateStaffTelNum(staff): " + e.getMessage());
		} finally {
			close(ps);
		}

		return false;
	}

	/**
	 * Get staff by parameters from the database
	 * 
	 * @param position
	 * @param surname
	 * @param name
	 * @return list of objects of class Staff
	 */

	public List<Staff> getStaffByParameters(String position, String surname, String name) {
		List<Staff> staff = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Getting staff by parameters");
			ps = con.prepareStatement(Requests.SQL_GET_STAFF_BY_PARAMETERS);
			ps.setString(1, position);
			ps.setString(2, surname);
			ps.setString(3, name);

			rs = ps.executeQuery();

			while (rs.next()) {
				staff.add(Util.extractStaff(rs));
			}

			return staff;
		} catch (SQLException e) {
			LOG.warn("Exception in getStaffByParameters: " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}
		return null;
	}

	/**
	 * Get staff by parameters from the database
	 * 
	 * @param position
	 * @return list of objects of class Staff
	 */

	public List<Staff> getStaffByJob(String position) {
		List<Staff> staff = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Getting staff by job");
			ps = con.prepareStatement(Requests.SQL_GET_STAFF_BY_JOB);
			ps.setString(1, position);

			rs = ps.executeQuery();

			while (rs.next()) {
				staff.add(Util.extractStaff(rs));
			}

			return staff;
		} catch (SQLException e) {
			LOG.warn("Exception in getStaffByJob: " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}
		return null;
	}

	/**
	 * Get staff for flight by flight id
	 * 
	 * @param id
	 * @return list of objects of class Staff
	 */

	public List<Staff> getSpecialFlightStaff(int id) {
		List<Staff> staff = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Getting staff by id");
			ps = con.prepareStatement(Requests.SQL_GET_SPECIAL_FLIGHT_STAFF);
			ps.setInt(1, id);

			rs = ps.executeQuery();

			while (rs.next()) {
				staff.add(Util.extractStaff(rs));
			}

			return staff;
		} catch (SQLException e) {
			LOG.warn("Exception in getSpecialFlightStaff: " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}
		return null;
	}

}
