package ua.khpi.yankovska.finalTask.db;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

import ua.khpi.yankovska.finalTask.entity.Entity;

/**
 * Abstract class DAO with field Connection without direct initialization.
 * 
 * @author Kate Yankovska
 */

public abstract class AbstractDAO<T extends Entity> {
	protected Connection con;

	/**
	 * Constructor
	 * 
	 * @param connection
	 */

	public AbstractDAO(Connection connection) {
		this.con = connection;
	}

	/**
	 * Find all objects from the database
	 * 
	 * @return list of Objects
	 */

	public abstract List<T> findAll();

	/**
	 * Delete object from the database by id
	 * 
	 * @param id
	 * @return true - operation done, false - not
	 */

	public abstract boolean delete(int id);

	/**
	 * Create new object
	 * 
	 * @param entity
	 * @return true - operation done, false - not
	 */

	public abstract boolean create(T entity);

	/**
	 * Update object
	 * 
	 * @param param
	 * @param id
	 * @return true - operation done, false - not
	 */

	public abstract boolean update(String param, int id);

	/**
	 * Statement closing
	 * 
	 * @param st
	 */

	public void close(Statement st) {
		Util.close(st);
	}
}
