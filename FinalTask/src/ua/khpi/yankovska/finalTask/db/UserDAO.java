package ua.khpi.yankovska.finalTask.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.entity.User;

/**
 * Class that extends {@link AbstractDAO<T>} and realizes override methods and
 * own methods
 * 
 * @author Kate Yankovska
 *
 */

public class UserDAO extends AbstractDAO<User> {

	private static final Logger LOG = Logger.getLogger(UserDAO.class.getName());

	/**
	 * Constructor
	 * 
	 * @param connection
	 */

	public UserDAO(Connection connection) {
		super(connection);
	}

	@Override
	public List<User> findAll() {
		List<User> user = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Finding all users");
			ps = con.prepareStatement(Requests.SQL_GET_USERS);

			rs = ps.executeQuery();

			while (rs.next()) {
				user.add(Util.extractUser(rs));
			}

			return user;
		} catch (SQLException e) {
			LOG.warn("Exception in findAll(staff): " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}
		return null;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(String param, int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean create(User user) {
		PreparedStatement ps = null;

		try {
			LOG.debug("Starting inserting user");
			ps = con.prepareStatement(Requests.SQL_CREATE_USER);
			ps.setString(1, user.getName());
			ps.setString(2, user.getSurname());
			ps.setString(3, user.getTelNum());
			ps.setString(4, user.getEmail());
			ps.setString(5, user.getLogin());
			ps.setString(6, user.getPassword());

			if (ps.executeUpdate() > 0) {
				LOG.debug("User: " + user + " was inserted");
				return true;
			}
		} catch (SQLException e) {
			LOG.warn("Exception in insertingUser(user): " + e.getMessage());
		} finally {
			Util.close(ps);
		}

		return false;
	}

	/**
	 * Get user by login
	 * 
	 * @param login
	 * @return object User
	 */

	public User getUser(String login) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Starting getting user");
			ps = con.prepareStatement(Requests.SQL_GET_USER_BY_LOGIN);
			ps.setString(1, login);

			rs = ps.executeQuery();

			if (rs.next()) {
				LOG.debug("Finishing getting user");
				return Util.extractUser(rs);
			}
		} catch (SQLException e) {
			LOG.warn("Exception in getUser(login): " + e.getMessage());
		} finally {
			Util.close(rs);
			Util.close(ps);
		}

		return null;
	}

}
