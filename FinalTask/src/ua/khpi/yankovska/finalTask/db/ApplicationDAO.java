package ua.khpi.yankovska.finalTask.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.entity.Application;

/**
 * Class that extends {@link AbstractDAO<T>} and realizes override methods
 * 
 * @author Kate Yankovska
 *
 */

public class ApplicationDAO extends AbstractDAO<Application> {

	private static final Logger LOG = Logger.getLogger(ApplicationDAO.class.getName());

	/**
	 * Constructor
	 * 
	 * @param connection
	 */

	public ApplicationDAO(Connection connection) {
		super(connection);
	}

	@Override
	public List<Application> findAll() {
		List<Application> applications = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Getting all applications");
			ps = con.prepareStatement(Requests.SQL_GET_APPLICATION);

			rs = ps.executeQuery();

			while (rs.next()) {
				applications.add(Util.extractApplication(rs));
			}

			return applications;
		} catch (SQLException e) {
			LOG.warn("Exception in findAll (applications): " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}
		return null;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean create(Application application) {
		PreparedStatement ps = null;

		try {
			LOG.debug("Inserting application");
			ps = con.prepareStatement(Requests.SQL_CREATE_APPLICATION);
			ps.setString(1, application.getSendDate());
			ps.setInt(2, application.getIdDisp());
			ps.setString(3, application.getText());
			ps.setString(4, application.getStatus());

			if (ps.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			LOG.warn("Exception in create(application): " + e.getMessage());
		} finally {
			close(ps);
		}

		return false;
	}

	@Override
	public boolean update(String status, int id) {
		PreparedStatement ps = null;
		try {
			LOG.debug("Updating application status");
			ps = con.prepareStatement(Requests.SQL_UPDATE_APPLICATION_STATUS);
			ps.setString(1, status);
			ps.setInt(2, id);
			if (ps.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			LOG.warn("Exception in update status (application): " + e.getMessage());
		} finally {
			close(ps);
		}

		return false;
	}
}
