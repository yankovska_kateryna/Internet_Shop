package ua.khpi.yankovska.finalTask.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.entity.Flight;

/**
 * Class that extends {@link AbstractDAO<T>} and realizes override methods and
 * own methods
 * 
 * @author Kate Yankovska
 *
 */

public class FlightDAO extends AbstractDAO<Flight> {

	private static final Logger LOG = Logger.getLogger(FlightDAO.class.getName());

	/**
	 * Constructor
	 * 
	 * @param connection
	 */

	public FlightDAO(Connection connection) {
		super(connection);
	}

	@Override
	public List<Flight> findAll() {
		List<Flight> flights = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Getting all flights");
			ps = con.prepareStatement(Requests.SQL_GET_FLIGHTS);

			rs = ps.executeQuery();

			while (rs.next()) {
				flights.add(Util.extractFlight(rs));
			}

			return flights;
		} catch (SQLException e) {
			LOG.warn("Exception in findAll (flight): " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}
		return null;
	}

	@Override
	public boolean delete(int id) {
		PreparedStatement ps = null;

		try {
			LOG.debug("Deleting all flights");
			ps = con.prepareStatement(Requests.SQL_DELETE_FLIGHT);
			ps.setInt(1, id);

			if (ps.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			LOG.warn("Exception in delete (flight): " + e.getMessage());
		} finally {
			close(ps);
		}

		return false;
	}

	@Override
	public boolean create(Flight flight) {
		PreparedStatement ps = null;

		try {
			LOG.debug("Inserting flight");
			ps = con.prepareStatement(Requests.SQL_CREATE_FLIGHT);
			ps.setString(1, flight.getFromCity());
			ps.setString(2, flight.getFromCountry());
			ps.setString(3, flight.getToCity());
			ps.setString(4, flight.getToCountry());
			ps.setString(5, flight.getFlightDate());
			ps.setString(6, flight.getFlightArrival());
			ps.setString(7, flight.getRaceName());

			if (ps.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			LOG.warn("Exception in create(flight): " + e.getMessage());
		} finally {
			close(ps);
		}

		return false;
	}

	@Override
	public boolean update(String flightDate, int id) {
		return false;
	}

	public boolean updateDates(String flightDate, String flightArrival, int id) {
		PreparedStatement ps = null;
		try {
			LOG.debug("Update flight departure");
			ps = con.prepareStatement(Requests.SQL_UPDATE_FLIGHT);
			ps.setString(1, flightDate);
			ps.setString(2, flightArrival);
			ps.setInt(3, id);

			if (ps.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			LOG.warn("Exception in update departure(flight): " + e.getMessage());
		} finally {
			close(ps);
		}

		return false;
	}

	/**
	 * Get flights by parameters from database
	 * 
	 * @param cityFrom
	 * @param cityTo
	 * @param flightDate
	 * @return list of objects of class Flight
	 */

	public List<Flight> findAllFlights(String cityFrom, String cityTo, String flightDate) {
		List<Flight> flights = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Finding all flights by cityFrom, cityTo, flightDate");
			ps = con.prepareStatement(Requests.SQL_GET_FLIGHTS_BY_PARAMETERS);
			ps.setString(1, cityFrom);
			ps.setString(2, cityTo);
			ps.setString(3, flightDate);

			rs = ps.executeQuery();

			while (rs.next()) {
				flights.add(Util.extractFlight(rs));
			}

			return flights;
		} catch (SQLException e) {
			LOG.warn("Exception in findAllFlights(flight): " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}
		return null;
	}

	/**
	 * Get flights by parameters from database
	 * 
	 * @param id
	 * @return list of objects of class Flight
	 */

	public List<Flight> getFlightByFlightNumber(int id) {
		List<Flight> flights = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Getting flight by flight number and date");
			ps = con.prepareStatement(Requests.SQL_GET_FLIGHTS_BY_RACENUMBER);
			ps.setInt(1, id);

			rs = ps.executeQuery();

			while (rs.next()) {
				flights.add(Util.extractFlight(rs));
			}

			return flights;
		} catch (SQLException e) {
			LOG.warn("Exception in getFlightByFlightNumber: " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}

		return null;
	}

	/**
	 * Get flights by parameters from database
	 * 
	 * @param flightDate
	 * @return list of objects of class Flight
	 */

	public List<Flight> getFlightByDate(String flightDate) {
		List<Flight> flights = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Getting flight by date");
			ps = con.prepareStatement(Requests.SQL_GET_FLIGHTS_BY_DATE);
			ps.setString(1, flightDate);

			rs = ps.executeQuery();

			while (rs.next()) {
				flights.add(Util.extractFlight(rs));
			}

			return flights;
		} catch (SQLException e) {
			LOG.warn("Exception in getFlightByDate: " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}

		return null;
	}

	/**
	 * Get max flight
	 * 
	 * @return integer value
	 */

	public int getMaxFlight() {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Get max flight number");
			ps = con.prepareStatement(Requests.SQL_GET_MAX_FLIGHT_NUM);

			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getInt("max(id)");
			}
		} catch (SQLException e) {
			LOG.warn("Error in getting max flight. " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}

		return 0;
	}

	/**
	 * Get dates of flights by staff id
	 * 
	 * @param staffId
	 * @return list of objects of class Flight
	 */

	public List<Flight> findDatesOfFlightsByStaffId(int staffId) {
		List<Flight> arrivalDates = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Finding all flight dates of worker");
			ps = con.prepareStatement(Requests.SQL_GET_DATE_STAFF);
			ps.setInt(1, staffId);

			rs = ps.executeQuery();

			while (rs.next()) {
				arrivalDates.add(Util.extractFlightForStaff(rs));
			}

			return arrivalDates;
		} catch (SQLException e) {
			LOG.warn("Exception in find flight dates to the staff: " + e.getMessage());
		} finally {
			Util.close(rs);
			Util.close(ps);
		}
		return null;
	}

	/**
	 * Get update date of the flight
	 * 
	 * @param raceId
	 * @return object Flight
	 */

	public Flight findInputDateOfFlight(int raceId) {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Finding a flight date of adding flight");
			ps = con.prepareStatement(Requests.SQL_GET_INPUT_FLIGHT_DATE);
			ps.setInt(1, raceId);

			rs = ps.executeQuery();

			if (rs.next()) {
				return Util.extractFlightForStaff(rs);
			}
		} catch (SQLException e) {
			LOG.warn("Exception in find flight date to the staff: " + e.getMessage());
		} finally {
			Util.close(rs);
			Util.close(ps);
		}
		return null;
	}

	/**
	 * Get flight staff by flight id
	 * 
	 * @param id
	 * @return list of objects of class Flight
	 */

	public List<Flight> getFlightStaff(int id) {
		List<Flight> flights = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Getting flight staff by flight id");
			ps = con.prepareStatement(Requests.SQL_GET_STAFF_FLIGHT_BY_ID);
			ps.setInt(1, id);

			rs = ps.executeQuery();

			while (rs.next()) {
				flights.add(Util.extractFlight(rs));
			}

			return flights;
		} catch (SQLException e) {
			LOG.warn("Exception in getFlightStaff: " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}

		return null;
	}

	/**
	 * Get all old flights
	 * 
	 * @return list of objects of class Flight
	 */

	public List<Flight> findOldFlights() {
		List<Flight> flights = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Getting all old flights");
			ps = con.prepareStatement(Requests.SQL_GET_OLD_FLIGHTS);

			rs = ps.executeQuery();

			while (rs.next()) {
				flights.add(Util.extractFlightForStaff(rs));
			}

			return flights;
		} catch (SQLException e) {
			LOG.warn("Exception in findAllOldFlights (flight): " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}
		return null;
	}
	
	public List<Flight> findCountFlights(String cityFrom, String cityTo) {
		List<Flight> flights = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Finding and counting all flights by cityFrom, cityTo");
			ps = con.prepareStatement(Requests.SQL_COUNT_THE_SAME_FLIGHTS);
			ps.setString(1, cityFrom);
			ps.setString(2, cityTo);

			rs = ps.executeQuery();

			while (rs.next()) {
				flights.add(Util.extractFlightCount(rs));
			}

			return flights;
		} catch (SQLException e) {
			LOG.warn("Exception in findAllFlights(flight): " + e.getMessage());
		} finally {
			Util.close(rs);
			close(ps);
		}
		return null;
	}
}
