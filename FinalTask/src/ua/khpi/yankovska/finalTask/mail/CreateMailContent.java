package ua.khpi.yankovska.finalTask.mail;

public class CreateMailContent {

	public static String createRegistrationContent(String login, String password) {

		return "<html>\r\n" + "<head>\r\n" + "	<title>Registration</title>\r\n" + "</head>\r\n" + "<body>\r\n"
				+ "	<p style=\"font-size: 30px\">������ ����!</p>\r\n"
				+ "	<p style=\"font-size: 25px\">�� ������� ���������������� � ������� � ������ ���������� � �a����!</p>\r\n"
				+ "	<p style=\"font-weight: bold; font-size: 25px\">Login: " + login + "</p>\r\n"
				+ "	<p style=\"font-weight: bold; font-size: 25px\">Password: " + password + "</p>\r\n" + "\r\n"
				+ "</body>\r\n" + "</html>";
	}

	public static String createApplicationContent(String dispatcher, String date, String comment) {

		return "<html>\r\n" + "<head>\r\n" + "	<title>Registration</title>\r\n" + "</head>\r\n" + "<body>\r\n"
				+ "	<p style=\"font-size: 30px\">��������� �������������!</p>\r\n"
				+ "	<p style=\"font-size: 20px\">������ ����! ��� ��������� ������ �� " + dispatcher + ".</p>\r\n"
				+ "	<p style=\"font-size: 20px\"> <b>���������� ������: </b>" + comment + "</p>\r\n"
				+ "   <p style=\"font-size: 20px\"> <b>���� ������: </b>" + date + "</p>\r\n"
				+ "	<p style=\"font-size: 20px\">����������, ����������� ������ ��� ����� ������!</p>\r\n" + "\r\n"
				+ "</body>\r\n" + "</html>";
	}

	public static String createFileContent(String dispatcher, int id) {

		return "<html>\r\n" + "<head>\r\n" + "	<title>Flight formation</title>\r\n" + "</head>\r\n" + "<body>\r\n"
				+ "	<p style=\"font-size: 30px\">��������� �������������!</p>\r\n"
				+ "	<p style=\"font-size: 20px\">������ ����! �� �������� ���� �� " + dispatcher + " ���������� "
				+ "����� � " + id + ".</p>\r\n" + "\r\n" + "</body>\r\n" + "</html>";
	}
}
