package ua.khpi.yankovska.finalTask.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

@WebListener
public class ContextListener implements ServletContextListener {

	private static final Logger LOG = Logger.getLogger(ContextListener.class.getName());

	public void contextInitialized(ServletContextEvent sce) {
		LOG.info("Starting initialization of the context");

		ServletContext context = sce.getServletContext();

		PropertyConfigurator.configure(context.getRealPath("WEB-INF/log4j.properties"));

		LOG.info("Initialization of the context was finished");
	}

	public void contextDestroyed(ServletContextEvent sce) {
		LOG.info("Context was destroyed");
	}

}
