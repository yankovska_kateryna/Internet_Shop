package ua.khpi.yankovska.finalTask.filewriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.entity.Staff;

public class CreatorTXT implements Creator {

	private static final Logger LOG = Logger.getLogger(CreatorTXT.class.getName());

	@Override
	public File create(List<Staff> list, File f) throws IOException {
		LOG.trace("File writing");
		FileWriter bf;
		try {
			bf = new FileWriter(f);
			for (int i = 0; i < list.size(); i++) {
				bf.write(list.get(i).getId() + " " + list.get(i).getName() + " " + list.get(i).getSurname() + " "
						+ list.get(i).getPosition() + System.lineSeparator());
			}
			bf.flush();
			bf.close();
		} catch (IOException e) {
			LOG.error("Problem while writing to file");
			throw new IOException();
		}
		return f;
	}
}

