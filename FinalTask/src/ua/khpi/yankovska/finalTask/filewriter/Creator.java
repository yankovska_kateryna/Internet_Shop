package ua.khpi.yankovska.finalTask.filewriter;

import java.io.File;
import java.io.IOException;
import java.util.List;

import ua.khpi.yankovska.finalTask.entity.Staff;

/**
 * Interface which creates files
 * 
 * @author Kate Yankovska
 *
 */

public interface Creator {

	/**
	 * Create file
	 * 
	 * @param list
	 *            - list of Staff
	 * @param f
	 *            - file name
	 * @return file
	 */

	public File create(List<Staff> list, File f) throws IOException;

}
