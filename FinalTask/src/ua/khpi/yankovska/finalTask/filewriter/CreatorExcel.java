package ua.khpi.yankovska.finalTask.filewriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import ua.khpi.yankovska.finalTask.entity.Staff;

public class CreatorExcel implements Creator {

	private static final Logger LOG = Logger.getLogger(CreatorExcel.class.getName());

	@Override
	public File create(List<Staff> list, File f) {
		Workbook wbook = new HSSFWorkbook();
		Sheet sheet = wbook.createSheet("New staff");

		Row row = sheet.createRow(0);
		Cell number = row.createCell(0);
		number.setCellType(CellStyle.ALIGN_LEFT);
		number.setCellValue("� ����������");
		Cell name = row.createCell(1);
		name.setCellType(CellStyle.ALIGN_LEFT);
		name.setCellValue("���");
		Cell surname = row.createCell(2);
		surname.setCellType(CellStyle.ALIGN_LEFT);
		surname.setCellValue("�������");
		Cell position = row.createCell(3);
		position.setCellType(CellStyle.ALIGN_LEFT);
		position.setCellValue("���������");
		Cell tel = row.createCell(4);
		tel.setCellType(CellStyle.ALIGN_LEFT);
		tel.setCellValue("����� ��������");

		row = sheet.createRow(1);
		number = row.createCell(0);
		number.setCellValue("");
		name = row.createCell(1);
		name.setCellValue("");
		surname = row.createCell(2);
		surname.setCellValue("");
		position = row.createCell(3);
		position.setCellValue("");
		tel = row.createCell(4);
		tel.setCellValue("");

		for (int i = 0; i < list.size(); i++) {
			row = sheet.createRow(i + 2);
			number = row.createCell(0);
			number.setCellType(CellStyle.ALIGN_LEFT);
			number.setCellValue(list.get(i).getId());
			name = row.createCell(1);
			name.setCellType(CellStyle.ALIGN_LEFT);
			name.setCellValue(list.get(i).getName());
			surname = row.createCell(2);
			surname.setCellValue(list.get(i).getSurname());
			surname.setCellType(CellStyle.ALIGN_LEFT);
			position = row.createCell(3);
			position.setCellValue(list.get(i).getPosition());
			position.setCellType(CellStyle.ALIGN_LEFT);
			tel = row.createCell(4);
			tel.setCellValue(list.get(i).getTelNum());
			tel.setCellType(CellStyle.ALIGN_LEFT);
			sheet.autoSizeColumn(0);
			sheet.autoSizeColumn(1);
			sheet.autoSizeColumn(2);
			sheet.autoSizeColumn(3);
			sheet.autoSizeColumn(4);
		}

		try {
			wbook.write(new FileOutputStream(f));
		} catch (IOException e) {
			LOG.error("Error while writing XLS file", e);
		}

		return f;
	}

}
