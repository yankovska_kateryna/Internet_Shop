package ua.khpi.yankovska.finalTask;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.commands.CommandList;

@WebServlet("/ControllerServlet")
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(ControllerServlet.class.getName());
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response, "GET");
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response, "POST");
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response, String type)
			throws IOException, ServletException {
		LOG.debug("Starting processing request");
		String commandName = request.getParameter("command");
		Command command = CommandList.getCommand(commandName);
		LOG.trace("Getting command with name " + commandName + ": " + command.getClass().getSimpleName());
		String way = command.execute(request, response);

		LOG.trace("Way to go next: " + way);

		if (way == null) {
			response.sendRedirect(way);
		} else {
			if (type.equals("GET")) {
				request.getRequestDispatcher(way).forward(request, response);
				LOG.trace("Forward: " + way);
			} else if (type.equals("POST")) {
				response.sendRedirect(way);
				LOG.trace("Send redirect: " + way);
			}
		}
		LOG.debug("Finishing processing request" + System.lineSeparator());
	}

}
