CREATE DATABASE finalTask COLLATE utf8_general_ci;
USE finalTask;

CREATE TABLE users (
	id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    surname VARCHAR(30) NOT NULL,
    telNum VARCHAR(12) NOT NULL UNIQUE,
    email VARCHAR(30) NOT NULL UNIQUE,
    login VARCHAR(30) NOT NULL UNIQUE,
    password VARCHAR(200) NOT NULL
);

CREATE TABLE flights (
	id INT PRIMARY KEY AUTO_INCREMENT,
    fromCity VARCHAR(50) NOT NULL,
    fromCountry VARCHAR(50) NOT NULL,
    toCity VARCHAR(50) NOT NULL,
    toCountry VARCHAR(50) NOT NULL,
    flightDate DATETIME NOT NULL,
    flightArrival DATETIME NOT NULL,
    raceName VARCHAR(50) NOT NULL
);

CREATE TABLE staff (
	id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    surname VARCHAR(30) NOT NULL,
    position VARCHAR(30) NOT NULL,
    telNum VARCHAR(12) NOT NULL UNIQUE
);

CREATE TABLE application (
	id INT PRIMARY KEY AUTO_INCREMENT,
    sendDate DATE NOT NULL,
    id_disp INT NOT NULL,
    FOREIGN KEY (id_disp) REFERENCES users(id),
    text VARCHAR(1000) NOT NULL,
    status VARCHAR(30) NOT NULL
);

CREATE TABLE user_flight (
	user_id INT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
    
    flight_id INT NOT NULL UNIQUE,
    FOREIGN KEY (flight_id) REFERENCES flights(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
    
    status VARCHAR(50) NOT NULL,
    PRIMARY KEY(user_id, flight_id)
);

CREATE TABLE flight_staff (
	flight_id INT NOT NULL,
    FOREIGN KEY (flight_id) REFERENCES flights(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
    
    staff_id INT NOT NULL,
    FOREIGN KEY (staff_id) REFERENCES staff(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
    
    PRIMARY KEY(flight_id, staff_id)
);

delete from flights;
drop table flights;

alter table flight_staff auto_increment=1000;

SELECT * FROM users;
SELECT * FROM flights;
SELECT * FROM staff;
SELECT * FROM user_flight;
SElECT * FROM application;
SELECT * FROM flight_staff;

DROP TABLE flight_staff;

insert into users values (default, 'Катерина', 'Янковская', '0963736931', 'airlines.mailpost@gmail.com', 'admin', 'admin');

UPDATE users SET password='C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC' WHERE login = 'admin';

SELECT staff.id, staff.name, staff.surname, staff.position, flight_staff.flight_id
FROM staff inner join flight_staff ON flight_staff.staff_id = staff.id;

SELECT id, Date(flightDate), Date(flightArrival) FROM flights;

select flightDate, flightArrival from flights inner join flight_staff on flights.id = flight_staff.flight_id order by flightDate;

SELECT * from flights inner join 
flight_staff on flights.id = flight_staff.flight_id where staff_id = 1000;

UPDATE flights SET flightDate = '2018-02-09', flightArrival = '2018-02-10' WHERE id = 10000;
update users set login = 'kate' where id=1007;

delete from users where id=1001;

select flight_staff.*, count(staff.position) from flight_staff inner join staff on staff_id = id group by(staff.position);

select id, name, surname, position from staff inner join flight_staff on staff_id = id where flight_id = 10037;

select position, count(position) from staff inner join flight_staff on staff_id = id where flight_id = 10037 group by(position);

select count(position), position from staff group by position order by count(position) desc;

select count(raceName), flights.* from flights where fromCity = 'Париж' and toCity = 'Мадрид' group by(raceName);

