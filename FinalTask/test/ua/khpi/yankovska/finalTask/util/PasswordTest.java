package ua.khpi.yankovska.finalTask.util;
import static org.junit.Assert.assertFalse;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;

import ua.khpi.yankovska.finalTask.util.Password;

public class PasswordTest {
	
	@Test
	public void hashTest() {
		String password = "password";
		String result = null;
		try {
			result = Password.hash(password);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		assertFalse(result.equals(password));
	}

}
