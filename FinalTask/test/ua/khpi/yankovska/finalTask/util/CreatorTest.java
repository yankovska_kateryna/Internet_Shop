package ua.khpi.yankovska.finalTask.util;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import ua.khpi.yankovska.finalTask.entity.Staff;
import ua.khpi.yankovska.finalTask.filewriter.Creator;
import ua.khpi.yankovska.finalTask.filewriter.CreatorTXT;

public class CreatorTest {

	private List<Staff> addToList() {
		List<Staff> list = new ArrayList<Staff>();

		Staff f = new Staff();
		f.setId(1);
		f.setName("Ann");
		f.setSurname("Ivanova");
		f.setPosition("Pilot");

		list.add(f);

		return list;
	}

	@Test
	public void creatorTXTTest() throws IOException {
		List<Staff> list = new ArrayList<Staff>();
		list = addToList();

		StringBuilder sb1 = new StringBuilder();
		sb1.append(list.get(0).getId() + " " + list.get(0).getName() + " " + list.get(0).getSurname() + " "
				+ list.get(0).getPosition());

		File f = new File("C:\\Users\\User\\Desktop\\Java\\FinalTask\\testingFile.txt");

		Creator creator = new CreatorTXT();
		creator.create(list, f);

		StringBuilder sb = new StringBuilder();
		BufferedReader bf = new BufferedReader(new FileReader(f));
		String str;

		while ((str = bf.readLine()) != null) {
			sb.append(str);
		}
		bf.close();

		assertTrue(sb.toString().equals(sb1.toString()));
	}

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Test
	public void creatorFallTest() throws IOException {
		List<Staff> list = new ArrayList<Staff>();
		list = addToList();

		StringBuilder sb1 = new StringBuilder();
		sb1.append(list.get(0).getId() + " " + list.get(0).getName() + " " + list.get(0).getSurname() + " "
				+ list.get(0).getPosition());

		File f = new File("C:\\Windows\\testingFile.txt");

		Creator creator = new CreatorTXT();
		exception.expect(IOException.class);
		creator.create(list, f);
	}

}
