package ua.khpi.yankovska.finalTask.commands.adminStaff;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.khpi.yankovska.finalTask.commands.Command;
import ua.khpi.yankovska.finalTask.db.DBManager;
import ua.khpi.yankovska.finalTask.db.StaffDAO;
import ua.khpi.yankovska.finalTask.db.Util;

/**
 * Class which implements from interface {@link Command}. It has only one method
 * which update personnel's surname in the database.
 * 
 * @author Kate Yankovska
 *
 */

public class CommandUpdateStaffSurname implements Command {

	private static final String ADMIN_WORKERS_JSP = "adminWorkers.jsp";
	private static final String ERROR = "Error";
	private static final Logger LOG = Logger.getLogger(CommandUpdateStaffSurname.class.getName());

	/**
	 * Returns a jsp page. Fulfills the request to update personnel's surname to the
	 * database.
	 *
	 * @return an adminWorkers.jsp result page if request is fulfilled or an
	 *         Error500.jsp page if there is an exception.
	 * 
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);

		session.removeAttribute("noError");

		int id = Integer.parseInt(request.getParameter("id2"));
		String surname = request.getParameter("surname3").trim();

		LOG.trace("Getting id/surname: " + id + "/" + surname);

		if (surname == null) {
			session.setAttribute(ERROR, "��������� ��� ����!");
			return ADMIN_WORKERS_JSP;
		} else if (surname == "") {
			session.setAttribute(ERROR, "������� ������!");
			return ADMIN_WORKERS_JSP;
		}

		surname = surname.replaceAll("[\\s]{1,}", "");
		surname = surname.substring(0, 1).toUpperCase() + surname.substring(1);

		LOG.debug("Starting db operation");

		DBManager dbManager = DBManager.getInstance();
		boolean updatingSurname = false;

		try {
			Connection con = dbManager.getConnection();
			StaffDAO staffDAO = new StaffDAO(con);
			updatingSurname = staffDAO.update(surname, id);
			Util.close(con);
		} catch (Exception e) {
			LOG.error("Error in updating staff surname");
			return "errors/Error500.jsp";
		}

		LOG.debug("Finishing DB operation");

		if (updatingSurname == false) {
			session.setAttribute(ERROR, "���������� ��� ����� ������� ��� � ����");
			return ADMIN_WORKERS_JSP;
		} else {
			session.setAttribute("noError", "��������� �������");
			session.removeAttribute(ERROR);
			return ADMIN_WORKERS_JSP;
		}
	}
}
