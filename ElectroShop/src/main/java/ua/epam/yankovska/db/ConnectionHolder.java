package ua.epam.yankovska.db;

import java.sql.Connection;

/**
 * Class that holds connections in ThreadLocal.
 *
 * @author Kateryna_Yankovska
 */

public class ConnectionHolder {
    private ThreadLocal<Connection> holder;

    public ConnectionHolder(DBManager connectionManagerImpl) {
        holder = ThreadLocal.withInitial(connectionManagerImpl::getConnection);
    }

    public Connection getConnection() {
        return holder.get();
    }

    public void removeConnection() {
        holder.remove();
    }
}
