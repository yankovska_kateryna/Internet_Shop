package ua.epam.yankovska.db.impl;

import org.apache.log4j.Logger;
import ua.epam.yankovska.db.ConnectionHolder;
import ua.epam.yankovska.db.TransactionManager;
import ua.epam.yankovska.db.Util;
import ua.epam.yankovska.exception.DBException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Class that realizes transaction.
 *
 * @author Kateryna_Yankovska
 */

public class TransactionManagerImpl implements TransactionManager {
    private static final Logger LOG = Logger.getLogger(TransactionManagerImpl.class.getName());
    private ConnectionHolder connectionHolder;

    public TransactionManagerImpl(ConnectionHolder connectionHolder) {
        this.connectionHolder = connectionHolder;
    }

    @Override
    public <T> T doInTransaction(Supplier<T> transaction) {
        T result;
        try {
            connectionHolder.getConnection().setAutoCommit(false);
            result = transaction.get();
            connectionHolder.getConnection().commit();
        } catch (SQLException | DBException e) {
            LOG.error(e);
            doRollBack(connectionHolder.getConnection());
            throw new DBException(e);
        } finally {
            Util.close(connectionHolder.getConnection());
            connectionHolder.removeConnection();
        }
        return result;
    }

    private void doRollBack(Connection connection) {
        if (Objects.nonNull(connection)) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                LOG.error("Rollback error" + e.getMessage());
                throw new DBException("DBException: " + e);
            }
        }
    }
}
