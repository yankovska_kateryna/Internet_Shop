package ua.epam.yankovska.db;

import ua.epam.yankovska.exception.DBException;

/**
 * Util class.
 *
 * @author Kateryna_Yankovska
 */

public class Util {

    public static void close(AutoCloseable ac) {
        if (ac != null) {
            try {
                ac.close();
            } catch (Exception e) {
                throw new DBException("DBException", e);
            }
        }
    }
}
