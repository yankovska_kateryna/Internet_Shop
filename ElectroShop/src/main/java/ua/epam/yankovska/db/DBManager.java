package ua.epam.yankovska.db;

import java.sql.Connection;

public interface DBManager {

    Connection getConnection();
}
