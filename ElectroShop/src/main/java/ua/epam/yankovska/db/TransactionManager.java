package ua.epam.yankovska.db;

import java.util.function.Supplier;

public interface TransactionManager {

    <T> T doInTransaction(Supplier<T> transaction);

}
