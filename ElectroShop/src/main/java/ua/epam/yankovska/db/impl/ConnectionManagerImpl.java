package ua.epam.yankovska.db.impl;

import org.apache.log4j.Logger;
import ua.epam.yankovska.db.DBManager;
import ua.epam.yankovska.exception.DBException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Class that connects application to the database.
 *
 * @author Kateryna_Yankovska
 */

public class ConnectionManagerImpl implements DBManager {
    private static final Logger LOG = Logger.getLogger(ConnectionManagerImpl.class.getName());
    private static final String JAVA_COMP_ENV = "java:comp/env/";
    private DataSource dataSource;

    public ConnectionManagerImpl(String resourceInv) {
        try {
            Context envContext = new InitialContext();
            dataSource = (DataSource) envContext.lookup(JAVA_COMP_ENV + resourceInv);
        } catch (NamingException e) {
            LOG.error("Error in naming " + e.getMessage());
            throw new DBException("DBException", e);
        }
    }

    @Override
    public Connection getConnection() {
        Connection connection;
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
            throw new DBException("DBException", e);
        }
        return connection;
    }
}
