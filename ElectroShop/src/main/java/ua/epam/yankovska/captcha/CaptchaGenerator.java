package ua.epam.yankovska.captcha;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Class that generate captcha image and value.
 *
 * @author Kateryna_Yankovska
 */

public class CaptchaGenerator {

    private static final String CAPTURE_POSSIBLE_VALUES = "0123456789";
    private long expirePeriod;

    public CaptchaGenerator(long expirePeriod) {
        this.expirePeriod = expirePeriod;
    }

    public Captcha generateCaptcha() {

        int width = 150;
        int height = 50;
        List arrayList = new ArrayList();
        String captureCode = CAPTURE_POSSIBLE_VALUES;
        for (int i = 1; i < captureCode.length() - 1; i++) {
            arrayList.add(captureCode.charAt(i));
        }
        Collections.shuffle(arrayList);
        Iterator itr = arrayList.iterator();
        String s;
        StringBuilder s2 = new StringBuilder();
        Object obj;
        while (itr.hasNext()) {
            obj = itr.next();
            s = obj.toString();
            s2.append(s);
        }
        String s1 = s2.substring(0, 6);
        char[] s3 = s1.toCharArray();
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = bufferedImage.createGraphics();
        Font font = new Font("Georgia", Font.BOLD, 18);
        g2d.setFont(font);
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHints(rh);
        GradientPaint gp = new GradientPaint(0, 0, Color.red, 0, height / 2, Color.black, true);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, width, height);
        g2d.setColor(new Color(255, 153, 0));
        Random r = new Random();
        String captcha = String.copyValueOf(s3);
        int x = 0;
        int y;
        for (int i = 0; i < s3.length; i++) {
            x += 10 + (Math.abs(r.nextInt()) % 15);
            y = 20 + Math.abs(r.nextInt()) % 20;
            g2d.drawChars(s3, i, 1, x, y);
        }
        g2d.dispose();

        return new Captcha(captcha, bufferedImage, LocalDateTime.now().plusSeconds(expirePeriod));
    }
}
