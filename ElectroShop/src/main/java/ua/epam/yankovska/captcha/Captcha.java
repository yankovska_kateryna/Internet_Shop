package ua.epam.yankovska.captcha;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

/**
 * Class-entity of captcha object that stores all parameters of captcha.
 * @author Kateryna_Yankovska
 */

public class Captcha {

    private String captcha;
    private BufferedImage bufferedImage;
    private LocalDateTime expireDate;

    public Captcha(String captcha, BufferedImage bufferedImage, LocalDateTime expireDate) {
        this.captcha = captcha;
        this.bufferedImage = bufferedImage;
        this.expireDate = expireDate;
    }

    public boolean isExpired(){
        return expireDate.isBefore(LocalDateTime.now());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Captcha captcha1 = (Captcha) o;

        if (!captcha.equals(captcha1.captcha)) return false;
        if (!bufferedImage.equals(captcha1.bufferedImage)) return false;
        return expireDate.equals(captcha1.expireDate);
    }

    @Override
    public int hashCode() {
        int result = captcha.hashCode();
        result = 31 * result + bufferedImage.hashCode();
        result = 31 * result + expireDate.hashCode();
        return result;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    @Override
    public String toString() {
        return captcha;
    }
}
