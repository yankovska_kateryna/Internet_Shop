package ua.epam.yankovska.captcha;

import org.apache.log4j.Logger;
import ua.epam.yankovska.service.CaptchaService;
import ua.epam.yankovska.storage.CaptchaStorage;

import javax.sound.midi.Soundbank;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Class that executes thread and removes old captchas.
 * @author Kateryna_Yankovska
 */

public class CaptchaStorageCleaner {

    private static final Logger LOG = Logger.getLogger(CaptchaStorageCleaner.class.getName());

    public void executeThread(CaptchaStorage captchaStorage, ScheduledExecutorService executor, String cleaningTime) {

        Runnable cleanMap = () -> {
            for (Map.Entry<Integer, Captcha> entry : captchaStorage.getCaptchaStorage().entrySet()) {
                if (entry.getValue().isExpired()) {
                    captchaStorage.remove(entry.getKey());
                }
            }
            LOG.trace("Remain values: " + captchaStorage.getCaptchaStorage().values());
        };

        executor.scheduleAtFixedRate(cleanMap, Long.parseLong(cleaningTime), Long.parseLong(cleaningTime), TimeUnit.SECONDS);
    }
}
