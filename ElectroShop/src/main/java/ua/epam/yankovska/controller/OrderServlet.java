package ua.epam.yankovska.controller;

import org.apache.log4j.Logger;
import ua.epam.yankovska.bean.impl.CardBean;
import ua.epam.yankovska.bean.impl.CheckoutInfoBean;
import ua.epam.yankovska.constants.PaymentType;
import ua.epam.yankovska.entity.Order;
import ua.epam.yankovska.entity.OrderedItem;
import ua.epam.yankovska.entity.Product;
import ua.epam.yankovska.entity.User;
import ua.epam.yankovska.exception.AppRuntimeException;
import ua.epam.yankovska.service.OrderService;
import ua.epam.yankovska.storage.BasketStorage;
import ua.epam.yankovska.validator.CardValidator;
import ua.epam.yankovska.validator.CheckoutInfoValidator;
import ua.epam.yankovska.validator.Validator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static ua.epam.yankovska.constants.Constant.ContextAttribute.ORDER_SERVICE;
import static ua.epam.yankovska.constants.Constant.JSPWays.CONFIRM;
import static ua.epam.yankovska.constants.Constant.JSPWays.ITEMS;
import static ua.epam.yankovska.constants.Constant.JSPWays.ORDER;
import static ua.epam.yankovska.constants.Constant.JSPWays.ORDER_JSP;
import static ua.epam.yankovska.constants.Status.ACCEPTED;

/**
 * Servlet that works with user's orders.
 *
 * @author Kateryna_Yankovska
 */

@WebServlet("/order")
public class OrderServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(OrderServlet.class.getName());
    private OrderService orderService;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ServletContext context = servletConfig.getServletContext();
        orderService = (OrderService) context.getAttribute(ORDER_SERVICE);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BasketStorage basketStorage = (BasketStorage) request.getSession().getAttribute("basket");
        if (Objects.isNull(basketStorage) || basketStorage.countProducts() == 0) {
            response.sendRedirect(request.getContextPath() + ITEMS);
        } else {
            request.getRequestDispatcher(ORDER_JSP).forward(request, response);
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false);
        Validator validator = new CheckoutInfoValidator();
        CheckoutInfoBean checkoutInfoBean = new CheckoutInfoBean(request);
        Map<String, String> addressErrors = validator.validate(checkoutInfoBean);

        Map<String, String> cardErrors = new HashMap<>();
        if (checkoutInfoBean.getPayment().equals(PaymentType.card)) {
            validator = new CardValidator();
            CardBean cardBean = new CardBean(request);
            cardErrors = validator.validate(cardBean);
        }

        if (!addressErrors.isEmpty() || !cardErrors.isEmpty()) {
            session.setAttribute("addressErrors", addressErrors);
            session.setAttribute("cardErrors", cardErrors);
            response.sendRedirect(request.getContextPath() + ORDER);
        } else {
            User user = (User) session.getAttribute("user");
            BasketStorage basketStorage = (BasketStorage) session.getAttribute("basket");
            Order order = makeOrder(user, checkoutInfoBean, basketStorage);
            session.setAttribute("order", order);
            try {
                int orderId = orderService.add(order);
                basketStorage.deleteAll();
                session.setAttribute("orderId", orderId);
                response.sendRedirect(request.getContextPath() + CONFIRM);
            } catch (AppRuntimeException e) {
                LOG.error(e);
            }
        }
    }

    private Order makeOrder(User user, CheckoutInfoBean checkoutInfoBean, BasketStorage basketStorage) {
        Order order = new Order();
        order.setUser(user);
        order.setPayment(checkoutInfoBean.getPayment());
        order.setDate(LocalDateTime.now());
        order.setAddress(checkoutInfoBean.combineAddress());
        order.setDetails("Everything is good!");
        order.setStatus(ACCEPTED);
        List<OrderedItem> orderedItems = new ArrayList<>();
        for (Map.Entry<Product, Integer> basket : basketStorage.getAll().entrySet()) {
            OrderedItem orderedItem = new OrderedItem(basket.getKey(), basket.getValue());
            orderedItems.add(orderedItem);
        }
        order.setOrderedItem(orderedItems);
        return order;
    }
}
