package ua.epam.yankovska.controller;

import org.apache.log4j.Logger;
import ua.epam.yankovska.entity.User;
import ua.epam.yankovska.service.AvatarService;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

import static ua.epam.yankovska.constants.Constant.SessionAttribute.USER;

/**
 * Servlet that works with avatar.
 *
 * @author Kateryna_Yankovska
 */

@WebServlet("/avatar")
@MultipartConfig
public class AvatarServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(AvatarServlet.class.getName());
    private static final String IMAGE_SERVICE = "imageService";
    private static final String JPG = "jpg";
    private static final String IMAGE_JPG = "image/jpg";
    private AvatarService avatarService;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ServletContext context = getServletContext();
        avatarService = (AvatarService) context.getAttribute(IMAGE_SERVICE);
    }

    @Override
    public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        HttpSession session = httpServletRequest.getSession(false);
        User user = (User) session.getAttribute(USER);
        processRequest(httpServletRequest, httpServletResponse, user);
    }

    private void processRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, User user) {
        BufferedImage avatar = avatarService.findAvatar(httpServletRequest, user);
        try {
            httpServletResponse.setContentType(IMAGE_JPG);
            OutputStream os = httpServletResponse.getOutputStream();
            ImageIO.write(avatar, JPG, os);
            os.flush();
            os.close();
        } catch (IOException e) {
            LOG.error("IOException" + e.getMessage());
        }
    }
}
