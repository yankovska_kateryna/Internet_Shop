package ua.epam.yankovska.controller;

import org.apache.log4j.Logger;
import ua.epam.yankovska.bean.impl.RegistrationFormBean;
import ua.epam.yankovska.captcha.Captcha;
import ua.epam.yankovska.captcha.CaptchaGenerator;
import ua.epam.yankovska.converter.impl.RegistrationBeanConverter;
import ua.epam.yankovska.entity.User;
import ua.epam.yankovska.exception.AppRuntimeException;
import ua.epam.yankovska.service.AvatarService;
import ua.epam.yankovska.service.CaptchaService;
import ua.epam.yankovska.service.UserService;
import ua.epam.yankovska.strategy.captchaStrategy.CaptchaStrategy;
import ua.epam.yankovska.validator.RegistrationValidator;
import ua.epam.yankovska.validator.Validator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;

import static ua.epam.yankovska.constants.Constant.ContextAttribute.CAPTCHA_SERVICE;
import static ua.epam.yankovska.constants.Constant.ContextAttribute.IMAGE_SERVICE;
import static ua.epam.yankovska.constants.Constant.ContextAttribute.USER_SERVICE;
import static ua.epam.yankovska.constants.Constant.JSPWays.REGISTRATION;
import static ua.epam.yankovska.constants.Constant.JSPWays.REGISTRATION_JSP;
import static ua.epam.yankovska.constants.Constant.SessionAttribute.ERRORS;

/**
 * Servlet that works with the registration form and user's inputs.
 *
 * @author Kateryna_Yankovska
 */

@WebServlet("/registration")
@MultipartConfig
public class RegistrationServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(RegistrationServlet.class.getName());
    private static final String STRATEGY = "strategy";
    private static final String BEAN = "bean";
    private static final String CAPTCHA = "captcha";
    private static final String ERROR_CAPTCHA = "error_captcha";
    private static final String INCORRECT_INPUT = "Incorrect input";
    private static final String TIME_IS_OUT = "Time is out!";
    private static final String TIME_EXPIRED = "timeExpired";
    private static final String ERROR_LOGIN = "error_login";
    private static final String ERROR_EMAIL = "error_email";
    private static final String USER_WITH_SUCH_LOGIN_EXISTS = "User with such login exists";
    private static final String USER_WITH_SUCH_EMAIL_EXISTS = "User with such email exists";
    private static final String SUCCESS_RESULT = "success_result";
    private static final String USER_WAS_SUCCESSFULLY_ADDED = "User was successfully registered!";
    private UserService userService;
    private CaptchaService captchaService;
    private AvatarService avatarService;
    private CaptchaStrategy captchaStrategy;
    private long timeExpired;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ServletContext context = servletConfig.getServletContext();
        userService = (UserService) context.getAttribute(USER_SERVICE);
        captchaService = (CaptchaService) context.getAttribute(CAPTCHA_SERVICE);
        avatarService = (AvatarService) context.getAttribute(IMAGE_SERVICE);
        captchaStrategy = (CaptchaStrategy) context.getAttribute(STRATEGY);
        timeExpired = Long.parseLong(context.getInitParameter(TIME_EXPIRED));
    }

    public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
        CaptchaGenerator captchaGenerator = new CaptchaGenerator(timeExpired);
        int id = captchaService.add(captchaGenerator.generateCaptcha());
        captchaStrategy.setCaptchaId(httpServletRequest, httpServletResponse, id);
        httpServletRequest.getRequestDispatcher(REGISTRATION_JSP).forward(httpServletRequest, httpServletResponse);
    }

    @Override
    public void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        Validator validator = new RegistrationValidator();
        RegistrationFormBean registrationFormBean = new RegistrationFormBean(httpServletRequest);
        Map<String, String> errors = validator.validate(registrationFormBean);
        validateCaptcha(httpServletRequest, errors);
        try {
            HttpSession session = httpServletRequest.getSession(false);
            User user = null;
            if (errors.isEmpty()) {
                RegistrationBeanConverter converter = new RegistrationBeanConverter();
                user = converter.convertToUser(registrationFormBean);
                errors = checkIfUserExists(user, errors);
            }
            if (errors.isEmpty()) {
                userService.create(user);
                avatarService.saveAvatar(httpServletRequest, user);
                session.setAttribute(SUCCESS_RESULT, USER_WAS_SUCCESSFULLY_ADDED);
                httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + REGISTRATION);
            } else {
                session.setAttribute(BEAN, registrationFormBean);
                session.setAttribute(ERRORS, errors);
                httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + REGISTRATION);
            }
        } catch (AppRuntimeException e) {
            LOG.error(e);
        }
    }

    private void validateCaptcha(HttpServletRequest httpServletRequest, Map<String, String> errors) {
        int indexString = captchaStrategy.getCaptchaId(httpServletRequest);
        Captcha captcha = captchaService.getCaptcha(indexString);
        if (Objects.isNull(captcha) || captcha.isExpired()) {
            errors.put(ERROR_CAPTCHA, TIME_IS_OUT);
        } else if (!httpServletRequest.getParameter(CAPTCHA).trim().equals(captcha.toString())) {
            errors.put(ERROR_CAPTCHA, INCORRECT_INPUT);
        }
    }

    private Map<String, String> checkIfUserExists(User user, Map<String, String> errors) {
        boolean isExistsLogin = userService.checkLogin(user.getLogin());
        boolean isExistsEmail = userService.checkEmail(user.getEmail());
        if (isExistsLogin && isExistsEmail) {
            errors.put(ERROR_LOGIN, USER_WITH_SUCH_LOGIN_EXISTS);
            errors.put(ERROR_EMAIL, USER_WITH_SUCH_EMAIL_EXISTS);
        } else if (isExistsEmail) {
            errors.put(ERROR_EMAIL, USER_WITH_SUCH_EMAIL_EXISTS);
        } else if (isExistsLogin) {
            errors.put(ERROR_LOGIN, USER_WITH_SUCH_LOGIN_EXISTS);
        }
        return errors;
    }
}
