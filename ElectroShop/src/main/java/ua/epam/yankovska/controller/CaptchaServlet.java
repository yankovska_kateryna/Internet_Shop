package ua.epam.yankovska.controller;

import org.apache.log4j.Logger;
import ua.epam.yankovska.captcha.Captcha;
import ua.epam.yankovska.service.CaptchaService;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Servlet that works with captcha.
 *
 * @author Kateryna_Yankovska
 */

@WebServlet("/captcha")
public class CaptchaServlet extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(CaptchaServlet.class.getName());
    private static final String CAPTCHA_SERVICE = "captchaService";
    private static final String ID = "id";
    private static final String IMAGE_PNG = "image/png";
    private static final String PNG = "png";
    private CaptchaService captchaService;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ServletContext context = getServletContext();
        captchaService = (CaptchaService) context.getAttribute(CAPTCHA_SERVICE);
    }

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse servletResponse) throws IOException {
        processRequest(httpServletRequest, servletResponse);
    }

    private void processRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        int captchaIndex = Integer.parseInt(httpServletRequest.getParameter(ID));
        httpServletResponse.setContentType(IMAGE_PNG);
        Captcha captcha = captchaService.getCaptcha(captchaIndex);
        OutputStream os = httpServletResponse.getOutputStream();
        ImageIO.write(captcha.getBufferedImage(), PNG, os);
        os.flush();
        os.close();
        LOG.trace("Captcha written");
    }
}
