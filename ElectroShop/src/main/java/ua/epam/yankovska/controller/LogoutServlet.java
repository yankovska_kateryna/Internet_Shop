package ua.epam.yankovska.controller;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

/**
 * Servlet that cleans session.
 *
 * @author Kateryna_Yankovska
 */

@WebServlet("/exit")
public class LogoutServlet extends HttpServlet{

    private static final String INDEX = "/index";

    @Override
    public void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        HttpSession session = httpServletRequest.getSession(false);

        if(Objects.nonNull(session)){
            session.invalidate();
        }
        httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + INDEX);
    }
}
