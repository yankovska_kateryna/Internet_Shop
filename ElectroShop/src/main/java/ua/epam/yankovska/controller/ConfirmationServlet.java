package ua.epam.yankovska.controller;

import ua.epam.yankovska.entity.Order;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

import static ua.epam.yankovska.constants.Constant.JSPWays.CONFIRM_JSP;
import static ua.epam.yankovska.constants.Constant.JSPWays.INDEX;

@WebServlet("/confirm")
public class ConfirmationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Order order = (Order) request.getSession().getAttribute("order");
        if(Objects.isNull(order)) {
            response.sendRedirect(request.getContextPath() + INDEX);
        } else {
            request.getRequestDispatcher(CONFIRM_JSP).forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws  IOException {
        request.getSession().removeAttribute("order");
        request.getSession().removeAttribute("oderId");
        response.sendRedirect(request.getContextPath() + INDEX);
    }
}
