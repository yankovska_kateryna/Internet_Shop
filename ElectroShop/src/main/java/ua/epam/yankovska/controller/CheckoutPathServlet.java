package ua.epam.yankovska.controller;

import ua.epam.yankovska.storage.BasketStorage;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static ua.epam.yankovska.constants.Constant.JSPWays.BASKET;
import static ua.epam.yankovska.constants.Constant.JSPWays.ITEMS;
import static ua.epam.yankovska.constants.Constant.JSPWays.ORDER;
import static ua.epam.yankovska.constants.Constant.JSPWays.REGISTRATION;

@WebServlet("/path")
public class CheckoutPathServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        BasketStorage basketStorage = (BasketStorage) session.getAttribute("basket");
        if (session.getAttribute("user") == null && basketStorage.countProducts() != 0) {
            session.setAttribute("previousPage", BASKET);
            response.sendRedirect(request.getContextPath() + REGISTRATION);
        } else if (basketStorage.countProducts() == 0) {
            response.sendRedirect(request.getContextPath() + ITEMS);
        } else {
            response.sendRedirect(request.getContextPath() + ORDER);
        }
    }
}
