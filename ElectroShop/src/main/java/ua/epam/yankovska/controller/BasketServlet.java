package ua.epam.yankovska.controller;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import ua.epam.yankovska.bean.impl.AJAXBean;
import ua.epam.yankovska.dto.AJAXResponseDTO;
import ua.epam.yankovska.entity.Product;
import ua.epam.yankovska.exception.DBException;
import ua.epam.yankovska.service.ProductService;
import ua.epam.yankovska.storage.BasketStorage;
import ua.epam.yankovska.storage.impl.BasketStorageImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static ua.epam.yankovska.constants.Constant.ContextAttribute.PRODUCT_SERVICE;
import static ua.epam.yankovska.constants.Constant.JSPWays.BASKET_JSP;

/**
 * Servlet that works with user's basket.
 *
 * @author Kateryna_Yankovska
 */

@WebServlet("/basket")
public class BasketServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(BasketServlet.class.getName());
    private ProductService productService;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ServletContext context = servletConfig.getServletContext();
        productService = (ProductService) context.getAttribute(PRODUCT_SERVICE);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(BASKET_JSP).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        BasketStorage basketStorage = (BasketStorage) session.getAttribute("basket");

        AJAXBean ajaxBean = new AJAXBean(request);

        Product product;
        try {
            product = productService.getById(ajaxBean.getId());
            if (Objects.nonNull(product)) {
                basketStorage.add(product);
            }
        } catch (DBException e) {
            LOG.error("Problem in DB");
        }

        AJAXResponseDTO ajaxDto = new AJAXResponseDTO();
        ajaxDto.setCount(basketStorage.countProducts());
        ajaxDto.setPrice(basketStorage.sumOfThePurchase());
        writeToJSON(ajaxDto, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        BasketStorage basketStorage = (BasketStorage) session.getAttribute("basket");

        AJAXBean ajaxBean = new AJAXBean(request);
        List<Product> basket = basketStorage.getProducts();
        double price = 0;
        int count = 0;
        for (Product aBasket : basket) {
            if (aBasket.getId() == ajaxBean.getId()) {
                basketStorage.set(aBasket, ajaxBean.getCount());
                price = aBasket.getPrice();
                count = basketStorage.getAll().get(aBasket);
            }
        }

        AJAXResponseDTO ajaxDto = new AJAXResponseDTO();
        ajaxDto.setCount(basketStorage.countProducts());
        ajaxDto.setPrice(basketStorage.sumOfThePurchase());
        ajaxDto.setSubtotal(count, price);
        writeToJSON(ajaxDto, response);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        BasketStorage basketStorage = (BasketStorage) session.getAttribute("basket");

        AJAXBean ajaxBean = new AJAXBean(request);
        if(ajaxBean.getId() == -1) {
            basketStorage.deleteAll();
        } else {
            List<Product> basket = basketStorage.getProducts();
            for (Product aBasket : basket) {
                if (aBasket.getId() == ajaxBean.getId()) {
                    basketStorage.delete(aBasket);
                }
            }
        }

        AJAXResponseDTO ajaxDto = new AJAXResponseDTO();
        ajaxDto.setCount(basketStorage.countProducts());
        ajaxDto.setPrice(basketStorage.sumOfThePurchase());
        writeToJSON(ajaxDto, response);
    }

    private void writeToJSON(AJAXResponseDTO ajaxDto, HttpServletResponse response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        String jsonInString = mapper.writeValueAsString(ajaxDto);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonInString);
    }
}
