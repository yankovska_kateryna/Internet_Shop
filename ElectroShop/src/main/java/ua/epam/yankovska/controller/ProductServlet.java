package ua.epam.yankovska.controller;

import org.apache.log4j.Logger;
import ua.epam.yankovska.bean.impl.ItemsFilterBean;
import ua.epam.yankovska.entity.Product;
import ua.epam.yankovska.exception.DBException;
import ua.epam.yankovska.service.CategoryService;
import ua.epam.yankovska.service.ProducerService;
import ua.epam.yankovska.service.ProductService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static ua.epam.yankovska.constants.Constant.ContextAttribute.CATEGORY_SERVICE;
import static ua.epam.yankovska.constants.Constant.ContextAttribute.PRODUCER_SERVICE;
import static ua.epam.yankovska.constants.Constant.ContextAttribute.PRODUCT_SERVICE;
import static ua.epam.yankovska.constants.Constant.JSPWays.STORE_JSP;
import static ua.epam.yankovska.constants.Constant.SessionAttribute.ERRORS;

/**
 * Servlet that works with items from the shop.
 *
 * @author Kateryna_Yankovska
 */

@WebServlet("/items")
public class ProductServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(ProductServlet.class.getName());
    private ProductService productService;
    private ProducerService producerService;
    private CategoryService categoryService;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ServletContext context = servletConfig.getServletContext();
        productService = (ProductService) context.getAttribute(PRODUCT_SERVICE);
        categoryService = (CategoryService) context.getAttribute(CATEGORY_SERVICE);
        producerService = (ProducerService) context.getAttribute(PRODUCER_SERVICE);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        double priceMin = 0, priceMax = 0;

        try {
            request.setAttribute("categories", categoryService.getAll());
            request.setAttribute("producers", producerService.getAll());
            priceMin = productService.getPriceMin();
            priceMax = productService.getPriceMax();
        } catch (DBException e) {
            LOG.error("Problem in db");
        }

        int productNumber = 0;
        ItemsFilterBean itemsFilterBean = new ItemsFilterBean(request, priceMin, priceMax);
        try {
            productNumber = productService.countAll(itemsFilterBean);
            List<Product> products = productService.getAll(itemsFilterBean);
            request.setAttribute("products", products);
            if (products.isEmpty()) {
                request.setAttribute(ERRORS, "There is no elements with this parameters!");
            }
        } catch (DBException e) {
            LOG.error("Problem in db");
        }

        request.setAttribute("lastPage", getLastPage(productNumber, itemsFilterBean));
        request.setAttribute("filterBean", itemsFilterBean);
        request.getRequestDispatcher(STORE_JSP).forward(request, response);
    }

    private int getLastPage(int productNumber, ItemsFilterBean itemsFilterBean) {
        return (int) Math.ceil((double) productNumber / itemsFilterBean.getProductsPerPage());
    }
}
