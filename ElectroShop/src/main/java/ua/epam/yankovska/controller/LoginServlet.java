package ua.epam.yankovska.controller;

import org.apache.log4j.Logger;
import ua.epam.yankovska.entity.User;
import ua.epam.yankovska.exception.AppRuntimeException;
import ua.epam.yankovska.service.UserService;
import ua.epam.yankovska.util.Password;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

import static ua.epam.yankovska.constants.Constant.JSPWays.BASKET;
import static ua.epam.yankovska.constants.Constant.SessionAttribute.USER;

/**
 * Servlet that make authentication of the user and checks inputting login and password/
 *
 * @author Kateryna_Yankovska
 */

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(LoginServlet.class.getName());
    private static final String USER_SERVICE = "userService";
    private static final String INCORRECT_PASSWORD = "Incorrect password";
    private static final String USER_WITH_THIS_LOGIN_NOT_EXIST = "User with this login doesn't exist";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private UserService userService;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ServletContext context = servletConfig.getServletContext();
        userService = (UserService) context.getAttribute(USER_SERVICE);
    }

    @Override
    public void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        HttpSession session = httpServletRequest.getSession(true);
        String error = "";

        LOG.debug("Starting DB operation");

        User user;
        try {
            String login = httpServletRequest.getParameter(LOGIN);
            String password = Password.hash(httpServletRequest.getParameter(PASSWORD));
            user = userService.get(login);
            if (Objects.nonNull(user)) {
                if (!user.getPassword().equals(password)) {
                    error = INCORRECT_PASSWORD;
                }
            } else {
                error = USER_WITH_THIS_LOGIN_NOT_EXIST;
            }
            if (!error.isEmpty()) {
                session.setAttribute("errorLogin", error);
            } else {
                session.setAttribute(USER, user);
            }
            if(session.getAttribute("previousPage") == null) {
                httpServletResponse.sendRedirect(httpServletRequest.getHeader("referer"));
            } else {
                session.removeAttribute("previousPage");
                httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + BASKET);
            }
        } catch (AppRuntimeException e) {
            LOG.error(e);
        }
    }
}
