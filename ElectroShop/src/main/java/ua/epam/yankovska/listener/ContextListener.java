package ua.epam.yankovska.listener;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import ua.epam.yankovska.captcha.CaptchaStorageCleaner;
import ua.epam.yankovska.dao.impl.SQLCategoryDAO;
import ua.epam.yankovska.dao.impl.SQLOrderDAO;
import ua.epam.yankovska.dao.impl.SQLProducerDAO;
import ua.epam.yankovska.dao.impl.SQLProductDAO;
import ua.epam.yankovska.dao.impl.SQLUserDAO;
import ua.epam.yankovska.db.ConnectionHolder;
import ua.epam.yankovska.db.DBManager;
import ua.epam.yankovska.db.TransactionManager;
import ua.epam.yankovska.db.impl.ConnectionManagerImpl;
import ua.epam.yankovska.db.impl.TransactionManagerImpl;
import ua.epam.yankovska.service.impl.AvatarServiceImpl;
import ua.epam.yankovska.service.impl.CaptchaServiceImpl;
import ua.epam.yankovska.service.impl.CategoryServiceImpl;
import ua.epam.yankovska.service.impl.OrderServiceImpl;
import ua.epam.yankovska.service.impl.ProducerServiceImpl;
import ua.epam.yankovska.service.impl.ProductServiceImpl;
import ua.epam.yankovska.service.impl.UserServiceImpl;
import ua.epam.yankovska.storage.CaptchaStorage;
import ua.epam.yankovska.storage.impl.CaptchaStorageImpl;
import ua.epam.yankovska.strategy.captchaStrategy.CaptchaStrategy;
import ua.epam.yankovska.strategy.captchaStrategy.impl.CookieStrategy;
import ua.epam.yankovska.strategy.captchaStrategy.impl.HiddenFieldStrategy;
import ua.epam.yankovska.strategy.captchaStrategy.impl.SessionStrategy;
import ua.epam.yankovska.strategy.localizationStratagy.LocalizationStoreManager;
import ua.epam.yankovska.strategy.localizationStratagy.LocalizationStoreStrategy;
import ua.epam.yankovska.strategy.localizationStratagy.impl.CookieStoreStrategy;
import ua.epam.yankovska.strategy.localizationStratagy.impl.SessionStoreStrategy;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static ua.epam.yankovska.constants.Constant.InitAttributes.CAPTCHA_HANDLER_STRATEGY;
import static ua.epam.yankovska.constants.Constant.InitAttributes.LOCALE_HANDLER_STRATEGY;
import static ua.epam.yankovska.constants.Constant.InitAttributes.STRATEGY_LOCALE;

@WebListener
public class ContextListener implements ServletContextListener {

    private static final Logger LOG = Logger.getLogger(ContextListener.class.getName());
    private static final String USER_SERVICE = "userService";
    private static final String CAPTCHA_SERVICE = "captchaService";
    private static final String IMAGE_SERVICE = "imageService";
    private static final String PRODUCT_SERVICE = "productService";
    private static final String CATEGORY_SERVICE = "categoryService";
    private static final String PRODUCER_SERVICE = "producerService";
    private static final String ORDER_SERVICE = "orderService";
    private static final String STRATEGY = "strategy";
    private static final String SESSION = "session";
    private static final String COOKIE = "cookie";
    private static final String HIDDEN = "hidden";
    private static final String CAPTCHA_CLEANER = "captchaCleaner";
    private static final String AVATAR_PATH = "avatarPath";
    private static final String COOKIE_EXPIRED = "cookieExpired";
    private ScheduledExecutorService executor;

    public void contextInitialized(ServletContextEvent sce) {
        LOG.info("Starting initialization of the context");

        ServletContext context = sce.getServletContext();
        DBManager connectionManagerImpl = new ConnectionManagerImpl("jdbc/electroShop");
        ConnectionHolder connectionHolder = new ConnectionHolder(connectionManagerImpl);
        TransactionManager transactionManager = new TransactionManagerImpl(connectionHolder);
        CaptchaStorage captchaStorage = new CaptchaStorageImpl();

        context.setAttribute(USER_SERVICE, new UserServiceImpl(new SQLUserDAO(connectionHolder), transactionManager));
        context.setAttribute(CAPTCHA_SERVICE, new CaptchaServiceImpl(captchaStorage));
        context.setAttribute(PRODUCT_SERVICE, new ProductServiceImpl(new SQLProductDAO(connectionHolder), transactionManager));
        context.setAttribute(CATEGORY_SERVICE, new CategoryServiceImpl(new SQLCategoryDAO(connectionHolder), transactionManager));
        context.setAttribute(PRODUCER_SERVICE, new ProducerServiceImpl(new SQLProducerDAO(connectionHolder), transactionManager));
        context.setAttribute(IMAGE_SERVICE, new AvatarServiceImpl(context.getInitParameter(AVATAR_PATH)));
        context.setAttribute(STRATEGY, getHandler(fillCaptchaHandlerStrategyMap(), context));
        context.setAttribute(ORDER_SERVICE, new OrderServiceImpl(new SQLOrderDAO(connectionHolder), transactionManager));
        context.setAttribute(STRATEGY_LOCALE, new LocalizationStoreManager(getLanguageCookieExpireTime(context)));

        executor = getExecutor();
        CaptchaStorageCleaner captchaStorageCleaner = new CaptchaStorageCleaner();
        captchaStorageCleaner.executeThread(captchaStorage, executor, context.getInitParameter(CAPTCHA_CLEANER));
        LOG.info("Thread starts");

        LOG.info("Initialization of the context was finished");
    }

    private ScheduledExecutorService getExecutor() {
        return Executors.newSingleThreadScheduledExecutor(r -> {
            Thread t = Executors.defaultThreadFactory().newThread(r);
            t.setDaemon(true);
            return t;
        });
    }

    public void contextDestroyed(ServletContextEvent sce) {
        executor.shutdown();
        LOG.info("Context was destroyed");
    }

    private Map<String, CaptchaStrategy> fillCaptchaHandlerStrategyMap() {
        Map<String, CaptchaStrategy> captchaHandlerStrategyMap = new HashMap<>();
        captchaHandlerStrategyMap.put(SESSION, new SessionStrategy());
        captchaHandlerStrategyMap.put(COOKIE, new CookieStrategy());
        captchaHandlerStrategyMap.put(HIDDEN, new HiddenFieldStrategy());

        return captchaHandlerStrategyMap;
    }

    private CaptchaStrategy getHandler(Map<String, CaptchaStrategy> captchaHandlerStrategyMap, ServletContext servletContext) {
        return captchaHandlerStrategyMap.get(servletContext.getInitParameter(CAPTCHA_HANDLER_STRATEGY));
    }

    private int getLanguageCookieExpireTime(ServletContext context) {
        int cookieTime = 0;
        String expireTime = context.getInitParameter(COOKIE_EXPIRED);
        if (NumberUtils.isNumber(expireTime)) {
            cookieTime = Integer.parseInt(expireTime);
        }
        return cookieTime;
    }
}
