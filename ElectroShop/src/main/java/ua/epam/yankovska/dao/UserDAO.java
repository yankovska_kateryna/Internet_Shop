package ua.epam.yankovska.dao;

import ua.epam.yankovska.entity.User;

public interface UserDAO extends DAO {
    boolean create(User user);

    User get(String login);

    boolean checkLogin(String login);

    boolean checkEmail(String email);
}
