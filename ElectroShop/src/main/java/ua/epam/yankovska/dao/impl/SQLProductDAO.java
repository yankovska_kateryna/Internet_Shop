package ua.epam.yankovska.dao.impl;

import org.apache.log4j.Logger;
import ua.epam.yankovska.bean.impl.ItemsFilterBean;
import ua.epam.yankovska.builder.SQLBuilder;
import ua.epam.yankovska.dao.ProductDAO;
import ua.epam.yankovska.db.ConnectionHolder;
import ua.epam.yankovska.db.Util;
import ua.epam.yankovska.entity.Product;
import ua.epam.yankovska.exception.DBException;
import ua.epam.yankovska.util.Converter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static ua.epam.yankovska.constants.Constant.Requests.SQL_GET_BY_ID;
import static ua.epam.yankovska.constants.Constant.Requests.SQL_GET_MAX_PRICE;
import static ua.epam.yankovska.constants.Constant.Requests.SQL_GET_MIN_PRICE;

/**
 * Class that works with database.
 *
 * @author Kateryna_Yankovska
 */

public class SQLProductDAO implements ProductDAO {
    private static final Logger LOG = Logger.getLogger(SQLProductDAO.class.getName());
    private ConnectionHolder connectionHolder;

    public SQLProductDAO(ConnectionHolder connectionHolder) {
        this.connectionHolder = connectionHolder;
    }

    @Override
    public double getPriceMin() {
        return getPrice(SQL_GET_MIN_PRICE);
    }

    @Override
    public double getPriceMax() {
        return getPrice(SQL_GET_MAX_PRICE);
    }

    private double getPrice(String query) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            LOG.debug("Starting getting price");
            preparedStatement = connectionHolder.getConnection().prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getDouble("price");
            }
        } catch (SQLException e) {
            LOG.error("SQLException: ", e);
            throw new DBException("Problem with DB", e);
        } finally {
            Util.close(resultSet);
            Util.close(preparedStatement);
        }
        return 0.00;
    }

    @Override
    public List<Product> getAll(ItemsFilterBean bean) {
        List<Product> products = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            LOG.debug("Starting getting categories");
            String name = bean.getName();
            double priceMin = bean.getPriceMin();
            double priceMax = bean.getPriceMax();
            String category = new Converter().convertIntArrayToString(bean.getCategory());
            String producer = new Converter().convertIntArrayToString(bean.getProducer());
            String sort = new Converter().convertSort(bean.getSorter());

            preparedStatement = connectionHolder.getConnection().prepareStatement(
                    new SQLBuilder().select("products.id", "products.name",
                            "description", "price", "categories.name", "producers.name")
                            .from("products")
                            .join("categories", "category_id", "categories.id")
                            .join("producers", "producer_id", "producers.id")
                            .where()
                            .between("price")
                            .constraint("products.name", name)
                            .in("category_id", category)
                            .in("producer_id", producer)
                            .orderBy(sort)
                            .limit()
                            .offset()
                            .build());

            int k = 0;
            preparedStatement.setDouble(++k, priceMin);
            preparedStatement.setDouble(++k, priceMax);
            if (!name.equals("")) {
                preparedStatement.setString(++k, name);
            }
            preparedStatement.setInt(++k, bean.getProductsPerPage());
            preparedStatement.setInt(++k, (bean.getPage() - 1) * bean.getProductsPerPage());

            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                products.add(extractProduct(resultSet));
            }
            return products;
        } catch (SQLException e) {
            LOG.error("Exception in getting all products: ", e);
            throw new DBException("Problem with DB", e);
        } finally {
            Util.close(resultSet);
            Util.close(preparedStatement);
        }
    }

    @Override
    public int countAll(ItemsFilterBean bean) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int productNumber;
        try {
            LOG.debug("Starting counting products");
            String name = bean.getName();
            double priceMin = bean.getPriceMin();
            double priceMax = bean.getPriceMax();
            String category = new Converter().convertIntArrayToString(bean.getCategory());
            String producer = new Converter().convertIntArrayToString(bean.getProducer());
            preparedStatement = connectionHolder.getConnection().prepareStatement(
                    new SQLBuilder().select()
                            .count()
                            .from("products")
                            .join("categories", "category_id", "categories.id")
                            .join("producers", "producer_id", "producers.id")
                            .where()
                            .between("price")
                            .constraint("products.name", name)
                            .in("category_id", category)
                            .in("producer_id", producer)
                            .build());

            int k = 0;
            preparedStatement.setDouble(++k, priceMin);
            preparedStatement.setDouble(++k, priceMax);
            if (!name.equals("")) {
                preparedStatement.setString(++k, name);
            }
            resultSet = preparedStatement.executeQuery();
            productNumber = resultSet.next() ? resultSet.getInt("count") : 0;
        } catch (SQLException e) {
            LOG.error("Exception in counting number of products: ", e);
            throw new DBException("Problem with DB", e);
        } finally {
            Util.close(resultSet);
            Util.close(preparedStatement);
        }
        return productNumber;
    }

    @Override
    public Product getById(int id) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Product product = null;
        try {
            LOG.debug("Starting counting products");
            preparedStatement = connectionHolder.getConnection().prepareStatement(SQL_GET_BY_ID);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                product = extractProduct(resultSet);
            }
            return product;
        } catch (SQLException e) {
            LOG.error("Exception in counting number of products: ", e);
            throw new DBException("Problem with DB", e);
        } finally {
            Util.close(resultSet);
            Util.close(preparedStatement);
        }
    }

    private Product extractProduct(ResultSet rs) throws SQLException {
        Product product = new Product();
        product.setId(rs.getInt("id"));
        product.setName(rs.getString("name"));
        product.setPrice(rs.getDouble("price"));
        product.setDescription(rs.getString("description"));
        product.setCategory(rs.getString("categories.name"));
        product.setProducer(rs.getString("producers.name"));
        return product;
    }
}
