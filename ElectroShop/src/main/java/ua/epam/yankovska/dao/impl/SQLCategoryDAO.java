package ua.epam.yankovska.dao.impl;

import org.apache.log4j.Logger;
import ua.epam.yankovska.dao.CategoryDAO;
import ua.epam.yankovska.db.ConnectionHolder;
import ua.epam.yankovska.db.Util;
import ua.epam.yankovska.entity.Category;
import ua.epam.yankovska.exception.DBException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static ua.epam.yankovska.constants.Constant.Requests.SQL_GET_ALL_CATEGORIES;

/**
 * Class that works with database.
 *
 * @author Kateryna_Yankovska
 */

public class SQLCategoryDAO implements CategoryDAO {
    private static final Logger LOG = Logger.getLogger(SQLCategoryDAO.class.getName());
    private ConnectionHolder connectionHolder;

    public SQLCategoryDAO(ConnectionHolder connectionHolder) {
        this.connectionHolder = connectionHolder;
    }

    @Override
    public List<Category> getAll() {
        List<Category> categories = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            LOG.debug("Starting getting categories");
            preparedStatement = connectionHolder.getConnection().prepareStatement(SQL_GET_ALL_CATEGORIES);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                categories.add(extractCategory(resultSet));
            }
            return categories;
        } catch (SQLException e) {
            LOG.error("Exception in getting all categories: ", e);
            throw new DBException("Problem with DB", e);
        } finally {
            Util.close(resultSet);
            Util.close(preparedStatement);
        }
    }

    private Category extractCategory(ResultSet rs) throws SQLException {
        Category category = new Category();
        category.setId(rs.getInt("id"));
        category.setName(rs.getString("name"));
        return category;
    }
}
