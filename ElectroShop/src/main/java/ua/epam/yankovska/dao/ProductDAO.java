package ua.epam.yankovska.dao;

import ua.epam.yankovska.bean.impl.ItemsFilterBean;
import ua.epam.yankovska.entity.Product;

import java.util.List;

public interface ProductDAO extends DAO{
    double getPriceMin();
    double getPriceMax();
    Product getById(int id);
    List<Product> getAll(ItemsFilterBean bean);
    int countAll(ItemsFilterBean bean);
}
