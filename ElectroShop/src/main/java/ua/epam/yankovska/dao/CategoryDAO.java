package ua.epam.yankovska.dao;

import ua.epam.yankovska.entity.Category;

import java.util.List;

public interface CategoryDAO extends DAO{
    List<Category> getAll();
}
