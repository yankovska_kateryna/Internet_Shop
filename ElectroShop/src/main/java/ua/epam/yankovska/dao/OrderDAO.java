package ua.epam.yankovska.dao;

import ua.epam.yankovska.entity.Order;

public interface OrderDAO {
    int add(Order order);
}
