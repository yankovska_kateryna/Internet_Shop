package ua.epam.yankovska.dao;

import ua.epam.yankovska.entity.Producer;

import java.util.List;

public interface ProducerDAO {
    List<Producer> getAll();
}