package ua.epam.yankovska.dao.impl;

import org.apache.log4j.Logger;
import ua.epam.yankovska.dao.OrderDAO;
import ua.epam.yankovska.db.ConnectionHolder;
import ua.epam.yankovska.db.Util;
import ua.epam.yankovska.entity.Order;
import ua.epam.yankovska.entity.OrderedItem;
import ua.epam.yankovska.exception.DBException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;

import static ua.epam.yankovska.constants.Constant.Requests.SQL_CREATE_ORDER;
import static ua.epam.yankovska.constants.Constant.Requests.SQL_CREATE_ORDER_PRODUCT;

/**
 * Class that works with database.
 *
 * @author Kateryna_Yankovska
 */

public class SQLOrderDAO implements OrderDAO {

    private static final Logger LOG = Logger.getLogger(SQLOrderDAO.class.getName());
    private ConnectionHolder connectionHolder;

    public SQLOrderDAO(ConnectionHolder connectionHolder) {
        this.connectionHolder = connectionHolder;
    }

    @Override
    public int add(Order order) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int orderId = 0;
        try {
            LOG.debug("Starting inserting order");
            preparedStatement = connectionHolder.getConnection().prepareStatement(SQL_CREATE_ORDER, Statement.RETURN_GENERATED_KEYS);
            fillPreparedStatementToOrder(order, preparedStatement);
            if (preparedStatement.executeUpdate() > 0) {
                resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    orderId = resultSet.getInt(1);
                }
                addOrdersItems(orderId, order.getOrderedItem());
            }
        } catch (SQLException e) {
            LOG.error("Exception in add order: ", e);
            throw new DBException("Problem with DB", e);
        } finally {
            Util.close(resultSet);
            Util.close(preparedStatement);
        }
        return orderId;
    }

    private void addOrdersItems(int orderId, List<OrderedItem> orderItems) {
        PreparedStatement preparedStatement = null;
        int count;
        double price;
        int productId;
        try {
            LOG.debug("Starting inserting order-product");
            preparedStatement = connectionHolder.getConnection().prepareStatement(SQL_CREATE_ORDER_PRODUCT);

            for (OrderedItem orderItem : orderItems) {
                count = orderItem.getCount();
                price = orderItem.getProduct().getPrice();
                productId = orderItem.getProduct().getId();

                fillPreparedStatementToOrdersProducts(orderId, count, price, productId, preparedStatement);

                preparedStatement.addBatch();
            }

            if (preparedStatement.executeBatch().length > 0) {
                LOG.debug("Order-product was inserted");
            }
        } catch (SQLException e) {
            LOG.error("Exception in add order-product: ", e);
            throw new DBException("Problem with DB", e);
        } finally {
            Util.close(preparedStatement);
        }
    }

    private void fillPreparedStatementToOrder(Order order, PreparedStatement preparedStatement) throws SQLException {
        int index = 1;
        preparedStatement.setString(index++, order.getStatus().toString());
        preparedStatement.setString(index++, order.getDetails());
        preparedStatement.setString(index++, Timestamp.valueOf(order.getDate()).toString());
        preparedStatement.setString(index++, order.getAddress());
        preparedStatement.setString(index++, order.getPayment());
        preparedStatement.setInt(index, order.getUser().getId());
    }

    private void fillPreparedStatementToOrdersProducts(int orderId, int count, double price, int productId, PreparedStatement preparedStatement) throws SQLException {
        int index = 1;
        preparedStatement.setInt(index++, orderId);
        preparedStatement.setInt(index++, productId);
        preparedStatement.setInt(index++, count);
        preparedStatement.setDouble(index, price);
    }
}
