package ua.epam.yankovska.dao.impl;

import org.apache.log4j.Logger;
import ua.epam.yankovska.dao.ProducerDAO;
import ua.epam.yankovska.db.ConnectionHolder;
import ua.epam.yankovska.db.Util;
import ua.epam.yankovska.entity.Producer;
import ua.epam.yankovska.exception.DBException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static ua.epam.yankovska.constants.Constant.Requests.SQL_GET_ALL_PRODUCERS;

/**
 * Class that works with database.
 *
 * @author Kateryna_Yankovska
 */

public class SQLProducerDAO implements ProducerDAO {
    private static final Logger LOG = Logger.getLogger(SQLProducerDAO.class.getName());
    private ConnectionHolder connectionHolder;

    public SQLProducerDAO(ConnectionHolder connectionHolder) {
        this.connectionHolder = connectionHolder;
    }

    @Override
    public List<Producer> getAll() {
        List<Producer> categories = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            LOG.debug("Starting getting producers");
            preparedStatement = connectionHolder.getConnection().prepareStatement(SQL_GET_ALL_PRODUCERS);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                categories.add(extractProducer(resultSet));
            }
            return categories;
        } catch (SQLException e) {
            LOG.error("Exception in getting all producers: ", e);
            throw new DBException("Problem with DB", e);
        } finally {
            Util.close(resultSet);
            Util.close(preparedStatement);
        }
    }

    private Producer extractProducer(ResultSet rs) throws SQLException {
        Producer producer = new Producer();
        producer.setId(rs.getInt("id"));
        producer.setName(rs.getString("name"));
        return producer;
    }
}
