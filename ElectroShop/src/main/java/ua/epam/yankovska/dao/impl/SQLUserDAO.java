package ua.epam.yankovska.dao.impl;

import org.apache.log4j.Logger;
import ua.epam.yankovska.dao.UserDAO;
import ua.epam.yankovska.db.ConnectionHolder;
import ua.epam.yankovska.db.Util;
import ua.epam.yankovska.entity.User;
import ua.epam.yankovska.exception.DBException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static ua.epam.yankovska.constants.Constant.Requests.SQL_CREATE_USER;
import static ua.epam.yankovska.constants.Constant.Requests.SQL_FIND_USER_BY_EMAIL;
import static ua.epam.yankovska.constants.Constant.Requests.SQL_FIND_USER_BY_LOGIN;

/**
 * Class that works with database.
 *
 * @author Kateryna_Yankovska
 */

public class SQLUserDAO implements UserDAO {
    private static final Logger LOG = Logger.getLogger(SQLUserDAO.class.getName());
    private ConnectionHolder connectionHolder;

    public SQLUserDAO(ConnectionHolder connectionHolder) {
        this.connectionHolder = connectionHolder;
    }

    public boolean checkLogin(String login) {
        return check(login, SQL_FIND_USER_BY_LOGIN);
    }

    public boolean checkEmail(String email) {
        return check(email, SQL_FIND_USER_BY_EMAIL);
    }

    private boolean check(String ever, String query) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connectionHolder.getConnection().prepareStatement(query);
            preparedStatement.setString(1, ever);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException e) {
            LOG.error("SQLException: ", e);
            throw new DBException("DBException" + e.getMessage());
        } finally {
            Util.close(resultSet);
            Util.close(preparedStatement);
        }
        return false;
    }

    public boolean create(User user) {
        PreparedStatement preparedStatement = null;
        try {
            LOG.debug("Starting inserting user");
            preparedStatement = connectionHolder.getConnection().prepareStatement(SQL_CREATE_USER);
            fillPreparedStatement(user, preparedStatement);
            if (preparedStatement.executeUpdate() > 0) {
                LOG.debug("User: " + user.toString() + " was inserted");
                return true;
            }
        } catch (SQLException e) {
            LOG.error("Exception in insertingUser(user): ", e);
            throw new DBException("DBException" + e.getMessage());
        } finally {
            Util.close(preparedStatement);
        }
        return false;
    }

    private void fillPreparedStatement(User user, PreparedStatement preparedStatement) throws SQLException {
        int index = 1;
        preparedStatement.setString(index++, user.getFirstName());
        preparedStatement.setString(index++, user.getLastName());
        preparedStatement.setBoolean(index++, user.isGender());
        preparedStatement.setString(index++, user.getEmail());
        preparedStatement.setString(index++, user.getLogin());
        preparedStatement.setString(index++, user.getPassword());
        preparedStatement.setString(index, user.getStatus().toUpperCase());
    }

    public User get(String login) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            LOG.debug("Starting getting user");
            preparedStatement = connectionHolder.getConnection().prepareStatement(SQL_FIND_USER_BY_LOGIN);
            preparedStatement.setString(1, login);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                LOG.debug("Finishing getting user");
                return extractUser(resultSet);
            }
        } catch (SQLException e) {
            LOG.error("Exception in get(login): ", e);
            throw new DBException("Problem with DB", e);
        } finally {
            Util.close(resultSet);
            Util.close(preparedStatement);
        }
        return null;
    }

    private User extractUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setFirstName(rs.getString("name"));
        user.setLastName(rs.getString("surname"));
        user.setGender(rs.getBoolean("gender"));
        user.setEmail(rs.getString("email"));
        user.setLogin(rs.getString("login"));
        user.setPassword(rs.getString("password"));
        user.setStatus(rs.getString("status"));
        return user;
    }
}
