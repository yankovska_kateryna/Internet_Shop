package ua.epam.yankovska.storage;

import ua.epam.yankovska.entity.Product;

import java.util.List;
import java.util.Map;

public interface BasketStorage {
    void add(Product product);

    void delete(Product product);

    void deleteAll();

    void set(Product product, int count);

    Map<Product, Integer> getAll();

    List<Product> getProducts();

    int countProducts();

    double sumOfThePurchase();
}
