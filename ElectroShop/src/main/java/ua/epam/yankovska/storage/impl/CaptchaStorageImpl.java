package ua.epam.yankovska.storage.impl;

import org.apache.log4j.Logger;
import ua.epam.yankovska.captcha.Captcha;
import ua.epam.yankovska.storage.CaptchaStorage;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class CaptchaStorageImpl implements CaptchaStorage {

    private static final Logger LOG = Logger.getLogger(CaptchaStorageImpl.class.getName());
    private Map<Integer, Captcha> captchaStorage = new ConcurrentHashMap<>();
    private AtomicInteger nextId = new AtomicInteger(0);

    @Override
    public int add(Captcha captcha) {
        int id = nextId.addAndGet(1);
        captchaStorage.put(id, captcha);
        LOG.trace("Captcha was added with id = " + id);
        return id;
    }

    @Override
    public Captcha getCaptcha(int id) {
        return captchaStorage.get(id);
    }

    @Override
    public Map<Integer, Captcha> getCaptchaStorage() {
        LOG.trace("Copied storage: " + captchaStorage.values());
        return new HashMap<>(captchaStorage);
    }

    @Override
    public void remove(int id) {
        captchaStorage.remove(id);
    }
}
