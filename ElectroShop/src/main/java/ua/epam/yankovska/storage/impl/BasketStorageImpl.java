package ua.epam.yankovska.storage.impl;

import ua.epam.yankovska.entity.Product;
import ua.epam.yankovska.storage.BasketStorage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Class that stores items in the basket.
 *
 * @author Kateryna_Yankovska
 */

public class BasketStorageImpl implements BasketStorage {

    private Map<Product, Integer> basket;

    public BasketStorageImpl() {
        basket = new HashMap<>();
    }

    @Override
    public void add(Product product) {
        if (Objects.isNull(basket.get(product))) {
            basket.put(product, 1);
            return;
        }
        basket.computeIfPresent(product, (key, value) -> value + 1);
    }

    @Override
    public void delete(Product product) {
        basket.remove(product);
    }

    @Override
    public void deleteAll() {
        basket.clear();
    }

    @Override
    public void set(Product product, int count) {
        basket.put(product, count);
    }

    @Override
    public Map<Product, Integer> getAll() {
        return new HashMap<>(basket);
    }

    @Override
    public List<Product> getProducts() {
        return new ArrayList<>(basket.keySet());
    }

    @Override
    public int countProducts() {
        int count = 0;
        Collection<Integer> values = basket.values();
        for (Integer value : values) {
            count += value;
        }
        return count;
    }

    @Override
    public double sumOfThePurchase() {
        double sum = 0;
        List<Product> products = new ArrayList<>(basket.keySet());
        for (Product product : products) {
            sum += product.getPrice() * basket.get(product);
        }
        return sum;
    }
}
