package ua.epam.yankovska.storage;

import ua.epam.yankovska.captcha.Captcha;

import java.util.Map;

public interface CaptchaStorage {

    int add(Captcha captcha);

    Captcha getCaptcha(int id);

    Map<Integer, Captcha> getCaptchaStorage();//rename

    void remove(int id);
}
