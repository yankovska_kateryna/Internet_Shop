package ua.epam.yankovska.strategy.captchaStrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Interface - strategy, that says how to work with captcha.
 */

public interface CaptchaStrategy {

    void setCaptchaId(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, int captchaId);

    int getCaptchaId(HttpServletRequest httpServletRequest);
}
