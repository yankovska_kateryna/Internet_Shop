package ua.epam.yankovska.strategy.localizationStratagy.impl;

import ua.epam.yankovska.strategy.localizationStratagy.LocalizationStoreStrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

import static ua.epam.yankovska.constants.Constant.Localization.LOCALE;

/**
 * Class that works with locale which stores in the session.
 *
 * @author Kateryna_Yankovska
 */

public class SessionStoreStrategy implements LocalizationStoreStrategy {

    @Override
    public Locale get(HttpServletRequest request) {
        return (Locale) request.getSession().getAttribute(LOCALE);
    }

    @Override
    public void set(Locale locale, HttpServletRequest request, HttpServletResponse response) {
        request.getSession().setAttribute(LOCALE, locale);
    }
}
