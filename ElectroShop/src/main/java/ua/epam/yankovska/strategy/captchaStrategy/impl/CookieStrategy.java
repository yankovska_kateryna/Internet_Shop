package ua.epam.yankovska.strategy.captchaStrategy.impl;

import ua.epam.yankovska.strategy.captchaStrategy.CaptchaStrategy;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Class that works with captcha id which stores in the cookie.
 * @author Kateryna_Yankovska
 */

public class CookieStrategy implements CaptchaStrategy {

    private static final String CAPTCHA_ID = "captchaId";
    private static final String CAPTCHA_INDEX = "captchaIndex";

    @Override
    public void setCaptchaId(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, int captchaId) {
        httpServletRequest.setAttribute(CAPTCHA_INDEX, captchaId);
        String id = String.valueOf(captchaId);
        Cookie cookie = new Cookie(CAPTCHA_ID, id);
        httpServletResponse.addCookie(cookie);
    }

    @Override
    public int getCaptchaId(HttpServletRequest httpServletRequest) {
        Cookie cookies[] = httpServletRequest.getCookies();
        String cookieValue = Stream.of(cookies)
                .filter(cookie -> Objects.equals(cookie.getName(), CAPTCHA_ID))
                .findFirst()
                .map(Cookie::getValue)
                .orElse("0");
        return Integer.parseInt(cookieValue);
    }
}
