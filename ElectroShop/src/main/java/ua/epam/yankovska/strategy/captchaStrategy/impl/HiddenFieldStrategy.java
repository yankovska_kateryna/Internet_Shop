package ua.epam.yankovska.strategy.captchaStrategy.impl;

import ua.epam.yankovska.strategy.captchaStrategy.CaptchaStrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class that works with captcha id which stores in the hidden field.
 * @author Kateryna_Yankovska
 */

public class HiddenFieldStrategy implements CaptchaStrategy {

    private static final String CAPTCHA_HIDDEN = "captcha_hidden";
    private static final String CAPTCHA_INDEX = "captchaIndex";

    @Override
    public void setCaptchaId(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, int captchaId) {
        httpServletRequest.setAttribute(CAPTCHA_INDEX, captchaId);
    }

    @Override
    public int getCaptchaId(HttpServletRequest httpServletRequest) {
        return Integer.parseInt(httpServletRequest.getParameter(CAPTCHA_HIDDEN));
    }
}
