package ua.epam.yankovska.strategy.captchaStrategy.impl;

import ua.epam.yankovska.strategy.captchaStrategy.CaptchaStrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class that works with captcha id which stores in session.
 * @author Kateryna_Yankovska
 */

public class SessionStrategy implements CaptchaStrategy {

    private static final String CAPTCHA_INDEX = "captchaIndex";

    @Override
    public void setCaptchaId(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, int captchaId) {
        httpServletRequest.getSession().setAttribute(CAPTCHA_INDEX, captchaId);
    }

    @Override
    public int getCaptchaId(HttpServletRequest httpServletRequest) {
        return Integer.parseInt(httpServletRequest.getSession().getAttribute(CAPTCHA_INDEX).toString());
    }
}
