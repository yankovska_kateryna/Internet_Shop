package ua.epam.yankovska.strategy.localizationStratagy;

import ua.epam.yankovska.strategy.localizationStratagy.impl.CookieStoreStrategy;
import ua.epam.yankovska.strategy.localizationStratagy.impl.SessionStoreStrategy;

import java.util.HashMap;
import java.util.Map;

/**
 * Class that stores possible variants of keeping locale.
 *
 * @author Kateryna_Yankovska
 */

public class LocalizationStoreManager {
    private Map<String, LocalizationStoreStrategy> localizationHandlerStrategyMap;
    private static final String COOKIE = "cookie";
    private static final String SESSION = "session";

    public LocalizationStoreManager(int expiredTime) {
        localizationHandlerStrategyMap = new HashMap<>();
        localizationHandlerStrategyMap.put(COOKIE, new CookieStoreStrategy(expiredTime));
        localizationHandlerStrategyMap.put(SESSION, new SessionStoreStrategy());
    }

    public LocalizationStoreStrategy getLocalizationStoreStrategy(String strategy) {
        return localizationHandlerStrategyMap.getOrDefault(strategy, localizationHandlerStrategyMap.get(SESSION));
    }
}
