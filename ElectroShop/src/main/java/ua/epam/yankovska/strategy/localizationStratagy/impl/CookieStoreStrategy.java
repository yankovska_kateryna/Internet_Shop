package ua.epam.yankovska.strategy.localizationStratagy.impl;

import org.apache.commons.lang3.LocaleUtils;
import ua.epam.yankovska.strategy.localizationStratagy.LocalizationStoreStrategy;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Class that works with locale which stores in the cookie.
 *
 * @author Kateryna_Yankovska
 */

public class CookieStoreStrategy implements LocalizationStoreStrategy {

    private static final String LOCALE = "locale";
    private int cookieExpireTime;

    public CookieStoreStrategy(int cookieExpireTime) {
        this.cookieExpireTime = cookieExpireTime;
    }

    @Override
    public Locale get(HttpServletRequest request) {
        if(Objects.isNull(request.getCookies())) {
            return null;
        }
        Cookie cookies[] = request.getCookies();
        return Stream.of(cookies)
                .filter(cookie -> Objects.equals(cookie.getName(), LOCALE))
                .map(Cookie::getValue)
                .map(LocaleUtils::toLocale)
                .findFirst()
                .orElse(null);
    }

    @Override
    public void set(Locale locale, HttpServletRequest request, HttpServletResponse response) {
        Cookie cookie = new Cookie(LOCALE, locale.toString());
        cookie.setMaxAge(cookieExpireTime);
        response.addCookie(cookie);
    }
}
