package ua.epam.yankovska.strategy.localizationStratagy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * Interface - strategy, that says how to work with locale.
 */

public interface LocalizationStoreStrategy {

    Locale get(HttpServletRequest request);

    void set(Locale locale, HttpServletRequest request, HttpServletResponse response);
}
