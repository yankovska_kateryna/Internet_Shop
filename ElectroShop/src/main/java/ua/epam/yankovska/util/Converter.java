package ua.epam.yankovska.util;

public class Converter {

    public String convertIntArrayToString(int[] array) {
        StringBuilder sb = new StringBuilder();
        StringBuilder allValues = new StringBuilder();
        if (array.length != 0) {
            for (int value : array) {
                if (value == 0) {
                    allValues.append("");
                }
                allValues.append(value).append((", "));
            }
            sb.append(allValues.substring(0, allValues.length() - 2)).append(")");
        }
        return sb.toString();
    }

    public String convertSort(int sorter) {
        if (sorter == 0) {
            return "products.name ASC";
        } else if (sorter == 1) {
            return "products.name DESC";
        } else if (sorter == 2) {
            return "products.price ASC";
        } else if (sorter == 3) {
            return "products.price DESC";
        }
        return "";
    }
}
