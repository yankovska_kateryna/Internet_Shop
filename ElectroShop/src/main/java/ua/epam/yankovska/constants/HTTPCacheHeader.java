package ua.epam.yankovska.constants;

/**
 * Cache HTTP response headers.
 *
 * @author Kateryna_Yankovska
 */

public enum HTTPCacheHeader {

    CACHE_CONTROL("Cache-Control"),
    EXPIRES("Expires"),
    PRAGMA("Pragma");

    private final String name;

    HTTPCacheHeader(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
