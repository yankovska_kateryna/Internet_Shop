package ua.epam.yankovska.constants;

/**
 * Bean that works with fields of the registration form and gets parameters from the request.
 *
 * @author Kateryna_Yankovska
 */

public final class Constant {

    private Constant() {
        throw new IllegalStateException();
    }

    public final class JSPWays {
        public static final String REGISTRATION_JSP = "/WEB-INF/jsp/registration.jsp";
        public static final String REGISTRATION = "/registration";
        public static final String STORE_JSP = "/WEB-INF/jsp/store.jsp";
        public static final String INDEX_JSP = "/WEB-INF/jsp/index.jsp";
        public static final String BASKET_JSP = "/WEB-INF/jsp/basket.jsp";
        public static final String ORDER_JSP = "/WEB-INF/jsp/order.jsp";
        public static final String CONFIRM_JSP = "/WEB-INF/jsp/confirm.jsp";
        public static final String ADMIN_JSP = "/WEB-INF/jsp/admin.jsp";
        public static final String ORDER = "/order";
        public static final String INDEX = "/index";
        public static final String BASKET = "/basket";
        public static final String CONFIRM = "/confirm";
        public static final String ITEMS = "/items";
    }

    public final class InitAttributes {
        public static final String CAPTCHA_HANDLER_STRATEGY = "captchaHandlerStrategy";
        public static final String LOCALE_HANDLER_STRATEGY = "localeHandlerStrategy";
        public static final String STRATEGY_LOCALE = "localeStrategy";
    }

    public final class Requests {
        public static final String SQL_CREATE_USER = "INSERT INTO users VALUES(DEFAULT, ?, ?, ?, ?, ?, ?, ?)";
        public static final String SQL_FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
        public static final String SQL_FIND_USER_BY_EMAIL = "SELECT * FROM users WHERE email = ?";

        public static final String SQL_GET_ALL_CATEGORIES = "SELECT * FROM categories";
        public static final String SQL_GET_ALL_PRODUCERS = "SELECT * FROM producers";

        public static final String SQL_GET_MIN_PRICE = "SELECT MIN(price) AS price FROM products";
        public static final String SQL_GET_MAX_PRICE = "SELECT MAX(price) AS price FROM products";
        public static final String SQL_GET_BY_ID = "SELECT products.id, products.name, description, price, " +
                "categories.name, producers.name " +
                "FROM products INNER JOIN categories, producers WHERE category_id = categories.id " +
                "AND producer_id = producers.id AND products.id = ?";

        public static final String SQL_CREATE_ORDER = "INSERT INTO orders VALUES (DEFAULT, ?, ?, ?, ?, ?, ?)";
        public static final String SQL_CREATE_ORDER_PRODUCT = "INSERT INTO orders_products VALUES (?, ?, ?, ?)";
    }

    public final class SessionAttribute {
        public static final String USER = "user";
        public static final String ERRORS = "errors";
    }

    public final class ContextAttribute {
        public static final String USER_SERVICE = "userService";
        public static final String CAPTCHA_SERVICE = "captchaService";
        public static final String IMAGE_SERVICE = "imageService";
        public static final String PRODUCT_SERVICE = "productService";
        public static final String CATEGORY_SERVICE = "categoryService";
        public static final String PRODUCER_SERVICE = "producerService";
        public static final String ORDER_SERVICE = "orderService";
    }

    public class Localization {
        public static final String LOCALE = "locale";
    }
}

