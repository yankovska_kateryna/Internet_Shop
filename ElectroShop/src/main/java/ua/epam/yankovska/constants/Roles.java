package ua.epam.yankovska.constants;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum Roles {

    ADMIN,
    USER;

    @JsonCreator
    public static Roles fromString(String string) {
        return Roles.valueOf(string.toUpperCase());
    }
}

