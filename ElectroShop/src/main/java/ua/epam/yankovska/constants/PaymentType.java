package ua.epam.yankovska.constants;

public enum PaymentType {
    card, cash
}
