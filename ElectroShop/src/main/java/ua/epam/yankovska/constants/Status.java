package ua.epam.yankovska.constants;

public enum Status {
    ACCEPTED, CANCELED, FINISHED, SENT
}
