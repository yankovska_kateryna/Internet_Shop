package ua.epam.yankovska.builder;

import java.util.Objects;

public class SQLBuilder {
    private static final String SELECT = "SELECT ";
    private static final String FROM = " FROM ";
    private static final String INNER_JOIN = " INNER JOIN ";
    private static final String ON = " ON ";
    private static final String WHERE = " WHERE ";
    private static final String BETWEEN = " BETWEEN ";
    private static final String ORDER_BY = " ORDER BY ";
    private static final String AND = " AND ";
    private static final String COUNT = "COUNT";
    private static final String LIMIT = " LIMIT ";
    private static final String OFFSET = " OFFSET ";
    private static final String IN = " IN ";
    private StringBuilder sb;

    public SQLBuilder() {
        sb = new StringBuilder();
    }

    public SQLBuilder select(String... columns) {
        StringBuilder allValues = new StringBuilder();
        sb.append(SELECT);
        if (columns.length != 0) {
            for (String column : columns) {
                allValues.append(column).append(", ");
            }
            sb.append(allValues.substring(0, allValues.length() - 2));
        }
        return this;
}

    public SQLBuilder from(String table) {
        sb.append(FROM).append(table);
        return this;
    }

    public SQLBuilder join(String table, String firstParam, String secondParam) {
        sb.append(INNER_JOIN).append(table)
                .append(ON).append(firstParam).append(" = ").append(secondParam);
        return this;
    }

    public SQLBuilder where() {
        sb.append(WHERE);
        return this;
    }

    public SQLBuilder orderBy(String sortType) {
        if (Objects.nonNull(sortType)) {
            sb.append(ORDER_BY).append(sortType);
        }
        return this;
    }

    public SQLBuilder and() {
        sb.append(AND);
        return this;
    }

    public SQLBuilder in(String column, String values) {
        if (!values.equals("")) {
            this.and();
            sb.append(column).append(IN).append("(").append(values);
        }
        return this;
    }

    public SQLBuilder count() {
        sb.append(COUNT).append("(*)").append(" AS COUNT");
        return this;
    }

    public SQLBuilder between(String column) {
        sb.append(column).append(BETWEEN).append("?").append(AND).append("?");
        return this;
    }

    public SQLBuilder limit() {
        sb.append(LIMIT).append("?");
        return this;
    }

    public SQLBuilder offset() {
        sb.append(OFFSET).append("?");
        return this;
    }

    public SQLBuilder constraint(String column, String value) {
        if (!value.equals("")) {
            this.and();
            sb.append(column).append(" = ").append("?");
        }
        return this;
    }

    public String build() {
        return this.toString();
    }

    @Override
    public String toString() {
        return sb.toString();
    }
}
