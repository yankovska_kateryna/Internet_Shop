package ua.epam.yankovska.exception;

/**
 * Exception that makes itself felt when the login already exists.
 *
 * @author Kateryna_Yankovska
 */

public class DBException extends AppRuntimeException {

    public DBException(String message) {
        super(message);
    }

    public DBException(String message, Throwable cause) {
        super(message, cause);
    }

    public DBException(Throwable cause) {
        super(cause);
    }
}
