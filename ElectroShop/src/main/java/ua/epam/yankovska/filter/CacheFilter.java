package ua.epam.yankovska.filter;

import org.apache.log4j.Logger;
import ua.epam.yankovska.constants.HTTPCacheHeader;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter that turns off cache.
 *
 * @author Kateryna_Yankovska
 */

public class CacheFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(CacheFilter.class.getName());

    @Override
    public void init(FilterConfig filterConfig) {
        LOG.debug("Initialize cache filter");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse resp = (HttpServletResponse) response;
        resp.setHeader(HTTPCacheHeader.CACHE_CONTROL.getName(), "no-cache, no-store, must-revalidate");
        resp.setHeader(HTTPCacheHeader.PRAGMA.getName(), "no-cache");
        resp.setDateHeader(HTTPCacheHeader.EXPIRES.getName(), 0L);
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        LOG.debug("Destroy cache filter");
    }
}
