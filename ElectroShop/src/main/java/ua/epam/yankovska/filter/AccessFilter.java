package ua.epam.yankovska.filter;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.log4j.Logger;
import ua.epam.yankovska.bean.impl.SecurityAccess;
import ua.epam.yankovska.bean.impl.SecurityBean;
import ua.epam.yankovska.constants.Roles;
import ua.epam.yankovska.entity.User;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static ua.epam.yankovska.constants.Constant.JSPWays.INDEX;

public class AccessFilter implements javax.servlet.Filter {

    private static final Logger LOG = Logger.getLogger(AccessFilter.class.getName());
    private List<SecurityAccess> constraints;
    private final static String PATH = "/WEB-INF/security.xml";

    @Override
    public void init(FilterConfig filterConfig) {
        LOG.debug("Initialize access filter");

        File file = new File(filterConfig.getServletContext().getRealPath(PATH));
        XmlMapper xmlMapper = new XmlMapper();
        SecurityBean securityBean = null;
        try {
            securityBean = xmlMapper.readValue(new FileInputStream(file), SecurityBean.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        constraints = securityBean.getConstraints();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        if (!isPageInSecurity(req)) {
            chain.doFilter(request, response);
            return;
        }

        if (!isUserInSystem(req)) {
            resp.sendRedirect(req.getContextPath() + INDEX);
            return;
        }

        if (!isRoleGiveAccess(req)) {
            req.getSession().setAttribute("errorMessage", "Access denied");
            resp.sendRedirect(req.getContextPath() + INDEX);
            return;
        }
        chain.doFilter(request, response);
    }

    private boolean isPageInSecurity(HttpServletRequest request) {
        return Objects.nonNull(getAppropriateConstraint(request));
    }

    private boolean isUserInSystem(HttpServletRequest request) {
        return Objects.nonNull(request.getSession().getAttribute("user"));
    }

    private boolean isRoleGiveAccess(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");
        SecurityAccess constraint = getAppropriateConstraint(request);
        for (Roles role : constraint.getRole()) {
            if (user.getStatus().equals(role.toString())) {
                return true;
            }
        }
        return false;
    }

    private SecurityAccess getAppropriateConstraint(HttpServletRequest req) {
        for (SecurityAccess constraint : constraints) {
            if (req.getRequestURI().contains(constraint.getUrlPattern())) {
                return constraint;
            }
        }
        return null;
    }

    @Override
    public void destroy() {
        LOG.debug("Destroy access filter");
    }
}
