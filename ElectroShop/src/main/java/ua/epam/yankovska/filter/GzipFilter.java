package ua.epam.yankovska.filter;

import org.apache.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Objects;
import java.util.zip.GZIPOutputStream;

/**
 * GZipFilter compresses files and makes the data sent to the browser smaller.
 *
 * @author Kateryna_Yankovska
 */

public class GzipFilter implements Filter {

    private static final Logger LOG = Logger.getLogger(GzipFilter.class.getName());
    private static final String CONTENT_ENCODING = "Content-Encoding";
    private static final String ACCEPT_ENCODING = "accept-encoding";
    private static final String GZIP = "gzip";
    private static final String ACCEPT = "Accept";
    private static final String TEXT = "text/";

    @Override
    public void init(FilterConfig filterConfig) {
        LOG.debug("Initialize GzipFilter");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        if (isExpectedGzipEncoding(req)) {
            GZIPServletResponseWrapper wrappedResponse = new GZIPServletResponseWrapper(resp);
            resp.addHeader(CONTENT_ENCODING, GZIP);
            chain.doFilter(req, wrappedResponse);
            wrappedResponse.closeResponse();
            return;
        }
        chain.doFilter(request, response);
    }

    private boolean isExpectedGzipEncoding(HttpServletRequest req) {
        boolean isExpectedText = Objects.nonNull(req.getHeader(ACCEPT)) && req.getHeader(ACCEPT).contains(TEXT);
        boolean isCanBeUnzip = Objects.nonNull(req.getHeader(ACCEPT_ENCODING)) && req.getHeader(ACCEPT_ENCODING).contains(GZIP);
        return (isExpectedText && isCanBeUnzip);
    }

    @Override
    public void destroy() {
        LOG.debug("Destroy GzipFilter");
    }

    private class GZIPServletResponseWrapper extends HttpServletResponseWrapper {
        private static final String CHARSET_NAME = "UTF-8";
        private HttpServletResponse response;
        private ServletOutputStream stream;
        private PrintWriter printWriter;

        private GZIPServletResponseWrapper(HttpServletResponse response) {
            super(response);
            this.response = response;
        }

        private void closeResponse() throws IOException {
            if (printWriter != null) {
                printWriter.close();
            }
            if (stream != null) {
                stream.close();
            }
        }

        @Override
        public ServletOutputStream getOutputStream() throws IOException {
            if (printWriter != null) {
                throw new IllegalStateException("getWriter() obtained already!");
            }
            if (stream == null)
                stream = new GZIPResponseStream(response);
            return stream;
        }

        @Override
        public PrintWriter getWriter() throws IOException {
            if (Objects.nonNull(printWriter)) {
                return printWriter;
            }
            if (Objects.nonNull(stream)) {
                throw new IllegalStateException("getOutputStream() obtained already!");
            }
            stream = new GZIPResponseStream(response);
            printWriter = new PrintWriter(new OutputStreamWriter(stream, CHARSET_NAME));
            return printWriter;
        }
    }

    private class GZIPResponseStream extends ServletOutputStream {
        private GZIPOutputStream gzipOutputStream;

        private GZIPResponseStream(HttpServletResponse response) throws IOException {
            super();
            gzipOutputStream = new GZIPOutputStream(response.getOutputStream());
        }

        @Override
        public void close() throws IOException {
            gzipOutputStream.close();
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setWriteListener(WriteListener writeListener) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void flush() throws IOException {
            gzipOutputStream.flush();
        }

        @Override
        public void write(int b) throws IOException {
            gzipOutputStream.write(b);
        }

        @Override
        public void write(byte b[]) throws IOException {
            write(b, 0, b.length);
        }

        @Override
        public void write(byte b[], int off, int len) throws IOException {
            gzipOutputStream.write(b, off, len);
        }
    }
}
