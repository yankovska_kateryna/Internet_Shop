package ua.epam.yankovska.filter;

import org.apache.log4j.Logger;
import ua.epam.yankovska.strategy.localizationStratagy.LocalizationStoreManager;
import ua.epam.yankovska.strategy.localizationStratagy.LocalizationStoreStrategy;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static ua.epam.yankovska.constants.Constant.InitAttributes.STRATEGY_LOCALE;

/**
 * Filter that sets an appropriate locale.
 *
 * @author Kateryna_Yankovska
 */

public class LocalizationFilter implements Filter {

    private static final Logger LOG = Logger.getLogger(LocalizationFilter.class.getName());
    private static final String DEFAULT_LOCALE = "default";
    private static final String SUPPORTED_LOCALE = "supported";

    private LocalizationStoreStrategy localizationStoreStrategy;
    private Locale defaultLocale;
    private List<Locale> allSupportedLocales;

    @Override
    public void init(FilterConfig filterConfig) {
        String strategyName = filterConfig.getInitParameter("storeStrategy");
        LOG.trace(strategyName);
        localizationStoreStrategy = ((LocalizationStoreManager) filterConfig.getServletContext().getAttribute(STRATEGY_LOCALE)).getLocalizationStoreStrategy(strategyName);
        allSupportedLocales = getSupportedLocale(filterConfig);
        defaultLocale = getDefaultLocale(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LOG.debug("Start filter");
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        Locale locale = changeLocale(req);
        if (Objects.isNull(locale)) {
            locale = Optional.ofNullable(localizationStoreStrategy.get(req))
                    .orElseGet(() -> getBrowserLocale(req));
        }
        localizationStoreStrategy.set(locale, req, resp);
        LocaleRequestWrapper wrapper = new LocaleRequestWrapper(req, locale);
        chain.doFilter(wrapper, response);
    }

    @Override
    public void destroy() {
        LOG.debug("Destroy filter");
    }

    private Locale changeLocale(HttpServletRequest request) {
        String locale = request.getParameter("lang");
        Locale result = null;
        if (Objects.nonNull(locale)) {
            result = allSupportedLocales.stream()
                    .filter(l -> l.getLanguage().equals(locale))
                    .findFirst()
                    .orElse(null);
        }
        return result;
    }

    private Locale getBrowserLocale(HttpServletRequest request) {
        Enumeration<Locale> locales = request.getLocales();
        while (locales.hasMoreElements()) {
            Locale currentLocale = locales.nextElement();
            if (allSupportedLocales.contains(currentLocale)) {
                return currentLocale;
            }
        }
        return defaultLocale;
    }

    private List<Locale> getSupportedLocale(FilterConfig filterConfig) {
        String supportedLocale = getInitParam(filterConfig, SUPPORTED_LOCALE);
        return Arrays.stream(supportedLocale.split(","))
                .map(Locale::new)
                .collect(Collectors.toList());
    }

    private Locale getDefaultLocale(FilterConfig filterConfig) {
        String defaultLocale = getInitParam(filterConfig, DEFAULT_LOCALE);
        return new Locale(defaultLocale);
    }

    private String getInitParam(FilterConfig filterConfig, String key) {
        return filterConfig.getInitParameter(key);
    }
}
