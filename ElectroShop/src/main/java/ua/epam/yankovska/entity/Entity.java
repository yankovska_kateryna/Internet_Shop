package ua.epam.yankovska.entity;

import java.io.Serializable;

/**
 * Class entity with property <b>id</b>
 * @author Kateryna_Yankovska
 */

public abstract class Entity implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;
    private int id;

    Entity(){}

    Entity(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
