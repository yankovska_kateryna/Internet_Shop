package ua.epam.yankovska.entity;

import ua.epam.yankovska.constants.Status;

import java.time.LocalDateTime;
import java.util.List;

public class Order extends Entity{
    private Status status;
    private String details;
    private LocalDateTime date;
    private User user;
    private String address;
    private String payment;
    private List<OrderedItem> orderedItem;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (status != order.status) return false;
        if (!details.equals(order.details)) return false;
        if (!date.equals(order.date)) return false;
        if (!user.equals(order.user)) return false;
        if (!address.equals(order.address)) return false;
        if (!payment.equals(order.payment)) return false;
        return orderedItem.equals(order.orderedItem);
    }

    @Override
    public int hashCode() {
        int result = status.hashCode();
        result = 31 * result + details.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + user.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + payment.hashCode();
        result = 31 * result + orderedItem.hashCode();
        return result;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public List<OrderedItem> getOrderedItem() {
        return orderedItem;
    }

    public void setOrderedItem(List<OrderedItem> orderedItem) {
        this.orderedItem = orderedItem;
    }
}
