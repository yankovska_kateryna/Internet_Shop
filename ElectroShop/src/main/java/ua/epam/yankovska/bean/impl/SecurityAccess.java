package ua.epam.yankovska.bean.impl;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import ua.epam.yankovska.constants.Roles;

import java.util.List;

public class SecurityAccess {
    @JacksonXmlProperty(localName = "url-pattern")
    private String urlPattern;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "role")
    private List<Roles> role;

    public String getUrlPattern() {
        return urlPattern;
    }

    public void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }

    public List<Roles> getRole() {
        return role;
    }

    public void setRole(List<Roles> role) {
        this.role = role;
    }
}
