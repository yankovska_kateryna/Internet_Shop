package ua.epam.yankovska.bean.impl;

import ua.epam.yankovska.bean.FormBean;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * Bean that works with fields of the registration form and gets parameters from the request.
 *
 * @author Kateryna_Yankovska
 */

public class RegistrationFormBean implements FormBean {

    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String EMAIL = "email";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String PASSWORD_CONFIRM = "password_confirm";
    private static final String GENDER = "gender";
    private String firstName;
    private String lastName;
    private String email;
    private String login;
    private String password;
    private String confirmPassword;
    private String gender;

    public RegistrationFormBean(HttpServletRequest request) {
        setFirstName(request.getParameter(FIRST_NAME).trim());
        setLastName(request.getParameter(LAST_NAME).trim());
        setEmail(request.getParameter(EMAIL).trim());
        setLogin(request.getParameter(LOGIN).trim());
        setPassword(request.getParameter(PASSWORD).trim());
        setConfirmPassword(request.getParameter(PASSWORD_CONFIRM).trim());
        if (Objects.nonNull(request.getParameter(GENDER))) {
            setGender(request.getParameter(GENDER).trim());
        } else {
            setGender("");
        }
    }

    public String getFirstName() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    private void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    private void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    private void setConfirmPassword(String passwordConfirm) {
        this.confirmPassword = passwordConfirm;
    }

    public String getGender() {
        return gender;
    }

    private void setGender(String gender) {
        this.gender = gender;
    }
}
