package ua.epam.yankovska.bean.impl;

import org.apache.commons.lang3.math.NumberUtils;
import ua.epam.yankovska.bean.FormBean;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * Bean that works with fields of the shop filter form and gets parameters from the request.
 *
 * @author Kateryna_Yankovska
 */

public class ItemsFilterBean implements FormBean {
    private static final String ITEM_NAME = "item_name";
    private static final String CATEGORY = "category";
    private static final String PRICE_MIN = "price-min";
    private static final String PRICE_MAX = "price-max";
    private static final String PRODUCER = "producer";
    private static final String SORTER = "sorter";
    private static final String PRODUCTS_PER_PAGE = "productsPerPage";
    private static final String PAGE = "page";
    private static final String AND = "&";
    private String name;
    private int[] category;
    private double priceMin;
    private double priceMinDefault;
    private double priceMaxDefault;
    private double priceMax;
    private int[] producer;
    private int sorter;
    private int productsPerPage;
    private int page;

    public ItemsFilterBean(HttpServletRequest request, double priceMin, double priceMax) {
        this.priceMinDefault = priceMin;
        this.priceMaxDefault = priceMax;
        this.name = validateName(request.getParameter(ITEM_NAME));
        this.category = convertCategory(request.getParameterValues(CATEGORY));
        this.priceMin = validatePriceMin(request.getParameter(PRICE_MIN));
        this.priceMax = validatePriceMax(request.getParameter(PRICE_MAX));
        this.producer = convertProducer(request.getParameterValues(PRODUCER));
        this.sorter = validateSorter(request.getParameter(SORTER));
        this.productsPerPage = validateProductsPerPage(request.getParameter(PRODUCTS_PER_PAGE));
        this.page = validatePage(request.getParameter(PAGE));
    }

    private String validateName(String name) {
        if (Objects.isNull(name)) {
            return "";
        }
        return name;
    }

    private int[] convertCategory(String[] category) {
        if (Objects.isNull(category)) {
            return new int[0];
        }
        int[] allCategories = new int[category.length];
        for (int i = 0; i < category.length; i++) {
            if (NumberUtils.isDigits(category[i])) {
                allCategories[i] = Integer.parseInt(category[i]);
            } else {
                allCategories[i] = 0;
            }
        }
        return allCategories;
    }

    private double validatePriceMin(String priceMin) {
        if (NumberUtils.isParsable(priceMin)) {
            return Double.parseDouble(priceMin);
        }
        return priceMinDefault;
    }

    private double validatePriceMax(String priceMax) {
        if (NumberUtils.isParsable(priceMax)) {
            return Double.parseDouble(priceMax);
        }
        return priceMaxDefault;
    }

    private int[] convertProducer(String[] producer) {
        if (Objects.isNull(producer)) {
            return new int[0];
        }

        int[] allProducers = new int[producer.length];
        for (int i = 0; i < producer.length; i++) {
            if (NumberUtils.isDigits(producer[i])) {
                allProducers[i] = Integer.parseInt(producer[i]);
            } else {
                allProducers[i] = 0;
            }
        }
        return allProducers;
    }

    private int validateProductsPerPage(String productsPerPage) {
        if (NumberUtils.isDigits(productsPerPage)) {
            int currentProductsPerPage = Integer.parseInt(productsPerPage);
            if (currentProductsPerPage > 20) {
                return 6;
            }
            return currentProductsPerPage;
        }
        return 6;
    }

    private int validatePage(String page) {
        if (NumberUtils.isDigits(page)) {
            int currentPage = Integer.parseInt(page);
            if (currentPage < 1){
                return 1;
            }
            return currentPage;
        }
        return 1;
    }

    private int validateSorter(String sorter) {
        if (NumberUtils.isDigits(sorter)) {
            return Integer.parseInt(sorter);
        }
        return 0;
    }

    public String getName() {
        return name;
    }

    public int[] getCategory() {
        return category;
    }

    public double getPriceMin() {
        return priceMin;
    }

    public double getPriceMax() {
        return priceMax;
    }

    public int[] getProducer() {
        return producer;
    }

    public int getSorter() {
        return sorter;
    }

    public int getProductsPerPage() {
        return productsPerPage;
    }

    public int getPage() {
        return page;
    }

    public String generateURL() {
        StringBuilder sb = new StringBuilder();
        sb.append(ITEM_NAME + "=").append(getName()).append(AND);
        for (int aCategory : getCategory()) {
            sb.append(CATEGORY + "=").append(aCategory).append(AND);
        }
        sb.append(PRICE_MIN + "=").append(getPriceMin()).append(AND);
        sb.append(PRICE_MAX + "=").append(getPriceMax()).append(AND);
        for (int aProducer : getProducer()) {
            sb.append(PRODUCER + "=").append(aProducer).append(AND);
        }
        sb.append(SORTER + "=").append(getSorter()).append(AND);
        sb.append(PRODUCTS_PER_PAGE + "=").append(getProductsPerPage()).append(AND);

        return sb.toString();
    }
}
