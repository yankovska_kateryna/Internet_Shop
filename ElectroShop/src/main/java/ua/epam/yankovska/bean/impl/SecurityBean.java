package ua.epam.yankovska.bean.impl;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "security")
public class SecurityBean {

    @JacksonXmlProperty(localName = "constraint")
    @JacksonXmlElementWrapper(useWrapping = false)

    private List<SecurityAccess> constraints;

    public List<SecurityAccess> getConstraints() {
        return constraints;
    }
}
