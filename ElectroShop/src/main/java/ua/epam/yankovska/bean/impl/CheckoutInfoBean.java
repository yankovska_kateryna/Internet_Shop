package ua.epam.yankovska.bean.impl;

import javax.servlet.http.HttpServletRequest;

/**
 * Bean that works with fields of the checkout form and gets parameters from the request.
 *
 * @author Kateryna_Yankovska
 */

public class CheckoutInfoBean {

    private String address;
    private String city;
    private String country;
    private String payment;

    public CheckoutInfoBean(HttpServletRequest request) {
        address = request.getParameter("address").trim();
        city = request.getParameter("city").trim();
        country = request.getParameter("country").trim();
        payment = request.getParameter("payment");
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getPayment() {
        return payment;
    }

    public String combineAddress() {
        return getCountry() + " " + getCity() + " " + getAddress();
    }
}
