package ua.epam.yankovska.bean.impl;

import org.apache.commons.lang3.math.NumberUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Bean that works with fields of the ajax request.
 *
 * @author Kateryna_Yankovska
 */

public class AJAXBean {

    private int id;
    private int count;
    private double price;

    public AJAXBean(HttpServletRequest request) {
        String id = request.getParameter("id_item");
        String count = request.getParameter("count");
        String price = request.getParameter("price");
        setId(NumberUtils.isParsable(id) ? Integer.parseInt(id) : 0);
        setCount(NumberUtils.isDigits(count) ? Integer.parseInt(count) : 0);
        setPrice(NumberUtils.isParsable(price) ? Double.parseDouble(price) : 0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}