package ua.epam.yankovska.bean.impl;

import javax.servlet.http.HttpServletRequest;

/**
 * Bean that works with fields of the card form and gets parameters from the request.
 *
 * @author Kateryna_Yankovska
 */

public class CardBean {
    private String firstName;
    private String lastName;
    private String cardNumber;
    private String cvv;
    private String month;
    private String year;

    public CardBean(HttpServletRequest request) {
        firstName = request.getParameter("firstName").trim();
        lastName = request.getParameter("lastName").trim();
        cardNumber = request.getParameter("cardNumber").trim();
        cvv = request.getParameter("cvv").trim();
        month = request.getParameter("month").trim();
        year = request.getParameter("year").trim();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCvv() {
        return cvv;
    }

    public String getMonth() {
        return month;
    }

    public String getYear() {
        return year;
    }
}
