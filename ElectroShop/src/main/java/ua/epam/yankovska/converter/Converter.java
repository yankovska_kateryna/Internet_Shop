package ua.epam.yankovska.converter;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * Interface that converts bean to entity.
 * @param <T> - entity
 * @param <U> - bean
 * @author Kateryna_Yankovska
 */

public interface Converter<T, U> {
    T convertToUser(U bean) throws UnsupportedEncodingException, NoSuchAlgorithmException;
}
