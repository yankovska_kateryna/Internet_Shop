package ua.epam.yankovska.converter.impl;

import ua.epam.yankovska.bean.impl.RegistrationFormBean;
import ua.epam.yankovska.converter.Converter;
import ua.epam.yankovska.entity.User;
import ua.epam.yankovska.util.Password;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * Class that converts RegistrationFormBean to the entity - User.
 *
 * @author Kateryna_Yankovska
 */

public class RegistrationBeanConverter implements Converter<User, RegistrationFormBean> {

    private static final String FEMALE = "Female";

    @Override
    public User convertToUser(RegistrationFormBean registrationFormBean) {
        User user = new User();
        user.setFirstName(registrationFormBean.getFirstName());
        user.setLastName(registrationFormBean.getLastName());
        user.setLogin(registrationFormBean.getLogin());
        user.setPassword(Password.hash(registrationFormBean.getPassword()));
        user.setEmail(registrationFormBean.getEmail());
        if (registrationFormBean.getGender().equals(FEMALE)) {
            user.setGender(true);
        } else {
            user.setGender(false);
        }
        return user;
    }
}
