package ua.epam.yankovska.service;

import ua.epam.yankovska.entity.Order;

public interface OrderService {
    int add(Order order);
}
