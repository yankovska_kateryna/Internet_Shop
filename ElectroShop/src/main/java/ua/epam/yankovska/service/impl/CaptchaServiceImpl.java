package ua.epam.yankovska.service.impl;

import ua.epam.yankovska.captcha.Captcha;
import ua.epam.yankovska.service.CaptchaService;
import ua.epam.yankovska.storage.CaptchaStorage;

import java.util.Map;

/**
 * Class that creates captcha to the map of captchas.
 *
 * @author Kateryna_Yankovska
 */

public class CaptchaServiceImpl implements CaptchaService {

    private CaptchaStorage captchaStorage;

    public CaptchaServiceImpl(CaptchaStorage captchaStorage) {
        this.captchaStorage = captchaStorage;
    }

    @Override
    public int add(Captcha captcha) {
        int id = captchaStorage.add(captcha);
        return id;
    }

    @Override
    public Captcha getCaptcha(int id) {
        return captchaStorage.getCaptcha(id);
    }
}
