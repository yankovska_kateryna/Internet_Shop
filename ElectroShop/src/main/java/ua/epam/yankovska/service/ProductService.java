package ua.epam.yankovska.service;

import ua.epam.yankovska.bean.impl.ItemsFilterBean;
import ua.epam.yankovska.entity.Product;

import java.util.List;

public interface ProductService {
    double getPriceMin();
    double getPriceMax();
    Product getById(int id);
    List<Product> getAll(ItemsFilterBean bean);
    int countAll(ItemsFilterBean bean);
}
