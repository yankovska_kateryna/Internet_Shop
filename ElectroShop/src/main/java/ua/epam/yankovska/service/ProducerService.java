package ua.epam.yankovska.service;

import ua.epam.yankovska.entity.Producer;

import java.util.List;

public interface ProducerService {
    List<Producer> getAll();
}
