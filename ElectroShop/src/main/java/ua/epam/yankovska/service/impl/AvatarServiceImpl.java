package ua.epam.yankovska.service.impl;

import org.apache.log4j.Logger;
import ua.epam.yankovska.entity.User;
import ua.epam.yankovska.service.AvatarService;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * Class that works with user's avatar.
 *
 * @author Kateryna_Yankovska
 */

public class AvatarServiceImpl implements AvatarService {
    private static final Logger LOG = Logger.getLogger(AvatarServiceImpl.class.getName());
    private static final String DEFAULT_PATH = "/avatar.jpg";
    private String path;

    public AvatarServiceImpl(String path) {
        this.path = path;
    }

    @Override
    public void saveAvatar(HttpServletRequest httpServletRequest, User user) {
        try {
            Part filePart = httpServletRequest.getPart("file");
            if (filePart.getSize() == 0) {
                return;
            }
            File imageDirectory = getImageDirectory(httpServletRequest);
            if (!imageDirectory.exists()) {
                imageDirectory.mkdirs();
            }
            String extension = getFileExtension(filePart);
            filePart.write(imageDirectory + "/avatar-" + user.getLogin() + "." + extension);
        } catch (IOException | ServletException e) {
            LOG.error("Exception ", e);
        }
    }

    private File getImageDirectory(HttpServletRequest httpServletRequest) {
        return new File(new File(httpServletRequest.getServletContext().getRealPath(File.separator)).getParent() + path);
    }

    private String getFileExtension(Part part) {
        String contentDisposition = part.getHeader("content-disposition");
        String[] items = contentDisposition.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                String[] tmp = s.substring(s.indexOf("=") + 2, s.length() - 1).split("\\.");
                return tmp[1];
            }
        }
        return "jpg";
    }

    /**
     * Firstly, initializing <b>img</b> with stub, default avatar.
     * If user hasn't got avatar, stub saves and returns.
     * Otherwise, gets user's exists avatar and saves it in <b>img</b> variable and returns.
     */
    @Override
    public BufferedImage findAvatar(HttpServletRequest httpServletRequest, User user) {
        BufferedImage img = null;
        try {
            File imageDirectory = new File(new File(httpServletRequest.getServletContext().getRealPath(File.separator)).getParent() + path);
            File imgPath = getImgPath(user, imageDirectory);
            img = ImageIO.read(imgPath);
        } catch (IOException e) {
            LOG.error(e);
        }
        return img;
    }

    private File getImgPath(User user, File imageDirectory) {
        File[] files = imageDirectory.listFiles();
        File result = null;
        if (Objects.nonNull(files)) {
            for (File file : files) {
                if (file.getName().contains(user.getLogin())) {
                    result = file;
                    break;
                }
            }
        }
        if (Objects.isNull(result)) {
            result = new File(imageDirectory + DEFAULT_PATH);
        }
        return result;
    }
}
