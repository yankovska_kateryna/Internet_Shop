package ua.epam.yankovska.service;

import ua.epam.yankovska.entity.User;

public interface UserService {
    boolean create(User user);

    boolean checkLogin(String login);

    boolean checkEmail(String email);

    User get(String login);
}
