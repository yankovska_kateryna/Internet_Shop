package ua.epam.yankovska.service.impl;

import ua.epam.yankovska.dao.OrderDAO;
import ua.epam.yankovska.db.TransactionManager;
import ua.epam.yankovska.entity.Order;
import ua.epam.yankovska.service.OrderService;

/**
 * Class that works with orders.
 *
 * @author Kateryna_Yankovska
 */

public class OrderServiceImpl implements OrderService{
    private OrderDAO orderDAO;
    private TransactionManager transactionManager;

    public OrderServiceImpl(OrderDAO orderDAO, TransactionManager transactionManager) {
        this.orderDAO = orderDAO;
        this.transactionManager = transactionManager;
    }

    @Override
    public int add(Order order) {
        return transactionManager.doInTransaction(() -> orderDAO.add(order));
    }
}
