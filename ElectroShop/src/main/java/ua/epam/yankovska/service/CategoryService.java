package ua.epam.yankovska.service;

import ua.epam.yankovska.entity.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAll();
}
