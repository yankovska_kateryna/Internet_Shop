package ua.epam.yankovska.service;

import ua.epam.yankovska.captcha.Captcha;

public interface CaptchaService {
    int add(Captcha captcha);

    Captcha getCaptcha(int id);
}
