package ua.epam.yankovska.service.impl;

import ua.epam.yankovska.dao.ProducerDAO;
import ua.epam.yankovska.db.TransactionManager;
import ua.epam.yankovska.entity.Producer;
import ua.epam.yankovska.service.ProducerService;

import java.util.List;

/**
 * Class that works with product's producers.
 *
 * @author Kateryna_Yankovska
 */

public class ProducerServiceImpl implements ProducerService {
    private ProducerDAO producerDAO;
    private TransactionManager transactionManager;

    public ProducerServiceImpl(ProducerDAO producerDAO, TransactionManager transactionManager) {
        this.producerDAO = producerDAO;
        this.transactionManager = transactionManager;
    }

    @Override
    public List<Producer> getAll() {
        return transactionManager.doInTransaction(() -> producerDAO.getAll());
    }
}
