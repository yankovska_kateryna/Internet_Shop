package ua.epam.yankovska.service.impl;

import ua.epam.yankovska.dao.UserDAO;
import ua.epam.yankovska.db.TransactionManager;
import ua.epam.yankovska.entity.User;
import ua.epam.yankovska.service.UserService;

/**
 * Class that works with users.
 *
 * @author Kateryna_Yankovska
 */

public class UserServiceImpl implements UserService {

    private UserDAO userDAO;
    private TransactionManager transactionManagerImpl;

    public UserServiceImpl(UserDAO userDAO, TransactionManager transactionManagerImpl) {
        this.userDAO = userDAO;
        this.transactionManagerImpl = transactionManagerImpl;
    }

    @Override
    public boolean create(User user) {
        return transactionManagerImpl.doInTransaction(() -> userDAO.create(user));
    }

    @Override
    public boolean checkLogin(String login) {
        return transactionManagerImpl.doInTransaction(() -> userDAO.checkLogin(login));
    }

    @Override
    public boolean checkEmail(String email) {
        return transactionManagerImpl.doInTransaction(() -> userDAO.checkEmail(email));
    }

    @Override
    public User get(String login) {
        return transactionManagerImpl.doInTransaction(() -> userDAO.get(login));
    }
}
