package ua.epam.yankovska.service;

import ua.epam.yankovska.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;

public interface AvatarService {

    void saveAvatar(HttpServletRequest httpServletRequest, User user);

    BufferedImage findAvatar(HttpServletRequest httpServletRequest, User user);

}
