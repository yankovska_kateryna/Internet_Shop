package ua.epam.yankovska.service.impl;

import ua.epam.yankovska.dao.CategoryDAO;
import ua.epam.yankovska.db.TransactionManager;
import ua.epam.yankovska.entity.Category;
import ua.epam.yankovska.service.CategoryService;

import java.util.List;

/**
 * Class that works with product's categories.
 *
 * @author Kateryna_Yankovska
 */

public class CategoryServiceImpl implements CategoryService {
    private CategoryDAO categoryDAO;
    private TransactionManager transactionManager;

    public CategoryServiceImpl(CategoryDAO categoryDAO, TransactionManager transactionManager) {
        this.categoryDAO = categoryDAO;
        this.transactionManager = transactionManager;
    }

    @Override
    public List<Category> getAll() {
        return transactionManager.doInTransaction(() -> categoryDAO.getAll());
    }
}
