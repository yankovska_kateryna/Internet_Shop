package ua.epam.yankovska.service.impl;

import ua.epam.yankovska.bean.impl.ItemsFilterBean;
import ua.epam.yankovska.dao.ProductDAO;
import ua.epam.yankovska.db.TransactionManager;
import ua.epam.yankovska.entity.Product;
import ua.epam.yankovska.service.ProductService;

import java.util.List;

/**
 * Class that works with products.
 *
 * @author Kateryna_Yankovska
 */

public class ProductServiceImpl implements ProductService {
    private ProductDAO productDAO;
    private TransactionManager transactionManager;

    public ProductServiceImpl(ProductDAO productDAO, TransactionManager transactionManager) {
        this.productDAO = productDAO;
        this.transactionManager = transactionManager;
    }

    @Override
    public double getPriceMin() {
        return transactionManager.doInTransaction(() -> productDAO.getPriceMin());
    }

    @Override
    public double getPriceMax() {
        return transactionManager.doInTransaction(() -> productDAO.getPriceMax());
    }

    @Override
    public List<Product> getAll(ItemsFilterBean bean) {
        return transactionManager.doInTransaction(() -> productDAO.getAll(bean));
    }

    @Override
    public int countAll(ItemsFilterBean bean) {
        return transactionManager.doInTransaction(() -> productDAO.countAll(bean));
    }

    @Override
    public Product getById(int id) {
        return transactionManager.doInTransaction(() -> productDAO.getById(id));
    }
}
