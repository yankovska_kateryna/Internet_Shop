package ua.epam.yankovska.validator;

import ua.epam.yankovska.bean.impl.CheckoutInfoBean;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class that validates user's checkout info.
 *
 * @author Kateryna_Yankovska
 */

public class CheckoutInfoValidator implements Validator<CheckoutInfoBean>{

    private static final String ADDRESS = "^[A-Za-zА-Яа-яЁё]{2,15}$";
    private static final Pattern PATTERN_ADDRESS = Pattern.compile(ADDRESS);
    private Map<String, String> errors = new HashMap<>();

    @Override
    public Map<String, String> validate(CheckoutInfoBean bean) {
        validateAddress(bean.getAddress());
        validateCity(bean.getCity());
        validateCountry(bean.getCountry());
        return errors;
    }

    private void validateAddress(String address) {
        Matcher m = PATTERN_ADDRESS.matcher(address);

        if (!m.find()) {
            errors.put("error_address", "Address is incorrect!");
        }
    }

    private void validateCity(String city) {
        Matcher m = PATTERN_ADDRESS.matcher(city);

        if (!m.find()) {
            errors.put("error_city", "City is incorrect!");
        }
    }

    private void validateCountry(String country) {
        Matcher m = PATTERN_ADDRESS.matcher(country);

        if (!m.find()) {
            errors.put("error_country", "Country is incorrect!");
        }
    }
}
