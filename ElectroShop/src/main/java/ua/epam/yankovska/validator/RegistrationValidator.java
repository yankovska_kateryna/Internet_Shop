package ua.epam.yankovska.validator;

import ua.epam.yankovska.bean.impl.RegistrationFormBean;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class that validates users input to the registration form.
 *
 * @author Kateryna_Yankovska
 */

public class RegistrationValidator implements Validator<RegistrationFormBean> {

    private static final String FIRST_NAME = "^[A-Za-zА-Яа-яЁё]{2,15}$";
    private static final String SECOND_NAME = "^[A-Za-zА-Яа-яЁё]{2,15}$";
    private static final String EMAIL = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$";
    private static final String LOGIN = "[A-Za-z0-9]{6,12}";
    private static final String PASSWORD = ".{5,}";
    private static final String GENDER = "^[A-Za-z]{1,10}$";
    private static final Pattern PATTERN_FIRST_NAME = Pattern.compile(FIRST_NAME);
    private static final Pattern PATTERN_SECOND_NAME = Pattern.compile(SECOND_NAME);
    private static final Pattern PATTERN_EMAIL = Pattern.compile(EMAIL);
    private static final Pattern PATTERN_LOGIN = Pattern.compile(LOGIN);
    private static final Pattern PATTERN_PASSWORD = Pattern.compile(PASSWORD);
    private static final Pattern PATTERN_GENDER = Pattern.compile(GENDER);
    private static final String ERROR_FIRST_NAME = "error_first_name";
    private static final String ERROR_LAST_NAME = "error_last_name";
    private static final String ERROR_EMAIL = "error_email";
    private static final String ERROR_LOGIN = "error_login";
    private static final String ERROR_PASSWORD = "error_password";
    private static final String ERROR_PASSWORD_CONFIRM = "error_password_confirm";
    private static final String ERROR_RADIO = "error_radio";
    private static final String FIRST_NAME_IS_INCORRECT = "First name is incorrect!";
    private static final String LAST_NAME_IS_INCORRECT = "Last name is incorrect!";
    private static final String EMAIL_IS_INCORRECT = "Email is incorrect!";
    private static final String LOGIN_IS_INCORRECT = "Login is incorrect!";
    private static final String THE_TWO_PASSWORDS_DO_NOT_MATCH = "The two passwords do not match!";
    private static final String PASSWORD_IS_INCORRECT = "Password is incorrect!";
    private static final String CHOOSE_GENDER = "Choose gender!";
    private Map<String, String> errors = new HashMap<>();

    @Override
    public Map<String, String> validate(RegistrationFormBean bean) {
        validateFirstName(bean.getFirstName());
        validateLastName(bean.getLastName());
        validateEmail(bean.getEmail());
        validateLogin(bean.getLogin());
        validatePassword(bean.getPassword(), bean.getConfirmPassword());
        validateGender(bean.getGender());
        return errors;
    }

    private void validateFirstName(String firstName) {
        Matcher m = PATTERN_FIRST_NAME.matcher(firstName);

        if (!m.find()) {
            errors.put(ERROR_FIRST_NAME, FIRST_NAME_IS_INCORRECT);
        }
    }

    private void validateLastName(String lastName) {
        Matcher m = PATTERN_SECOND_NAME.matcher(lastName);

        if (!m.find()) {
            errors.put(ERROR_LAST_NAME, LAST_NAME_IS_INCORRECT);
        }
    }

    private void validateEmail(String email) {
        Matcher m = PATTERN_EMAIL.matcher(email);

        if (!m.find()) {
            errors.put(ERROR_EMAIL, EMAIL_IS_INCORRECT);
        }
    }

    private void validateLogin(String login) {
        Matcher m = PATTERN_LOGIN.matcher(login);

        if (!m.find()) {
            errors.put(ERROR_LOGIN, LOGIN_IS_INCORRECT);
        }
    }

    private void validatePassword(String password, String confirmPassword) {
        Matcher m = PATTERN_PASSWORD.matcher(password);

        if (!m.find()) {
            if (password.equals(confirmPassword)) {
                errors.put(ERROR_PASSWORD_CONFIRM, THE_TWO_PASSWORDS_DO_NOT_MATCH);
            }
            errors.put(ERROR_PASSWORD, PASSWORD_IS_INCORRECT);
        } else {
            if (!password.equals(confirmPassword)) {
                errors.put(ERROR_PASSWORD_CONFIRM, THE_TWO_PASSWORDS_DO_NOT_MATCH);
            }
        }
    }

    private void validateGender(String gender) {
        Matcher m = PATTERN_GENDER.matcher(gender);

        if (!m.find()) {
            errors.put(ERROR_RADIO, CHOOSE_GENDER);
        }
    }
}
