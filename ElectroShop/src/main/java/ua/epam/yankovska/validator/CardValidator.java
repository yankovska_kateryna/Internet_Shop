package ua.epam.yankovska.validator;

import ua.epam.yankovska.bean.impl.CardBean;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class that validates user's input of the card.
 *
 * @author Kateryna_Yankovska
 */

public class CardValidator implements Validator<CardBean> {
    private static final String CARD = "[0-9]{16}";
    private static final String CVV = "[0-9]{3}";
    private static final String YEAR = "[0-9]{2}";
    private static final String DATE = "[0-9]{1,2}";
    private static final String NAME = "^[A-Za-z]{2,15}$";
    private static final Pattern PATTERN_NAME = Pattern.compile(NAME);
    private static final Pattern PATTERN_CARD = Pattern.compile(CARD);
    private static final Pattern PATTERN_CVV = Pattern.compile(CVV);
    private static final Pattern PATTERN_YEAR = Pattern.compile(YEAR);
    private static final Pattern PATTERN_DATE = Pattern.compile(DATE);
    private Map<String, String> errors = new HashMap<>();

    @Override
    public Map<String, String> validate(CardBean bean) {
        validateFirstName(bean.getFirstName());
        validateLastName(bean.getLastName());
        validateCardNumber(bean.getCardNumber());
        validateCvv(bean.getCvv());
        validateMonth(bean.getMonth());
        validateYear(bean.getYear());
        return errors;
    }

    private void validateFirstName(String firstName) {
        Matcher m = PATTERN_NAME.matcher(firstName);

        if (!m.find()) {
            errors.put("error_name", "First name format is incorrect");
        }
    }

    private void validateLastName(String lastName) {
        Matcher m = PATTERN_NAME.matcher(lastName);

        if (!m.find()) {
            errors.put("error_surname", "Last name format is incorrect");
        }
    }

    private void validateCardNumber(String cardNumber) {
        Matcher m = PATTERN_CARD.matcher(cardNumber);

        if (!m.find()) {
            errors.put("error_card", "Card number format is incorrect");
        }
    }

    private void validateCvv(String cvv) {
        Matcher m = PATTERN_CVV.matcher(cvv);

        if (!m.find()) {
            errors.put("error_cvv", "CVV number format is incorrect");
        }
    }

    private void validateMonth(String month) {
        Matcher m = PATTERN_DATE.matcher(month);

        if (!m.find()) {
            errors.put("error_month", "Month format is incorrect");
        }
    }

    private void validateYear(String year) {
        Matcher m = PATTERN_YEAR.matcher(year);

        if (!m.find()) {
            errors.put("error_year", "Year format is incorrect");
        }
    }
}
