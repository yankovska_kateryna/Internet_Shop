package ua.epam.yankovska.validator;

import java.util.Map;

public interface Validator<T> {
    Map<String, String> validate(T bean);
}
