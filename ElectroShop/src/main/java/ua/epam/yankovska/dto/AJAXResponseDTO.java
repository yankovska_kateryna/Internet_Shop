package ua.epam.yankovska.dto;

/**
 * Class that forms AJAX response.
 *
 * @author Kateryna_Yankovska
 */

public class AJAXResponseDTO {

    private int count;
    private double price;
    private double subtotal;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(int count, double price) {
        this.subtotal = count * price;
    }
}
