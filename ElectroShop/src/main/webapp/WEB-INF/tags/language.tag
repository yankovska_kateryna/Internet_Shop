<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="bundle/lang" />
<%@ attribute name="path" required="true" rtexprvalue="true" type="java.lang.String" %>
<li><a href="${path}?lang=en">English</a></li>
<li><a href="${path}?lang=ru"><fmt:message key="russian"/></a></li>