<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="captcha_id" required="true" rtexprvalue="true" type="java.lang.Integer" %>
<%@ attribute name="captcha_error" required="true" rtexprvalue="true" type="java.lang.String" %>
<div class="form-group" id="captcha_div">
<label>Are you human?<span style="color:red">*</span></label><br>
<img src="${pageContext.request.contextPath}/captcha?id=${captcha_id}"><br><br>
<input type="hidden" name="captcha_hidden" id="captcha_hidden" value="${captcha_id}"/>
<input class="registration-input" type="text" name="captcha" id="captcha" placeholder="Input Captcha" required>
<div id="error_captcha"><font color="red"><c:out value="${captcha_error}"/>
</div>
</div>