<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="login_error" required="true" rtexprvalue="true" type="java.lang.String" %>
<div id="error_login_login">
<font color="red"><c:out value="${login_error}"/></font>
</div>