<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="user" required="true" rtexprvalue="true" type="java.lang.String" %>
<c:choose>
<c:when test = "${not empty user}">
  <li>
    <form method="post" class="form-inline" action="${pageContext.request.contextPath}/exit">
    <img class="avatar-image" src="${pageContext.request.contextPath}/avatar">
    <font color="white" size="2.5px" margin=><c:out value="${user}"/></font>
    <input class="submit-btn-logout" type="submit" class="loginBtn" id="submit" value="Logout"/>
    </form>
  <li>
</c:when>
<c:otherwise>
  <li>
    <form method="post" class="form-inline" id="loginForm" action="${pageContext.request.contextPath}/login">
    <div class="form-group" id="login_in_div">
        <input class="login-input" type="login" name="login" id="login" class="form-control" placeholder="Login" pattern="[A-Za-z0-9]{6,12}" required>
    </div>
    <div class="form-group" id="password_in_div">
        <input class="login-input" type="password" name="password" id="password" class="form-control" placeholder="Password" pattern=".{5,}" required>
    </div>
    <input class="submit-btn-login" type="submit" class="loginBtn" id="submit" value="Login"/>
    </form>
  </li>
  <li><a href="${pageContext.request.contextPath}/registration"><i class="fa fa-user-o"></i> Sign Up</a></li>
 </c:otherwise>
</c:choose>