<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<%@ taglib prefix="tagfile" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title>Electro - HTML Ecommerce Template</title>

 		<!-- Google font -->
 		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

 		<!-- Bootstrap -->
 		<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

 		<!-- Slick -->
 		<link type="text/css" rel="stylesheet" href="css/slick.css"/>
 		<link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

 		<!-- nouislider -->
 		<link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

 		<!-- Font Awesome Icon -->
 		<link rel="stylesheet" href="css/font-awesome.min.css">

 		<!-- Custom stlylesheet -->
 		<link type="text/css" rel="stylesheet" href="css/style.css"/>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

    </head>
	<body>
		<!-- HEADER -->
        		<header>
        			<!-- TOP HEADER -->
        			<div id="top-header">
        				<div class="container">
        					<ul class="header-links pull-left">
        						<li><a href="#"><i class="fa fa-phone"></i> +021-95-51-84</a></li>
        						<li><a href="#"><i class="fa fa-envelope-o"></i> email@email.com</a></li>
        						<li><a href="#"><i class="fa fa-map-marker"></i> Sumskaya st.</a></li>
        					</ul>
        					<ul class="header-links pull-right">
                            	<tagfile:loginForm user="${sessionScope.user.getLogin()}"/>
                                <tagfile:language path="${pageContext.request.contextPath}/order"/>
                                <tagfile:loginFormException login_error="${sessionScope.errorLogin}"></tagfile:loginFormException>
                                <c:remove scope="session" var="errorLogin" />
                            </ul>
        				</div>
        			</div>
        			<!-- /TOP HEADER -->

        			<!-- MAIN HEADER -->
        			<div id="header">
        				<!-- container -->
        				<div class="container">
        					<!-- row -->
        					<div class="row">
        						<!-- LOGO -->
        						<div class="col-md-3">
        							<div class="header-logo">
        								<a href="#" class="logo">
        									<img src="./img/logo.png" alt="">
        								</a>
        							</div>
        						</div>
        						<!-- /LOGO -->

        						<!-- SEARCH BAR -->
        						<div class="col-md-6">
        							<div class="header-search">
        								<form>
        									<select class="input-select">
        										<option value="0">All Categories</option>
        										<option value="1">Category 01</option>
        										<option value="1">Category 02</option>
        									</select>
        									<input class="input" placeholder="Search here">
        									<button class="search-btn">Search</button>
        								</form>
        							</div>
        						</div>
        						<!-- /SEARCH BAR -->

        						<!-- ACCOUNT -->
                                    <div class="col-md-3 clearfix">
                                         <div class="header-ctn">
                                            <!-- Cart -->
                                            	<div class="dropdown">
                                            		<a href="${pageContext.request.contextPath}/basket">
                                            			<i class="fa fa-shopping-cart"></i>
                                                        <span>Your Cart</span>
                                                           <div id='productCount' class="qty">${sessionScope.basket.countProducts()}</div>
                                                           <c:choose>
                                                              <c:when test="${sessionScope.basket.sumOfThePurchase() == null}">
                                                                 <div id='productPrice' class="price">$0</div>
                                                              </c:when>
                                                              <c:otherwise>
                                                                 <div id='productPrice' class="price">$${sessionScope.basket.sumOfThePurchase()}</div>
                                                              </c:otherwise>
                                                           </c:choose>
                                            		</a>
                                            	</div>
                                            <!-- /Cart -->

        								<!-- Menu Toogle -->
        								<div class="menu-toggle">
        									<a href="#">
        										<i class="fa fa-bars"></i>
        										<span>Menu</span>
        									</a>
        								</div>
        								<!-- /Menu Toogle -->
        							</div>
        						</div>
        						<!-- /ACCOUNT -->
        					</div>
        					<!-- row -->
        				</div>
        				<!-- container -->
        			</div>
        			<!-- /MAIN HEADER -->
        		</header>
        		<!-- /HEADER -->

		    <!-- NAVIGATION -->
        		<nav id="navigation">
        			<!-- container -->
        			<div class="container">
        				<!-- responsive-nav -->
        				<div id="responsive-nav">
        					<!-- NAV -->
        					<ul class="main-nav nav navbar-nav">
        						<li><a href="${pageContext.request.contextPath}/index">Home</a></li>
        						<li><a href="${pageContext.request.contextPath}/items">Shop</a></li>
        						<li class="active"><a href="#">Checkout</a></li>
        					</ul>
        					<!-- /NAV -->
        				</div>
        				<!-- /responsive-nav -->
        			</div>
        			<!-- /container -->
        		</nav>
        	<!-- /NAVIGATION -->

		<!-- BREADCRUMB -->
		<div id="breadcrumb" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<h3 class="breadcrumb-header">Checkout</h3>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /BREADCRUMB -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<div class="col-md-7">
					    <form action="${pageContext.request.contextPath}/order" method="POST" id="orderForm">
						<!-- Billing Details -->
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Billing address</h3>
							</div>
							<div class="form-group">
								<input class="input" type="text" name="address" placeholder="Address" pattern="^[A-Za-zА-Яа-яЁё]{2,15}$" required>
							</div>
							<div id="error_address">
                                 <font color="red"><c:out value="${sessionScope.addressErrors['error_address']}"/></font>
                            </div>
							<div class="form-group">
								<input class="input" type="text" name="city" placeholder="City" pattern="^[A-Za-zА-Яа-яЁё]{2,15}$" required>
							</div>
							<div id="error_city">
                                 <font color="red"><c:out value="${sessionScope.addressErrors['error_city']}"/></font>
                            </div>
							<div class="form-group">
								<input class="input" type="text" name="country" placeholder="Country" pattern="^[A-Za-zА-Яа-яЁё]{2,15}$" required>
							</div>
							<div id="error_country">
                                 <font color="red"><c:out value="${sessionScope.addressErrors['error_country']}"/></font>
                            </div>
						</div>
                        <c:remove scope="session" var="addressErrors" />
						<!-- /Billing Details -->
					</div>

					<!-- Order Details -->
					<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Your Order</h3>
						</div>
						<div class="order-summary">
						 	<div class="order-col">
								<div><strong>PRODUCT</strong></div>
								<div><strong>TOTAL</strong></div>
							</div>
							<div class="order-products">
							<c:forEach var="entry" items="${basket.getAll()}">
								<div class="order-col">
									<div>${entry.value}x ${entry.key.category} ${entry.key.producer} ${entry.key.name}</div>
									<div>$${entry.value * entry.key.price}</div>
								</div>
							</c:forEach>
							</div>
							<div class="order-col">
								<div><strong>TOTAL</strong></div>
								<div><strong class="order-total">$${basket.sumOfThePurchase()}</strong></div>
							</div>
						  </div>
						<div class="payment-method">
							<div class="input-radio">
								<input type="radio" name="payment" id="payment-1" value="cash">
								<label for="payment-1">
									<span></span>
									Cash
								</label>
							</div>
							<div class="input-radio">
								<input type="radio" name="payment" id="payment-2" value="card" required>
								<label for="payment-2">
									<span></span>
									Card
								</label>
								<div class="caption">
				                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label for="cardNumber">CARD NUMBER</label>
                                                    <div class="input-group">
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="cardNumber"
                                                            placeholder="Valid Card Number"
                                                            autocomplete="cc-number"
                                                            maxlength="16"
                                                            minlength="16"
                                                            pattern="[0-9]{16}"
                                                            autofocus
                                                        />
                                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <label for="cardExpiry"><span class="hidden-xs">EXPIRATION</span><span class="visible-xs-inline">EXP</span> DATE</label>
                                        <div class="row">
                                            <div class="col-xs-3 col-md-3">
                                            <label for="cardCVC">MM</label>
                                            <div class="form-group">
                                                <input
                                                    type="number"
                                                    class="form-control"
                                                    name="month"
                                                    placeholder="MM"
                                                    autocomplete="cc-exp"
                                                    max="12"
                                                    min="1"
                                                    pattern="[0-9]{2}"
                                                />
                                            </div>
                                            </div>
                                            <div class="col-xs-3 col-md-3">
                                                <div class="form-group">
                                                <label for="cardCVC">YY</label>
                                                    <input
                                                        type="text"
                                                        class="form-control"
                                                        name="year"
                                                        placeholder="YY"
                                                        autocomplete="cc-exp"
                                                        maxlength="2"
                                                        minlength="2"
                                                        pattern="[0-9]{2}"
                                                    />
                                                </div>
                                            </div>
                                            <div class="col-xs-3 col-md-3">
                                            <label for="cardCVC">CVV</label>
                                            <div class="form-group">
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    name="cvv"
                                                    placeholder="CVV"
                                                    autocomplete="cc-csc"
                                                    maxlength="3"
                                                    minlength="3"
                                                    pattern="[0-9]{3}"
                                                />
                                            </div>
                                        </div>
                                        </div>

                                        <div class="row">
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label for="fistName">First name</label>
                                                    <input type="text" class="form-control" name="firstName" pattern="^[A-Za-z]{2,15}$"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label for="lastName">Second name</label>
                                                    <input type="text" class="form-control" name="lastName" pattern="^[A-Za-z]{2,15}$"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
							</div>
						</div>
						<input type="submit" class="primary-btn order-submit" id="final_submit" name="order-confirm" value="Submit">
					</div>
					</form>
					<!-- /Order Details -->
					<div id="error" align="right">
                        <font color="red"><c:out value="${sessionScope.cardErrors['error_card']}"/></font><br>
                        <font color="red"><c:out value="${sessionScope.cardErrors['error_month']}"/></font><br>
                        <font color="red"><c:out value="${sessionScope.cardErrors['error_year']}"/></font><br>
                        <font color="red"><c:out value="${sessionScope.cardErrors['error_cvv']}"/></font><br>
                        <font color="red"><c:out value="${sessionScope.cardErrors['error_name']}"/></font><br>
                        <font color="red"><c:out value="${sessionScope.cardErrors['error_surname']}"/></font><br>
                    </div>
				</div>
				<c:remove scope="session" var="cardErrors" />
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- FOOTER -->
        		<footer id="footer">
        			<!-- top footer -->
        			<div class="section">
        				<!-- container -->
        				<div class="container">
        					<!-- row -->
        					<div class="row">
        						<div class="col-md-3 col-xs-6">
        							<div class="footer">
        								<h3 class="footer-title">About Us</h3>
        								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
        								<ul class="footer-links">
        									<li><a href="#"><i class="fa fa-map-marker"></i>Sumskaya st.</a></li>
        									<li><a href="#"><i class="fa fa-phone"></i>+021-95-51-84</a></li>
        									<li><a href="#"><i class="fa fa-envelope-o"></i>email@email.com</a></li>
        								</ul>
        							</div>
        						</div>

        						<div class="col-md-3 col-xs-6">
        							<div class="footer">
        								<h3 class="footer-title">Categories</h3>
        								<ul class="footer-links">
        									<li><a href="#">Laptops</a></li>
        									<li><a href="#">Computers</a></li>
        									<li><a href="#">Printers</a></li>
        								</ul>
        							</div>
        						</div>

        						<div class="clearfix visible-xs"></div>

        						<div class="col-md-3 col-xs-6">
        							<div class="footer">
        								<h3 class="footer-title">Information</h3>
        								<ul class="footer-links">
        									<li><a href="#">About Us</a></li>
        									<li><a href="#">Contact Us</a></li>
        									<li><a href="#">Privacy Policy</a></li>
        									<li><a href="#">Terms & Conditions</a></li>
        								</ul>
        							</div>
        						</div>

        						<div class="col-md-3 col-xs-6">
        							<div class="footer">
        								<h3 class="footer-title">Service</h3>
        								<ul class="footer-links">
        									<li><a href="#">View Cart</a></li>
        								</ul>
        							</div>
        						</div>
        					</div>
        					<!-- /row -->
        				</div>
        				<!-- /container -->
        			</div>
        			<!-- /top footer -->

        			<!-- bottom footer -->
        			<div id="bottom-footer" class="section">
        				<div class="container">
        					<!-- row -->
        					<div class="row">
        						<div class="col-md-12 text-center">
        							<ul class="footer-payments">
        								<li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
        								<li><a href="#"><i class="fa fa-credit-card"></i></a></li>
        								<li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
        								<li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
        								<li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
        								<li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
        							</ul>
        							<span class="copyright">
        								Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Made by Kateryna Yankovska
        							</span>
        						</div>
        					</div>
        						<!-- /row -->
        				</div>
        				<!-- /container -->
        			</div>
        			<!-- /bottom footer -->
        		</footer>
        <!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/slick.min.js"></script>
		<script src="js/nouislider.min.js"></script>
		<script src="js/jquery.zoom.min.js"></script>

	</body>
</html>
