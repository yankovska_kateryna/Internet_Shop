<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="tagfile" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="bundle/lang" />
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title>Electro - HTML Ecommerce Template</title>

 		<!-- Google font -->
 		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

 		<!-- Bootstrap -->
 		<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

 		<!-- Slick -->
 		<link type="text/css" rel="stylesheet" href="css/slick.css"/>
 		<link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

 		<!-- nouislider -->
 		<link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

 		<!-- Font Awesome Icon -->
 		<link rel="stylesheet" href="css/font-awesome.min.css">

 		<!-- Custom stlylesheet -->
 		<link type="text/css" rel="stylesheet" href="css/style.css?1.11"/>

    </head>
	<body>
		<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<div id="top-header">
				<div class="container">
					<ul class="header-links pull-left">
						<li><a href="#"><i class="fa fa-phone"></i> +021-95-51-84</a></li>
						<li><a href="#"><i class="fa fa-envelope-o"></i> email@email.com</a></li>
						<li><a href="#"><i class="fa fa-map-marker"></i> Sumskaya st.</a></li>
					</ul>
					<ul class="header-links pull-right">
                        <tagfile:loginForm user="${sessionScope.user.getLogin()}"/>
						<tagfile:language path="${pageContext.request.contextPath}/items"/>
						<tagfile:loginFormException login_error="${sessionScope.errorLogin}"></tagfile:loginFormException>
                        <c:remove scope="session" var="errorLogin" />
					</ul>
				</div>
			</div>
			<!-- /TOP HEADER -->

			<!-- MAIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-md-3">
							<div class="header-logo">
								<a href="#" class="logo">
									<img src="./img/logo.png" alt="">
								</a>
							</div>
						</div>
						<!-- /LOGO -->

						<!-- SEARCH BAR -->
						<div class="col-md-6">
							<div class="header-search">
								<form>
									<select class="input-select">
										<option value="0">All Categories</option>
										<option value="1">Category 01</option>
										<option value="1">Category 02</option>
									</select>
									<input class="input" placeholder="Search here">
									<button class="search-btn">Search</button>
								</form>
							</div>
						</div>
						<!-- /SEARCH BAR -->

			<!-- ACCOUNT -->
            <div class="col-md-3 clearfix">
            	<div class="header-ctn">
            		<!-- Cart -->
            			<div class="dropdown">
            			  <a href="${pageContext.request.contextPath}/basket">
            					<i class="fa fa-shopping-cart"></i>
            					<span><fmt:message key="cart"/></span>
            				    <div id='productCount' class="qty">${sessionScope.basket.countProducts()}</div>
            				    <c:choose>
            				        <c:when test="${sessionScope.basket.sumOfThePurchase() == null}">
            					        <div id='productPrice' class="price">$0</div>
            			            </c:when>
            			            <c:otherwise>
            			                <div id='productPrice' class="price">$${sessionScope.basket.sumOfThePurchase()}</div>
            			            </c:otherwise>
            			        </c:choose>
            			  </a>
            			</div>
            		<!-- /Cart -->

								<!-- Menu Toogle -->
								<div class="menu-toggle">
									<a href="#">
										<i class="fa fa-bars"></i>
										<span>Menu</span>
									</a>
								</div>
								<!-- /Menu Toogle -->
							</div>
						</div>
						<!-- /ACCOUNT -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /MAIN HEADER -->
		</header>
		<!-- /HEADER -->

		<!-- NAVIGATION -->
		<nav id="navigation">
			<!-- container -->
			<div class="container">
				<!-- responsive-nav -->
				<div id="responsive-nav">
					<!-- NAV -->
					<ul class="main-nav nav navbar-nav">
						<li><a href="${pageContext.request.contextPath}/index"><fmt:message key="home"/></a></li>
						<li class="active"><a href="#"><fmt:message key="shop"/></a></li>
					</ul>
					<!-- /NAV -->
				</div>
				<!-- /responsive-nav -->
			</div>
			<!-- /container -->
		</nav>
		<!-- /NAVIGATION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- ASIDE -->
					<div id="aside" class="col-md-3">
					    <form action="${pageContext.request.contextPath}/items" method="GET" id="itemsFilter">
					    <!-- aside Widget -->
                        <div class="aside">
                        	<h3 class="aside-title"><fmt:message key="name"/></h3>
                        		<div class="form-group" id="items_name_div">
                                    <input class="input" type="text" name="item_name" id="item_name" placeholder=<fmt:message key="itemName"/> value="">
                                </div>
                        </div>
                        <!-- /aside Widget -->

						<!-- aside Widget -->
						<div class="aside">
							<h3 class="aside-title"><fmt:message key="categories"/></h3>
							<div class="checkbox-filter">
                                <c:forEach items="${categories}" var="category">
                                    <c:set var="contains" value="false" />
                                    <c:forEach var="item" items="${paramValues['category']}">
                                      <c:if test="${item eq category.id}">
                                        <c:set var="contains" value="true" />
                                      </c:if>
                                    </c:forEach>
								    <div class="input-checkbox">
								        <c:choose>
                                            <c:when test="${contains}">
                                                <input type="checkbox" id="${category.name}" name="category" value="${category.id}" checked>
                                            </c:when>
                                            <c:otherwise>
                                                <input type="checkbox" id="${category.name}" name="category" value="${category.id}">
                                            </c:otherwise>
                                        </c:choose>
                                        <label for="${category.name}">
                                            <span></span>
                                            ${category.name}
                                        </label>
								    </div>
								</c:forEach>
							</div>
						</div>
						<!-- /aside Widget -->

						<!-- aside Widget -->
						<div class="aside">
							<h3 class="aside-title"><fmt:message key="price"/></h3>
							<div class="price-filter">
								<div class="input-number price-min">
									<input id="price-min" type="number" name="price-min" placeholder = "Min" value="${filterBean.priceMin}">
									<span class="qty-up">+</span>
									<span class="qty-down">-</span>
								</div>
								<span>-</span>
								<div class="input-number price-max">
									<input id="price-max" type="number" name="price-max" placeholder = "Max" value="${filterBean.priceMax}">
									<span class="qty-up">+</span>
									<span class="qty-down">-</span>
								</div>
							</div>
						</div>
						<!-- /aside Widget -->

						<!-- aside Widget -->
						<div class="aside">
							<h3 class="aside-title"><fmt:message key="brand"/></h3>
							<div class="checkbox-filter">
							    <c:forEach items="${producers}" var="producer">
							        <c:set var="contains" value="false" />
                                      <c:forEach var="item" items="${paramValues['producer']}">
                                        <c:if test="${item eq producer.id}">
                                            <c:set var="contains" value="true" />
                                        </c:if>
                                      </c:forEach>
							        <div class="input-checkbox">
							            <c:choose>
                                          <c:when test="${contains}">
                                             <input type="checkbox" id="${producer.name}" name="producer" value="${producer.id}" checked>
                                          </c:when>
                                          <c:otherwise>
                                             <input type="checkbox" id="${producer.name}" name="producer" value="${producer.id}">
                                          </c:otherwise>
                                        </c:choose>
									    <label for="${producer.name}">
                                            <span></span>
                                            ${producer.name}
                                        </label>
							        </div>
							    </c:forEach>
							</div>
						</div>
						<!-- /aside Widget -->
						<input type="submit" class="submit-btn" name="filter-confirm" value=<fmt:message key="find"/>>
					</div>
					<!-- /ASIDE -->

					<!-- STORE -->
					<div id="store" class="col-md-9">
						<!-- store top filter -->
						<div class="store-filter clearfix">
							<div class="store-sort">
								<label>
									<fmt:message key="sortBy"/>:
									<select class="input-select" id = "sorter" name = "sorter">
										<option ${0 == param.sorter ? 'selected' : ''} value="0"><fmt:message key="nameA_Z"/></option>
										<option ${1 == param.sorter ? 'selected' : ''} value="1"><fmt:message key="nameZ_A"/></option>
										<option ${2 == param.sorter ? 'selected' : ''} value="2"><fmt:message key="fromCheapToExpansive"/></option>
                                        <option ${3 == param.sorter ? 'selected' : ''} value="3"><fmt:message key="fromExpansiveToCheap"/></option>
									</select>
								</label>
								<label>
									<fmt:message key="show"/>:
									<select class="input-select" id = "productsPerPage" name = "productsPerPage">
										<option ${6 == param.productsPerPage ? 'selected' : ''} value="6">6</option>
										<option ${12 == param.productsPerPage ? 'selected' : ''} value="12">12</option>
									</select>
								</label>
							</div>
						</div>
						<!-- /store top filter -->
					</form>

						<!-- store products -->
						<div class="row">
						    <c:out value="${errors}"/>
							<!-- product -->
							<c:forEach items="${products}" var="product">

                <div class="col-md-4 col-xs-6">

                    <div class="product">
                      <div class="product-img">
                        <img src="img/${product.name}.png">
                      </div>
                      <div class="product-body">
                        <p class="product-category">Category</p>
                        <h3 class="product-name" name="categoryItem" value="${product.category}"><a href="#">${product.category}</a></h3>
                        <p class="product-category">Brand</p>
                        <h3 class="product-name" name="brandItem" value="${product.producer}"><a href="#">${product.producer}</a></h3>
                        <p class="product-category">Name</p>
                        <h3 class="product-name" name="nameItem" value="${product.name}"><a href="#">${product.name}</a></h3>
                        <p class="product-category">Description</p>
                        <h6 class="product-description" name="descriptionItem" value="${product.description}"><a href="#">${product.description}</a></h6>
                        <h4 class="product-price" name="priceItem" value="${product.price}">$${product.price}</h4>
                        <button class="add-to-cart-btn" button_add="${product.id}" name="addToCartBtn" value="${product.id}"><i class="fa fa-shopping-cart"></i><fmt:message key="addToCart"/></button>
                      </div>
                    </div>

                </div>
              </c:forEach>
							<!-- /product -->
              </div>

                        <c:if test="${filterBean.page <= lastPage}">
						    <!-- store bottom filter -->
						    <div class="store-filter clearfix">
							<ul class="store-pagination">
                <c:choose>
                  <c:when test="${filterBean.page > 1}">
                    <li><a  class="btn" href="${pageContext.request.contextPath}/items?${filterBean.generateURL()}page=${filterBean.page-1}"><</a></li>
                  </c:when>
                  <c:otherwise>
                    <li><a class="btn disabled" href="${pageContext.request.contextPath}/items?${filterBean.generateURL()}page=${filterBean.page-1}" hidden><</a></li>
                  </c:otherwise>
                </c:choose>

                <c:forEach var="i" begin="1" end="${lastPage}" >
                    <c:choose>
                         <c:when test="${filterBean.page == i}">
                             <li class="active"><a class="btn disabled" href="${pageContext.request.contextPath}/items?${filterBean.generateURL()}page=${i}">${i}</a></li>
                         </c:when>
                         <c:otherwise>
                             <li><a  class="btn" href="${pageContext.request.contextPath}/items?${filterBean.generateURL()}page=${i}">${i}</a></li>
                         </c:otherwise>
                    </c:choose>
                                </c:forEach>

                                <c:choose>
                                     <c:when test="${filterBean.page < lastPage}">
                                          <li><a class="btn" href="${pageContext.request.contextPath}/items?${filterBean.generateURL()}page=${filterBean.page+1}">></a></li>
                                     </c:when>
                                     <c:otherwise>
                                          <li><a class="btn disabled" href="${pageContext.request.contextPath}/items?${filterBean.generateURL()}page=${filterBean.page+1}" hidden>></a></li>
                                     </c:otherwise>
                                </c:choose>
							</ul>
						</div>
						<!-- /store bottom filter -->
						</c:if>
					</div>
					<!-- /STORE -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->
		<!-- FOOTER -->
		<footer id="footer">
			<!-- top footer -->
			<div class="section">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">About Us</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<ul class="footer-links">
									<li><a href="#"><i class="fa fa-map-marker"></i>Sumskaya st.</a></li>
									<li><a href="#"><i class="fa fa-phone"></i>+021-95-51-84</a></li>
									<li><a href="#"><i class="fa fa-envelope-o"></i>email@email.com</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Categories</h3>
								<ul class="footer-links">									
									<li><a href="#">Laptops</a></li>
									<li><a href="#">Computers</a></li>
									<li><a href="#">Printers</a></li>
								</ul>
							</div>
						</div>

						<div class="clearfix visible-xs"></div>

						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Information</h3>
								<ul class="footer-links">
									<li><a href="#">About Us</a></li>
									<li><a href="#">Contact Us</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="#">Terms & Conditions</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Service</h3>
								<ul class="footer-links">
									<li><a href="#">View Cart</a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /top footer -->

			<!-- bottom footer -->
			<div id="bottom-footer" class="section">
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-md-12 text-center">
							<ul class="footer-payments">
								<li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
								<li><a href="#"><i class="fa fa-credit-card"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
							</ul>
							<span class="copyright">
								Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Made by Kateryna Yankovska
							</span>
						</div>
					</div>
						<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /bottom footer -->
		</footer>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/slick.min.js"></script>
		<script src="js/nouislider.min.js"></script>
		<script src="js/jquery.zoom.min.js"></script>
		<script src="js/main.js"></script>
		<script src="js/ajax.js"></script>
	</body>
</html>
