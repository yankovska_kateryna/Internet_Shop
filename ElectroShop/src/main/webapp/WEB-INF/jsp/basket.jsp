<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<%@ taglib prefix="tagfile" tagdir="/WEB-INF/tags" %>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title>Electro - HTML Ecommerce Template</title>

 		<!-- Google font -->
 		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

 		<!-- Bootstrap -->
 		<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

 		<!-- Slick -->
 		<link type="text/css" rel="stylesheet" href="css/slick.css"/>
 		<link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

 		<!-- nouislider -->
 		<link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

 		<!-- Font Awesome Icon -->
 		<link rel="stylesheet" href="css/font-awesome.min.css">

 		<!-- Custom stlylesheet -->
 		<link type="text/css" rel="stylesheet" href="css/style.css?1.11"/>

        <style>
           .layer {
            margin-top: 50px;
            margin-bottom: 50px;
           }
        </style>
    </head>
	<body>
		<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<div id="top-header">
				<div class="container">
					<ul class="header-links pull-left">
						<li><a href="#"><i class="fa fa-phone"></i> +021-95-51-84</a></li>
						<li><a href="#"><i class="fa fa-envelope-o"></i> email@email.com</a></li>
						<li><a href="#"><i class="fa fa-map-marker"></i> Sumskaya st.</a></li>
					</ul>
					<ul class="header-links pull-right">
                        <tagfile:loginForm user="${sessionScope.user.getLogin()}"/>
						<tagfile:language path="${pageContext.request.contextPath}/basket"/>
						<tagfile:loginFormException login_error="${sessionScope.errorLogin}"></tagfile:loginFormException>
                        <c:remove scope="session" var="errorLogin" />
					</ul>
				</div>
			</div>
			<!-- /TOP HEADER -->

			<!-- MAIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-md-3">
							<div class="header-logo">
								<a href="#" class="logo">
									<img src="./img/logo.png" alt="">
								</a>
							</div>
						</div>
						<!-- /LOGO -->

						<!-- SEARCH BAR -->
						<div class="col-md-6">
							<div class="header-search">
								<form>
									<select class="input-select">
										<option value="0">All Categories</option>
										<option value="1">Category 01</option>
										<option value="1">Category 02</option>
									</select>
									<input class="input" placeholder="Search here">
									<button class="search-btn">Search</button>
								</form>
							</div>
						</div>
						<!-- /SEARCH BAR -->

						<!-- ACCOUNT -->
                        <div class="col-md-3 clearfix">
                            <div class="header-ctn">
                            <!-- Cart -->
                                 <div class="dropdown">
                                    <a href="${pageContext.request.contextPath}/basket">
                                    	<i class="fa fa-shopping-cart"></i>
                                        <span>Your Cart</span>
                                        <div id='productCount' class="qty">${sessionScope.basket.countProducts()}</div>
                                        <c:choose>
                                           <c:when test="${sessionScope.basket.sumOfThePurchase() == null}">
                                              <div id='productPrice' class="price">$0</div>
                                           </c:when>
                                           <c:otherwise>
                                              <div id='productPrice' class="price">$${sessionScope.basket.sumOfThePurchase()}</div>
                                           </c:otherwise>
                                        </c:choose>
                                    </a>
                                 </div>
                            <!-- /Cart -->

								<!-- Menu Toogle -->
								<div class="menu-toggle">
									<a href="#">
										<i class="fa fa-bars"></i>
										<span>Menu</span>
									</a>
								</div>
								<!-- /Menu Toogle -->
							</div>
						</div>
						<!-- /ACCOUNT -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /MAIN HEADER -->
		</header>
		<!-- /HEADER -->

		<!-- NAVIGATION -->
		<nav id="navigation">
			<!-- container -->
			<div class="container">
				<!-- responsive-nav -->
				<div id="responsive-nav">
					<!-- NAV -->
					<ul class="main-nav nav navbar-nav">
						<li><a href="${pageContext.request.contextPath}/index">Home</a></li>
						<li><a href="${pageContext.request.contextPath}/items">Shop</a></li>
						<li class="active"><a href="#">Cart</a></li>
					</ul>
					<!-- /NAV -->
				</div>
				<!-- /responsive-nav -->
			</div>
			<!-- /container -->
		</nav>
		<!-- /NAVIGATION -->

        <!-- SECTION -->
        <div class="section">
        	<!-- container -->
        		<div class="container">
        			<!-- row -->
        			<div class="row">
        			    <c:choose>
                        <c:when test = "${sessionScope.basket.countProducts() > 0}">
        				<div class="col-md-12">
        	                <table id="cart" class="table table-hover table-condensed">
            	                <thead>
        			                <tr>
                                        <th style="width:50%">Product</th>
                                        <th style="width:10%">Price</th>
                                        <th style="width:8%">Quantity</th>
                                        <th style="width:22%" class="text-center">Subtotal</th>
                                        <th style="width:10%">
                                           <button class="btn btn-danger btn-sm" btn_delete_all="-1"><i class="fa fa-trash-o"></i></button>
                                        </th>
                                        </tr>
                                    </thead>
                                    <c:forEach var="entry" items="${basket.getAll()}">
                                    <tbody>
                                        <tr>
                                            <td data-th="Product">
                                                <div class="row">
                                                    <div class="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." class="img-responsive"/></div>
                                                        <div class="col-sm-10">
                                                            <h4 class="nomargin">${entry.key.name}</h4>
                                                            <h5>Producer: ${entry.key.producer}</h5>
                                                            <h5>Category: ${entry.key.category}</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-th="Price">${entry.key.price}</td>
                                            <td data-th="Quantity">
                                                <input type="number" id="productCountInput" input_count="${entry.key.id}" class="form-control text-center" value="${entry.value}" min="1">
                                            </td>
                                            <td data-th="Subtotal" class="text-center">$${entry.value * entry.key.price}</td>
                                            <td class="actions" data-th="">
                                                <button class="btn btn-danger btn-sm" button_delete="${entry.key.id}"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                    </c:forEach>
                                    <tfoot>
                                        <tr>
                                            <td><a href="${pageContext.request.contextPath}/items" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                                            <td colspan="2" class="hidden-xs"></td>
                                            <td class="hidden-xs text-center" id="purchaseSum"><strong>Total $${basket.sumOfThePurchase()}</strong></td>

                                            <form method="GET" action="${pageContext.request.contextPath}/path">
                                                <td><input type="submit" class="btn btn-success btn-block" name="registration-confirm" value="Checkout >"</td>
                                            </form>
                                        </tr>
                                    </tfoot>
                                </table>
        		            </div>
        		         </c:when>
        		         <c:otherwise>
        		            <p class="layer"><font size="16px" color="red">Basket is empty!</font></p>
        		         </c:otherwise>
        		         </c:choose>
        		        </div>
        		    </div>
        </div>
		<!-- FOOTER -->
		<footer id="footer">
			<!-- top footer -->
			<div class="section">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">About Us</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<ul class="footer-links">
									<li><a href="#"><i class="fa fa-map-marker"></i>Sumskaya st.</a></li>
									<li><a href="#"><i class="fa fa-phone"></i>+021-95-51-84</a></li>
									<li><a href="#"><i class="fa fa-envelope-o"></i>email@email.com</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Categories</h3>
								<ul class="footer-links">
									<li><a href="#">Laptops</a></li>
									<li><a href="#">Computers</a></li>
									<li><a href="#">Printers</a></li>
								</ul>
							</div>
						</div>

						<div class="clearfix visible-xs"></div>

						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Information</h3>
								<ul class="footer-links">
									<li><a href="#">About Us</a></li>
									<li><a href="#">Contact Us</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="#">Terms & Conditions</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Service</h3>
								<ul class="footer-links">
									<li><a href="#">View Cart</a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /top footer -->

			<!-- bottom footer -->
			<div id="bottom-footer" class="section">
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-md-12 text-center">
							<ul class="footer-payments">
								<li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
								<li><a href="#"><i class="fa fa-credit-card"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
							</ul>
							<span class="copyright">
								Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Made by Kateryna Yankovska
							</span>
						</div>
					</div>
						<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /bottom footer -->
		</footer>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/slick.min.js"></script>
		<script src="js/nouislider.min.js"></script>
		<script src="js/jquery.zoom.min.js"></script>
		<script src="js/ajax.js"></script>

	</body>
</html>
