<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="tagfile" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title>Electro - HTML Ecommerce Template</title>

 		<!-- Google font -->
 		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

 		<!-- Bootstrap -->
 		<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

 		<!-- Slick -->
 		<link type="text/css" rel="stylesheet" href="css/slick.css"/>
 		<link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

 		<!-- nouislider -->
 		<link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

 		<!-- Font Awesome Icon -->
 		<link rel="stylesheet" href="css/font-awesome.min.css">

 		<!-- Custom stlylesheet -->
 		<link type="text/css" rel="stylesheet" href="css/style.css"/>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

    </head>
	<body>
		<!-- HEADER -->
        		<header>
        			<!-- TOP HEADER -->
        			<div id="top-header">
        				<div class="container">
        					<ul class="header-links pull-left">
        						<li><a href="#"><i class="fa fa-phone"></i> +021-95-51-84</a></li>
        						<li><a href="#"><i class="fa fa-envelope-o"></i> email@email.com</a></li>
        						<li><a href="#"><i class="fa fa-map-marker"></i> Sumskaya st.</a></li>
        					</ul>
        					<ul class="header-links pull-right">
                            	<tagfile:loginForm user="${sessionScope.user.getLogin()}"/>
                                <tagfile:language path="${pageContext.request.contextPath}/confirm"/>
                                <tagfile:loginFormException login_error="${sessionScope.errorLogin}"></tagfile:loginFormException>
                                <c:remove scope="session" var="errorLogin" />
                            </ul>
        				</div>
        			</div>
        			<!-- /TOP HEADER -->

        			<!-- MAIN HEADER -->
        			<div id="header">
        				<!-- container -->
        				<div class="container">
        					<!-- row -->
        					<div class="row">
        						<!-- LOGO -->
        						<div class="col-md-3">
        							<div class="header-logo">
        								<a href="#" class="logo">
        									<img src="./img/logo.png" alt="">
        								</a>
        							</div>
        						</div>
        						<!-- /LOGO -->

        						<!-- SEARCH BAR -->
        						<div class="col-md-6">
        							<div class="header-search">
        								<form>
        									<select class="input-select">
        										<option value="0">All Categories</option>
        										<option value="1">Category 01</option>
        										<option value="1">Category 02</option>
        									</select>
        									<input class="input" placeholder="Search here">
        									<button class="search-btn">Search</button>
        								</form>
        							</div>
        						</div>
        						<!-- /SEARCH BAR -->

        						<!-- ACCOUNT -->
                                    <div class="col-md-3 clearfix">
                                         <div class="header-ctn">


        								<!-- Menu Toogle -->
        								<div class="menu-toggle">
        									<a href="#">
        										<i class="fa fa-bars"></i>
        										<span>Menu</span>
        									</a>
        								</div>
        								<!-- /Menu Toogle -->
        							</div>
        						</div>
        						<!-- /ACCOUNT -->
        					</div>
        					<!-- row -->
        				</div>
        				<!-- container -->
        			</div>
        			<!-- /MAIN HEADER -->
        		</header>
        		<!-- /HEADER -->

		<!-- BREADCRUMB -->
		<div id="breadcrumb" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<h3 class="breadcrumb-header">Your order</h3>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /BREADCRUMB -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- Order Details -->
					<div class="col-md-8">
					<div class="registration-details">
						<div class="section-title text-center">
							<h3 class="title">Your Order</h3>
						</div>
						<div class="order-summary order-details">
						    <div class="order-col">
                            	<div><strong>ORDER ID</strong></div>
                            	<div>${orderId}</div>
                            </div>
						 	<div class="order-col">
								<div><strong>DATE</strong></div>
								<div>
                  <fmt:parseDate value="${ order.getDate() }" pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime" type="both" />
                  <fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${ parsedDateTime }" />
						    </div>
						  </div>
						    <div class="order-col">
                                <div><strong>STATUS</strong></div>
                                <div>${order.getStatus()}</div>
                            </div>
                            <div class="order-col">
                                <div><strong>DETAILS</strong></div>
                                <div>${order.getDetails()}</div>
                            </div>
                            <div class="order-col">
                                <div><strong>ADDRESS</strong></div>
                                <div>${order.getAddress()}</div>
                            </div>
						</div>
						<div class="col-md-8">
						<form method="POST" action="${pageContext.request.contextPath}/confirm">
					        <input type="submit" class="primary-btn order-submit" id="confirm_submit" name="order-confirm" value="Submit">
					    </form>
					    </div>
						</div>
					</div>
				</div>
		</div>
	<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- FOOTER -->
        		<footer id="footer">
        			<!-- top footer -->
        			<div class="section">
        				<!-- container -->
        				<div class="container">
        					<!-- row -->
        					<div class="row">
        						<div class="col-md-3 col-xs-6">
        							<div class="footer">
        								<h3 class="footer-title">About Us</h3>
        								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
        								<ul class="footer-links">
        									<li><a href="#"><i class="fa fa-map-marker"></i>Sumskaya st.</a></li>
        									<li><a href="#"><i class="fa fa-phone"></i>+021-95-51-84</a></li>
        									<li><a href="#"><i class="fa fa-envelope-o"></i>email@email.com</a></li>
        								</ul>
        							</div>
        						</div>

        						<div class="col-md-3 col-xs-6">
        							<div class="footer">
        								<h3 class="footer-title">Categories</h3>
        								<ul class="footer-links">
        									<li><a href="#">Laptops</a></li>
        									<li><a href="#">Computers</a></li>
        									<li><a href="#">Printers</a></li>
        								</ul>
        							</div>
        						</div>

        						<div class="clearfix visible-xs"></div>

        						<div class="col-md-3 col-xs-6">
        							<div class="footer">
        								<h3 class="footer-title">Information</h3>
        								<ul class="footer-links">
        									<li><a href="#">About Us</a></li>
        									<li><a href="#">Contact Us</a></li>
        									<li><a href="#">Privacy Policy</a></li>
        									<li><a href="#">Terms & Conditions</a></li>
        								</ul>
        							</div>
        						</div>

        						<div class="col-md-3 col-xs-6">
        							<div class="footer">
        								<h3 class="footer-title">Service</h3>
        								<ul class="footer-links">
        									<li><a href="#">View Cart</a></li>
        								</ul>
        							</div>
        						</div>
        					</div>
        					<!-- /row -->
        				</div>
        				<!-- /container -->
        			</div>
        			<!-- /top footer -->

        			<!-- bottom footer -->
        			<div id="bottom-footer" class="section">
        				<div class="container">
        					<!-- row -->
        					<div class="row">
        						<div class="col-md-12 text-center">
        							<ul class="footer-payments">
        								<li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
        								<li><a href="#"><i class="fa fa-credit-card"></i></a></li>
        								<li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
        								<li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
        								<li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
        								<li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
        							</ul>
        							<span class="copyright">
        								Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Made by Kateryna Yankovska
        							</span>
        						</div>
        					</div>
        						<!-- /row -->
        				</div>
        				<!-- /container -->
        			</div>
        			<!-- /bottom footer -->
        		</footer>
        <!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/slick.min.js"></script>
		<script src="js/nouislider.min.js"></script>
		<script src="js/jquery.zoom.min.js"></script>

	</body>
</html>
