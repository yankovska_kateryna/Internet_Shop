<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tagfile" tagdir="/WEB-INF/tags" %>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title>Electro - HTML Ecommerce Template</title>

 		<!-- Google font -->
 		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

 		<!-- Bootstrap -->
 		<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

 		<!-- Slick -->
 		<link type="text/css" rel="stylesheet" href="css/slick.css"/>
 		<link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

 		<!-- nouislider -->
 		<link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

 		<!-- Font Awesome Icon -->
 		<link rel="stylesheet" href="css/font-awesome.min.css">

 		<!-- Custom stlylesheet -->
 		<link type="text/css" rel="stylesheet" href="css/style.css?1.11"/>
		
    </head>
	<body>
		<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<div id="top-header">
				<div class="container">
					<ul class="header-links pull-left">
						<li><a href="#"><i class="fa fa-phone"></i> +021-95-51-84</a></li>
						<li><a href="#"><i class="fa fa-envelope-o"></i> email@email.com</a></li>
						<li><a href="#"><i class="fa fa-map-marker"></i> Sumskaya st.</a></li>
					</ul>
					<ul class="header-links pull-right">
						<tagfile:loginForm user="${sessionScope.user.getLogin()}"/>
                        <tagfile:language path="${pageContext.request.contextPath}/registration"/>
                        <tagfile:loginFormException login_error="${sessionScope.errorLogin}"></tagfile:loginFormException>
                        <c:remove scope="session" var="errorLogin" />
					</ul>
				</div>
			</div>
			<!-- /TOP HEADER -->

			<!-- MAIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-md-3">
							<div class="header-logo">
								<a href="#" class="logo">
									<img src="./img/logo.png" alt="">
								</a>
							</div>
						</div>
						<!-- /LOGO -->

						<!-- SEARCH BAR -->
						<div class="col-md-6">
							<div class="header-search">
								<form>
									<select class="input-select">
										<option value="0">All Categories</option>
										<option value="1">Category 01</option>
										<option value="1">Category 02</option>
									</select>
									<input class="input" placeholder="Search here">
									<button class="search-btn">Search</button>
								</form>
							</div>
						</div>
						<!-- /SEARCH BAR -->

						<!-- ACCOUNT -->
                        <div class="col-md-3 clearfix">
                           <div class="header-ctn">
                              <!-- Cart -->
                              <div class="dropdown">
                                  <a href="${pageContext.request.contextPath}/basket">
                                    	<i class="fa fa-shopping-cart"></i>
                                        <span>Your Cart</span>
                                        <div id='productCount' class="qty">${sessionScope.basket.countProducts()}</div>
                                        <c:choose>
                                           <c:when test="${sessionScope.basket.sumOfThePurchase() == null}">
                                               <div id='productPrice' class="price">$0</div>
                                           </c:when>
                                           <c:otherwise>
                                               <div id='productPrice' class="price">$${sessionScope.basket.sumOfThePurchase()}</div>
                                           </c:otherwise>
                                        </c:choose>
                                  </a>
                              </div>
                              <!-- /Cart -->

								<!-- Menu Toogle -->
								<div class="menu-toggle">
									<a href="#">
										<i class="fa fa-bars"></i>
										<span>Menu</span>
									</a>
								</div>
								<!-- /Menu Toogle -->
							</div>
						</div>
						<!-- /ACCOUNT -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /MAIN HEADER -->
		</header>
		<!-- /HEADER -->

		<!-- NAVIGATION -->
		<nav id="navigation">
			<!-- container -->
			<div class="container">
				<!-- responsive-nav -->
				<div id="responsive-nav">
					<!-- NAV -->
					<ul class="main-nav nav navbar-nav">
						<li><a href="${pageContext.request.contextPath}/index">Home</a></li>
						<li><a href="${pageContext.request.contextPath}/items">Shop</a></li>
					</ul>
					<!-- /NAV -->
				</div>
				<!-- /responsive-nav -->
			</div>
			<!-- /container -->
		</nav>
		<!-- /NAVIGATION -->
		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<!-- Registration Details -->
						<div class="registration-details">
						    <font color="green" size="4px"><c:out value="${sessionScope.success_result}"/></font>
                            <c:remove scope="session" var="success_result" />
							<div class="section-title">
								<h3 class="title">Registration</h3>
							</div>
							<form action="${pageContext.request.contextPath}/registration" method="POST" id="registrationForm" enctype="multipart/form-data">
							    <div class="form-group" id="file_div">
							        <label>Your avatar</label><br>
							        <input type="file" name="file" id="file" accept=".jpg, .jpeg, .png" required/>
							    </div>
								<div class="form-group" id="first_name_div">
									<label>First Name<span style="color:red">*</span></label><br>
									<input class="registration-input" type="text" name="first_name" id="first_name" placeholder="First Name" value="${sessionScope.bean.firstName}" required>
									<div id="error_first_name" padding = 10px>
									    <font color="red"><c:out value="${sessionScope.errors['error_first_name']}"/></font>
									</div>
								</div>
								<div class="form-group" id="last_name_div">
									<label>Last Name<span style="color:red">*</span></label><br>
									<input class="registration-input" type="text" name="last_name" id="last_name" placeholder="Last Name" value="${sessionScope.bean.lastName}" required>
									<div id="error_last_name">
									    <font color="red"><c:out value="${sessionScope.errors['error_last_name']}"/></font>
									</div>
								</div>
								<div class="form-group" id="email_div">
									<label>Email<span style="color:red">*</span></label><br>
									<input class="registration-input" type="email" name="email" id="email" placeholder="Email" value="${sessionScope.bean.email}"  required>
									<div id="error_email">
									    <font color="red"><c:out value="${sessionScope.errors['error_email']}"/></font>
                                        </div>
								</div>
                                <div class="form-group" id="login_div">
                                   	<label>Login<span style="color:red">*</span></label><br>
                                   	<input class="registration-input" type="text" name="login" id="login" placeholder="Login" value="${sessionScope.bean.login}" required>
                                   	<div id="error_login">
                                   		<font color="red"><c:out value="${sessionScope.errors['error_login']}"/></font>
                                   	</div>
                                   	<div id="errors_reg">
                                        <font color="red"><c:out value="${sessionScope.errors['errors_reg']}"/></font>
                                    </div>
                                </div>
                                <div class="form-group" id="password_div">
                                   		<label>Password<span style="color:red">*</span></label><br>
                                   		<input class="registration-input" type="password" name="password" id="password" placeholder="Password" value="" required>
                                   		<div id="error_password"> <font color="red"><c:out value="${sessionScope.errors['error_password']}"/></font>
                                        </div>
                                </div>
								<div class="form-group" id="password_confirm_div">
									<label>Confirm Password<span style="color:red">*</span></label><br>
									<input class="registration-input" type="password" name="password_confirm" id="password_confirm" placeholder="Confirm Password" value="" required>
									<div id="error_password_confirm"><font color="red"><c:out value="${sessionScope.errors['error_password_confirm']}"/></font>
                                    </div>
								</div>
								<div class="payment-method">
                                    <label>Gender<span style="color:red">*</span></label><br>
									<div class="input-radio-reg">
										<input type="radio" name="gender" id="male" value="Male" required>
										<label for="male">
											<span></span>
											Male
										</label>
									</div>
									<div class="input-radio-reg">
										<input type="radio" name="gender" id="female" value="Female" required>
										<label for="female">
                                            <span></span>
											Female
										</label>
									</div>
                                    <div id="error_radio"><font color="red"><c:out value="${sessionScope.errors['error_radio']}"/></font><br>
                                    </div>
                                    <tagfile:captcha captcha_id="${captchaIndex}" captcha_error="${sessionScope.errors['error_captcha']}"></tagfile:captcha>
								<input type="submit" class="submit-btn" name="registration-confirm" value="Register">
								<c:remove scope="session" var="errors" />
								<c:remove scope="session" var="bean" />
							</form>
						</div>
						<!-- /Registration Details -->
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- FOOTER -->
		<footer id="footer">
			<!-- top footer -->
			<div class="section">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">About Us</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
								<ul class="footer-links">
									<li><a href="#"><i class="fa fa-map-marker"></i>Sumskaya st.</a></li>
									<li><a href="#"><i class="fa fa-phone"></i>+021-95-51-84</a></li>
									<li><a href="#"><i class="fa fa-envelope-o"></i>email@email.com</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Categories</h3>
								<ul class="footer-links">
									<li><a href="#">Laptops</a></li>
									<li><a href="#">Computers</a></li>
									<li><a href="#">Printers</a></li>
								</ul>
							</div>
						</div>

						<div class="clearfix visible-xs"></div>

						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Information</h3>
								<ul class="footer-links">
									<li><a href="#">About Us</a></li>
									<li><a href="#">Contact Us</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="#">Terms & Condition</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Service</h3>
								<ul class="footer-links">
									<li><a href="#">Sign In</a></li>
									<li><a href="#">View Cart</a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /top footer -->

			<!-- bottom footer -->
			<div id="bottom-footer" class="section">
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-md-12 text-center">
							<ul class="footer-payments">
								<li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
								<li><a href="#"><i class="fa fa-credit-card"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
							</ul>
							<span class="copyright">
								Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Made by Kateryna Yankovska
							</span>
						</div>
					</div>
						<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /bottom footer -->
		</footer>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/slick.min.js"></script>
		<script src="js/nouislider.min.js"></script>
		<script src="js/jquery.zoom.min.js"></script>
		
		<!-- Validation -->
		<!--<script type="text/javascript" src="js/registrationValidation.js"></script>-->
		<!--<script type="text/javascript" src="js/jQueryRegistrationValidation.js?1.4"></script>-->
	</body>
</html>
