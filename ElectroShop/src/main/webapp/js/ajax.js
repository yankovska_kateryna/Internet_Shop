$(document).ready(function(){
   $("button[button_add]").click(function() {
      const id=$(this).attr("button_add");
      $.post("basket",
      {
         id_item: id
      },
      function(data) {
         $('#productCount').text(data.count);
         $('#productPrice').text("$" + data.price);
      }
      );
   });

   $("button[button_delete]").click(function() {
        const id=$(this).attr("button_delete");
        var item = $(this).parent().parent();
        var params = {"id_item" : id};
        $.ajax({
           url: 'basket?'+$.param(params),
           type: 'DELETE',
           success: function(data) {
              item.hide('slow');
              $('#productCount').text(data.count);
              $('#productPrice').text("$" + data.price);
              $('#purchaseSum').text("$" + data.price);
           }
        });
   });

   $("input[input_count]").change(function() {
        const id=$(this).attr("input_count");
        const amount=$(this).val();
        const productSubtotal = $(this).parent().parent().find("td[data-th='Subtotal']");
        var params = {"id_item" : id, "count" : amount};
        $.ajax({
            url: 'basket?'+$.param(params),
            type: 'PUT',
            success: function(data) {
                $('#productCount').text(data.count);
                $('#productPrice').text("$" + data.price);
                $('#purchaseSum').text(" Total $" + data.price);
                $(productSubtotal).text("$" + data.subtotal);
            }
        });
   })

   $("button[btn_delete_all]").click(function() {
        const num=$(this).attr("btn_delete_all");
        var params = {"id_item" : num};
        $.ajax({
            url: 'basket?'+$.param(params),
            type: 'DELETE',
            success: function(data) {
                $('tbody').hide('slow');
                $('#productCount').text(data.count);
                $('#productPrice').text("$" + data.price);
                $('#purchaseSum').text("Total $" + data.price);
            }
        });
   });
});