var first_name = document.forms['registrationForm']['first_name'];
var last_name = document.forms['registrationForm']['last_name'];
var email = document.forms['registrationForm']['email'];
var login = document.forms['registrationForm']['login'];
var password = document.forms['registrationForm']['password'];
var password_confirm = document.forms['registrationForm']['password_confirm'];

var error_first_name = document.getElementById('error_first_name');
var error_last_name = document.getElementById('error_last_name');
var error_email = document.getElementById('error_email');
var error_login = document.getElementById('error_login');
var error_password = document.getElementById('error_password');
var error_password_confirm = document.getElementById('error_password_confirm');

function validate() {
	return firstNameVerify() & lastNameVerify() & emailVerify() & loginVerify() & passwordVerify() & confirmPasswordVerify();
}

function firstNameVerify() {
	var input = first_name.value;
	var pattern = "^[A-Za-zА-Яа-яЁё]{2,15}$";
	var matcher = new RegExp(pattern);
	if(!matcher.test(input)){
		first_name.style.border = "1px solid red";
		document.getElementById('error_first_name').style.color = "red";
		error_first_name.textContent = "First name is incorrect!";
		return false;
	} else {
		first_name.style.border = "1px solid black";
		document.getElementById('first_name_div').style.color = "black";
		error_first_name.textContent = "";
		return true;
	}
}

function lastNameVerify() {
	var input = last_name.value;
	var pattern = "^[A-Za-zА-Яа-яЁё]{2,15}$";
	var matcher = new RegExp(pattern);
	if(!matcher.test(input)){
		last_name.style.border = "1px solid red";
		document.getElementById('error_last_name').style.color = "red";
		error_last_name.textContent = "Last name is incorrect!";
		return false;
	} else {
		last_name.style.border = "1px solid black";
		document.getElementById('last_name_div').style.color = "black";
		error_last_name.textContent = "";
		return true;
	}
}

function emailVerify() {
	var input = email.value;
	var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";
	var matcher = new RegExp(pattern);
	if(!matcher.test(input)){
		email.style.border = "1px solid red";
		document.getElementById('error_email').style.color = "red";
		error_email.textContent = "Email is incorrect!";
		return false;
	} else {
		email.style.border = "1px solid black";
		document.getElementById('email_div').style.color = "black";
		error_email.textContent = "";
		return true;
	}
}

function loginVerify() {
	var input = login.value;
	var pattern = "[A-Za-z0-9]{6,12}";
	var matcher = new RegExp(pattern);
	if(!matcher.test(input)){
		login.style.border = "1px solid red";
		document.getElementById('error_login').style.color = "red";
		error_login.textContent = "Login is incorrect!";
		return false;
	} else {
		login.style.border = "1px solid black";
		document.getElementById('login_div').style.color = "black";
		error_login.textContent = "";
		return true;
	}
}

function passwordVerify() {
	var input = password.value;
	var pattern = ".{5,}";
	var matcher = new RegExp(pattern);
	if(!matcher.test(input)){
		password.style.border = "1px solid red";
		document.getElementById('error_password').style.color = "red";
		error_password.textContent = "Password is incorrect!";
		return false;
	} else {
		password.style.border = "1px solid black";
		document.getElementById('password_div').style.color = "black";
		error_password.textContent = "";
		return true;
	}
}

function confirmPasswordVerify() {
	if (passwordVerify() == false) {
		password_confirm.style.border = "1px solid red";
		document.getElementById('error_password_confirm').style.color = "red";
		error_password_confirm.textContent = "Input correct password!";
		return false;
	} else if (password.value != password_confirm.value) {
  		password_confirm.style.border = "1px solid red";
		document.getElementById('error_password_confirm').style.color = "red";
		error_password_confirm.textContent = "The two passwords do not match!";
		return false;
	} else {
		password_confirm.style.border = "1px solid black";
		document.getElementById('password_confirm_div').style.color = "black";
		error_password_confirm.textContent = "";
		return true;
	}
}

var registrationForm = document.getElementById("registrationForm");
registrationForm.addEventListener("submit", function( event ) {
  if(!validate()){
   event.preventDefault();
  }
});


