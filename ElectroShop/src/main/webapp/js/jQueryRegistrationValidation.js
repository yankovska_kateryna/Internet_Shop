const genderFields = $("input[name='gender']");

$(document).ready(function() {
	function validate() {
		return firstNameVerify() & lastNameVerify() & emailVerify() & loginVerify() & passwordVerify() & confirmPasswordVerify() & genderVerify() & captchaVerify();
	};

	function firstNameVerify() {
		var input = $("#first_name").val();
		var pattern = "^[A-Za-zА-Яа-яЁё]{2,15}$";
		var matcher = new RegExp(pattern);
		if(!matcher.test(input)){
			$("#error_first_name").css({"color": "red"});
  			$("#first_name").css("border", "red solid 1px");
			$("#error_first_name").html("First name is incorrect!");
			return false;
		} else {
			$("#first_name").css("border", "black solid 1px");
			$("#first_name_div").css({"color": "black"});
			$("#error_first_name").html("");
			return true;
		}
	}

	function lastNameVerify() {
		var input = $("#last_name").val();
		var pattern = "^[A-Za-zА-Яа-яЁё]{2,15}$";
		var matcher = new RegExp(pattern);
		if(!matcher.test(input)){
			$("#error_last_name").css({"color": "red"});
  			$("#last_name").css("border", "red solid 1px");
			$("#error_last_name").html("Last name is incorrect!");
			return false;
		} else {
			$("#last_name").css("border", "black solid 1px");
			$("#last_name_div").css({"color": "black"});
			$("#error_last_name").html("");
			return true;
		}
	}

	function emailVerify() {
		var input = $("#email").val();
		var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";
		var matcher = new RegExp(pattern);
		if(!matcher.test(input)){
			$("#error_email").css({"color": "red"});
  			$("#email").css("border", "red solid 1px");
			$("#error_email").html("Email is incorrect!");
			return false;
		} else {
			$("#email").css("border", "black solid 1px");
			$("#email_div").css({"color": "black"});
			$("#error_email").html("");
			return true;
		}
	}

	function loginVerify() {
		var input = $("#login").val();
		var pattern = "[A-Za-z0-9]{6,12}";
		var matcher = new RegExp(pattern);
		if(!matcher.test(input)){
			$("#error_login").css({"color": "red"});
  			$("#login").css("border", "red solid 1px");
			$("#error_login").html("Login is incorrect!");
			return false;
		} else {
			$("#login").css("border", "black solid 1px");
			$("#login_div").css({"color": "black"});
			$("#error_login").html("");
			return true;
		}
	}

	function passwordVerify() {
		var input = $("#password").val();
		var pattern = ".{5,}";
		var matcher = new RegExp(pattern);
		if(!matcher.test(input)){
			$("#error_password").css({"color": "red"});
  			$("#password").css("border", "red solid 1px");
			$("#error_password").html("Password is incorrect!");
			return false;
		} else {
			$("#password").css("border", "black solid 1px");
			$("#password_div").css({"color": "black"});
			$("#error_password").html("");
			return true;
		}
	}

	function confirmPasswordVerify() {
		if (passwordVerify() == false) {
			$("#error_password_confirm").css({"color": "red"});
  			$("#password_confirm").css("border", "red solid 1px");
			$("#error_password_confirm").html("Input correct password!");
			return false;
		} else if ($("#password").val() != $("#password_confirm").val()) {
  			$("#error_password_confirm").css({"color": "red"});
  			$("#password_confirm").css("border", "red solid 1px");
			$("#error_password_confirm").html("The two passwords do not match!");
			return false;
		} else {
			$("#password_confirm").css("border", "black solid 1px");
			$("#password_confirm_div").css({"color": "black"});
			$("#error_password_confirm").html("");
			return true;
		}
	}

	 function genderVerify(){
        var isValid = false;
        genderFields.each((i,elem)=>{
        if(!isValid && $(elem).is(':checked')){
            isValid = true;
            $("#error_radio").html("");
        } else if(!isValid){
            $("#error_radio").css({"color": "red"});
            $("#error_radio").html("Choose gender!");
        }
        });
         return isValid;
     }

     function captchaVerify() {
     	var input = $("#captcha").val();
     	var pattern = "[0-9]{6}";
     	var matcher = new RegExp(pattern);
   		if(!matcher.test(input)){
   			$("#error_captcha").css({"color": "red"});
   			$("#captcha").css("border", "red solid 1px");
     		$("#error_captcha").html("Incorrect captcha!");
     		return false;
     	} else {
     		$("#captcha").css("border", "black solid 1px");
     		$("#captcha_div").css({"color": "black"});
     		$("#error_captcha").html("");
     		return true;
     	}
     }

	var registrationForm = $("#registrationForm");
	registrationForm.bind("submit", function( event ) {
  	if(!validate()){
   		event.preventDefault();
  	}
	});
});