package ua.epam.yankovska.validator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.epam.yankovska.bean.impl.RegistrationFormBean;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationValidatorTest {
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String EMAIL = "email";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String PASSWORD_CONFIRM = "password_confirm";
    private static final String GENDER = "gender";
    private static final String ERROR_FIRST_NAME = "error_first_name";
    private static final String ERROR_LAST_NAME = "error_last_name";
    private static final String ERROR_EMAIL = "error_email";
    private static final String ERROR_LOGIN = "error_login";
    private static final String ERROR_PASSWORD = "error_password";
    private static final String ERROR_PASSWORD_CONFIRM = "error_password_confirm";
    private static final String ERROR_RADIO = "error_radio";
    private static final String FIRST_NAME_IS_INCORRECT = "First name is incorrect!";
    private static final String LAST_NAME_IS_INCORRECT = "Last name is incorrect!";
    private static final String EMAIL_IS_INCORRECT = "Email is incorrect!";
    private static final String LOGIN_IS_INCORRECT = "Login is incorrect!";
    private static final String THE_TWO_PASSWORDS_DO_NOT_MATCH = "The two passwords do not match!";
    private static final String PASSWORD_IS_INCORRECT = "Password is incorrect!";
    private static final String CHOOSE_GENDER = "Choose gender!";
    private Validator validator = new RegistrationValidator();

    @Mock
    private HttpServletRequest request;

    @Test
    public void testRegistrationBeanValidatorWhenAllIsCorrect() {
        when(request.getParameter(FIRST_NAME)).thenReturn("Alex");
        when(request.getParameter(LAST_NAME)).thenReturn("Alekseev");
        when(request.getParameter(EMAIL)).thenReturn("alex@gamil.com");
        when(request.getParameter(LOGIN)).thenReturn("alexey1");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("11111");
        when(request.getParameter(GENDER)).thenReturn("man");

        RegistrationFormBean registrationFormBean = new RegistrationFormBean(request);
        Map<String, String> errors = validator.validate(registrationFormBean);

        assertEquals(0, errors.size());
    }

    @Test
    public void testRegistrationBeanValidatorWhenFirstNameIsNotCorrect() {
        when(request.getParameter(FIRST_NAME)).thenReturn("Alex123");
        when(request.getParameter(LAST_NAME)).thenReturn("Alekseev");
        when(request.getParameter(EMAIL)).thenReturn("alex@gamil.com");
        when(request.getParameter(LOGIN)).thenReturn("alexey1");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("11111");
        when(request.getParameter(GENDER)).thenReturn("man");

        RegistrationFormBean registrationFormBean = new RegistrationFormBean(request);
        Map<String, String> errors = validator.validate(registrationFormBean);

        assertEquals(FIRST_NAME_IS_INCORRECT, errors.get(ERROR_FIRST_NAME));
    }

    @Test
    public void testRegistrationBeanValidatorWhenLastNameIsNotCorrect() {
        when(request.getParameter(FIRST_NAME)).thenReturn("Alex");
        when(request.getParameter(LAST_NAME)).thenReturn("Alekseev123");
        when(request.getParameter(EMAIL)).thenReturn("alex@gamil.com");
        when(request.getParameter(LOGIN)).thenReturn("alexey1");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("11111");
        when(request.getParameter(GENDER)).thenReturn("man");

        RegistrationFormBean registrationFormBean = new RegistrationFormBean(request);
        Map<String, String> errors = validator.validate(registrationFormBean);

        assertEquals(LAST_NAME_IS_INCORRECT, errors.get(ERROR_LAST_NAME));
    }

    @Test
    public void testRegistrationBeanValidatorWhenEmailIsNotCorrect() {
        when(request.getParameter(FIRST_NAME)).thenReturn("Alex");
        when(request.getParameter(LAST_NAME)).thenReturn("Alekseev123");
        when(request.getParameter(EMAIL)).thenReturn("alexgamil.com");
        when(request.getParameter(LOGIN)).thenReturn("alexey1");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("11111");
        when(request.getParameter(GENDER)).thenReturn("man");

        RegistrationFormBean registrationFormBean = new RegistrationFormBean(request);
        Map<String, String> errors = validator.validate(registrationFormBean);

        assertEquals(EMAIL_IS_INCORRECT, errors.get(ERROR_EMAIL));
    }

    @Test
    public void testRegistrationBeanValidatorWhenLoginIsNotCorrect() {
        when(request.getParameter(FIRST_NAME)).thenReturn("Alex");
        when(request.getParameter(LAST_NAME)).thenReturn("Alekseev123");
        when(request.getParameter(EMAIL)).thenReturn("alexgamil.com");
        when(request.getParameter(LOGIN)).thenReturn("alex");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("11111");
        when(request.getParameter(GENDER)).thenReturn("man");

        RegistrationFormBean registrationFormBean = new RegistrationFormBean(request);
        Map<String, String> errors = validator.validate(registrationFormBean);

        assertEquals(LOGIN_IS_INCORRECT, errors.get(ERROR_LOGIN));
    }

    @Test
    public void testRegistrationBeanValidatorWhenPasswordIsNotCorrect() {
        when(request.getParameter(FIRST_NAME)).thenReturn("Alex");
        when(request.getParameter(LAST_NAME)).thenReturn("Alekseev123");
        when(request.getParameter(EMAIL)).thenReturn("alexgamil.com");
        when(request.getParameter(LOGIN)).thenReturn("alex");
        when(request.getParameter(PASSWORD)).thenReturn("111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("111");
        when(request.getParameter(GENDER)).thenReturn("man");

        RegistrationFormBean registrationFormBean = new RegistrationFormBean(request);
        Map<String, String> errors = validator.validate(registrationFormBean);

        assertEquals(THE_TWO_PASSWORDS_DO_NOT_MATCH, errors.get(ERROR_PASSWORD_CONFIRM));
    }

    @Test
    public void testRegistrationBeanValidatorWhenPasswordAndConfirmPasswordAreNotCorrect() {
        when(request.getParameter(FIRST_NAME)).thenReturn("Alex");
        when(request.getParameter(LAST_NAME)).thenReturn("Alekseev123");
        when(request.getParameter(EMAIL)).thenReturn("alexgamil.com");
        when(request.getParameter(LOGIN)).thenReturn("alex");
        when(request.getParameter(PASSWORD)).thenReturn("111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("112");
        when(request.getParameter(GENDER)).thenReturn("man");

        RegistrationFormBean registrationFormBean = new RegistrationFormBean(request);
        Map<String, String> errors = validator.validate(registrationFormBean);

        assertEquals(PASSWORD_IS_INCORRECT, errors.get(ERROR_PASSWORD));
    }

    @Test
    public void testRegistrationBeanValidatorWhenPasswordIsCorrectButConfirmPasswordIsNotCorrect() {
        when(request.getParameter(FIRST_NAME)).thenReturn("Alex");
        when(request.getParameter(LAST_NAME)).thenReturn("Alekseev123");
        when(request.getParameter(EMAIL)).thenReturn("alexgamil.com");
        when(request.getParameter(LOGIN)).thenReturn("alex");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("112");
        when(request.getParameter(GENDER)).thenReturn("man");

        RegistrationFormBean registrationFormBean = new RegistrationFormBean(request);
        Map<String, String> errors = validator.validate(registrationFormBean);

        assertEquals(THE_TWO_PASSWORDS_DO_NOT_MATCH, errors.get(ERROR_PASSWORD_CONFIRM));
    }

    @Test
    public void testRegistrationBeanValidatorWhenGenderIsNotChosen() {
        when(request.getParameter(FIRST_NAME)).thenReturn("Alex");
        when(request.getParameter(LAST_NAME)).thenReturn("Alekseev123");
        when(request.getParameter(EMAIL)).thenReturn("alexgamil.com");
        when(request.getParameter(LOGIN)).thenReturn("alex");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("11111");
        when(request.getParameter(GENDER)).thenReturn("");

        RegistrationFormBean registrationFormBean = new RegistrationFormBean(request);
        Map<String, String> errors = validator.validate(registrationFormBean);

        assertEquals(CHOOSE_GENDER, errors.get(ERROR_RADIO));
    }
}