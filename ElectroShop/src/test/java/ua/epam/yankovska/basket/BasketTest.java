package ua.epam.yankovska.basket;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import ua.epam.yankovska.entity.Product;
import ua.epam.yankovska.storage.impl.BasketStorageImpl;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(MockitoJUnitRunner.class)
public class BasketTest {
    private Product product = new Product();

    @Spy
    private Map<Product, Integer> basket = new HashMap<>();

    @InjectMocks
    private BasketStorageImpl target;

    @Before
    public void setUp() {
        product.setPrice(100);
        product.setProducer("Sony");
        product.setDescription("Beautiful");
        product.setCategory("Computer");
        product.setName("A1234");
    }

    @Test
    public void basketShouldContainElements() {
        target.add(product);

        assertFalse(basket.isEmpty());
        assertEquals(1, target.countProducts());
    }

    @Test
    public void basketShouldContainThreeElements() {
        target.add(product);
        target.add(product);
        target.add(product);

        assertEquals(3, target.countProducts());
    }

    @Test
    public void basketShouldBeEmpty() {
        target.add(product);
        target.add(product);
        target.add(product);
        target.deleteAll();

        assertTrue(basket.isEmpty());
    }

    @Test
    public void basketShouldSetProductAndCount() {
        target.set(product, 4);

        assertEquals(4, target.countProducts());
    }

    @Test
    public void basketShouldGetSumOfPurchase() {
        product.setPrice(100);
        target.set(product, 3);

        assertEquals(300, target.sumOfThePurchase(), 0);
    }

}
