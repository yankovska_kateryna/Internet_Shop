package ua.epam.yankovska.filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.epam.yankovska.strategy.localizationStratagy.LocalizationStoreManager;
import ua.epam.yankovska.strategy.localizationStratagy.LocalizationStoreStrategy;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ua.epam.yankovska.constants.Constant.InitAttributes.STRATEGY_LOCALE;

@RunWith(MockitoJUnitRunner.class)
public class LocalizationFilterTest {

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain;

    @Mock
    private LocalizationStoreStrategy localizationStoreStrategy;

    @Mock
    private FilterConfig filterConfig;

    @Mock
    private ServletContext servletContext;

    @Mock
    private LocalizationStoreManager localizationStoreManager;

    @InjectMocks
    private LocalizationFilter target;

    @Before
    public void setUp() {
        when(filterConfig.getInitParameter("storeStrategy")).thenReturn("session");
        when(filterConfig.getServletContext()).thenReturn(servletContext);
        when(filterConfig.getInitParameter("supported")).thenReturn("en, ru");
        when(filterConfig.getInitParameter("default")).thenReturn("en");
        when(filterConfig.getServletContext().getAttribute(STRATEGY_LOCALE)).thenReturn(localizationStoreManager);
        when(localizationStoreManager.getLocalizationStoreStrategy("session")).thenReturn(localizationStoreStrategy);
        target.init(filterConfig);
    }

    @Test
    public void shouldSetChosenLocale() throws IOException, ServletException {
        when(request.getParameter("lang")).thenReturn("en");

        target.doFilter(request, response, filterChain);

        verify(localizationStoreStrategy).set(new Locale("en"), request, response);
    }

    @Test
    public void shouldSetLocaleFromStorageIfUserChosenUnsupportedLocale() throws IOException, ServletException {
        when(request.getParameter("lang")).thenReturn("ua");
        when(localizationStoreStrategy.get(request)).thenReturn(new Locale("ru"));

        target.doFilter(request, response, filterChain);

        verify(localizationStoreStrategy).set(new Locale("ru"), request, response);
    }

    @Test
    public void shouldSetLocaleFromUserIfUserChosenSupportedLocale() throws IOException, ServletException {
        when(request.getParameter("lang")).thenReturn("ru");
        when(localizationStoreStrategy.get(request)).thenReturn(new Locale("en"));

        target.doFilter(request, response, filterChain);

        verify(localizationStoreStrategy).set(new Locale("en"), request, response);
    }

    @Test
    public void shouldSetDefaultLocale() throws IOException, ServletException {
        ArrayList<Locale> list = new ArrayList<>();
        list.add(new Locale("bg"));
        list.add(new Locale("ua"));
        list.add(new Locale("jp"));
        when(request.getLocales()).thenReturn(Collections.enumeration(list));

        target.doFilter(request, response, filterChain);

        verify(localizationStoreStrategy).set(new Locale("en"), request, response);
    }

    @Test
    public void shouldSetWhatPersonChosenAndThisLocaleIsSupported() throws IOException, ServletException {
        when(request.getParameter("lang")).thenReturn("en");
        when(localizationStoreStrategy.get(request)).thenReturn(new Locale("ru"));

        target.doFilter(request, response, filterChain);

        verify(localizationStoreStrategy).set(new Locale("en"), request, response);
    }
}
