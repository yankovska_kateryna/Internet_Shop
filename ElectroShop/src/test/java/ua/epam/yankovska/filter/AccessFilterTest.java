package ua.epam.yankovska.filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.epam.yankovska.entity.User;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ua.epam.yankovska.constants.Constant.JSPWays.INDEX;

@RunWith(MockitoJUnitRunner.class)
public class AccessFilterTest {
    private final static String PATH = "/WEB-INF/security.xml";

    @InjectMocks
    private AccessFilter target;

    @Mock
    private FilterConfig filterConfig;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain;

    @Mock
    private HttpSession session;

    @Mock
    private ServletContext context;

    @Mock
    private User user;

    @Before
    public void setUp() {
        when(filterConfig.getServletContext()).thenReturn(context);
        when(context.getRealPath(anyString())).thenReturn("src/main/webapp/WEB-INF/security.xml");
        target.init(filterConfig);
    }

    @Test
    public void shouldPageBeNotSecure() throws IOException, ServletException {
        when(request.getRequestURI()).thenReturn("/basket");

        target.doFilter(request, response, filterChain);

        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    public void shouldUserNotBeInSystem() throws IOException, ServletException {
        when(request.getRequestURI()).thenReturn("/admin");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("user")).thenReturn(null);

        target.doFilter(request, response, filterChain);

        verify(response, times(1)).sendRedirect(request.getContextPath() + INDEX);
    }

    @Test
    public void shouldNotGiveAccessToRole() throws IOException, ServletException {
        when(request.getRequestURI()).thenReturn("/admin");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("user")).thenReturn(user);
        when(user.getStatus()).thenReturn("USER");

        target.doFilter(request, response, filterChain);

        verify(response, times(1)).sendRedirect(request.getContextPath() + INDEX);
    }
}
