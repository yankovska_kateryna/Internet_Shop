package ua.epam.yankovska.servlet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.epam.yankovska.captcha.Captcha;
import ua.epam.yankovska.controller.RegistrationServlet;
import ua.epam.yankovska.service.AvatarService;
import ua.epam.yankovska.service.CaptchaService;
import ua.epam.yankovska.service.UserService;
import ua.epam.yankovska.strategy.captchaStrategy.CaptchaStrategy;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ua.epam.yankovska.constants.Constant.JSPWays.REGISTRATION;
import static ua.epam.yankovska.constants.Constant.JSPWays.REGISTRATION_JSP;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationServletTest {

    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String EMAIL = "email";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String PASSWORD_CONFIRM = "password_confirm";
    private static final String GENDER = "gender";
    private static final String CAPTCHA = "captcha";

    @Mock
    private UserService userService;

    @Mock
    private AvatarService avatarService;

    @Mock
    private CaptchaService captchaService;

    @Mock
    private CaptchaStrategy captchaStrategy;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private RequestDispatcher requestDispatcher;

    @Mock
    private HttpSession session;

    @InjectMocks
    private RegistrationServlet target;

    @Before
    public void setUp() {
        when(captchaService.add(any())).thenReturn(1);
        when(request.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);
        when(request.getSession(anyBoolean())).thenReturn(session);
        when(request.getContextPath()).thenReturn("/shop");
    }

    @Test
    public void doGetShouldSetCaptchaIdToStrategy() throws IOException, ServletException {
        target.doGet(request, response);

        verify(captchaStrategy, times(1)).setCaptchaId(request, response, 1);
    }

    @Test
    public void doGetShouldForwardToRegistrationJsp() throws IOException, ServletException {
        target.doGet(request, response);

        verify(request, times(1)).getRequestDispatcher(REGISTRATION_JSP);
        verify(requestDispatcher, times(1)).forward(request, response);
    }

    @Test
    public void doPostShouldNotHaveAnyErrorAndSendRedirectToStoreJsp() throws IOException {
        when(request.getParameter(FIRST_NAME)).thenReturn("alex");
        when(request.getParameter(LAST_NAME)).thenReturn("alekseev");
        when(request.getParameter(EMAIL)).thenReturn("kate@mail.ru");
        when(request.getParameter(LOGIN)).thenReturn("Kate3005");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("11111");
        when(request.getParameter(GENDER)).thenReturn("man");
        when(request.getParameter(CAPTCHA)).thenReturn("1111");
        when(captchaStrategy.getCaptchaId(request)).thenReturn(1);
        when(captchaService.getCaptcha(1)).thenReturn(new Captcha("1111", null, LocalDateTime.now().plusSeconds(600)));

        target.doPost(request, response);

        verify(session, times(1)).setAttribute(anyString(), anyString());
        verify(response, times(1)).sendRedirect(request.getContextPath() + REGISTRATION);
    }

    @Test
    public void doPostShouldHaveError() throws IOException {
        when(request.getParameter(FIRST_NAME)).thenReturn("alex");
        when(request.getParameter(LAST_NAME)).thenReturn("alekseev");
        when(request.getParameter(EMAIL)).thenReturn("katemail.ru");
        when(request.getParameter(LOGIN)).thenReturn("Kate3005");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("11111");
        when(request.getParameter(GENDER)).thenReturn("man");
        when(request.getParameter(CAPTCHA)).thenReturn("1111");
        when(captchaStrategy.getCaptchaId(request)).thenReturn(1);
        when(captchaService.getCaptcha(1)).thenReturn(new Captcha("1111", null, LocalDateTime.now().plusSeconds(600)));

        target.doPost(request, response);

        verify(session, times(2)).setAttribute(anyString(), any());
        verify(response, times(1)).sendRedirect(request.getContextPath() + REGISTRATION);
    }

    @Test
    public void doPostShouldHaveIncorrectCaptchaErrorInValidateCaptcha() throws IOException {
        when(request.getParameter(FIRST_NAME)).thenReturn("alex");
        when(request.getParameter(LAST_NAME)).thenReturn("alekseev");
        when(request.getParameter(EMAIL)).thenReturn("kate@mail.ru");
        when(request.getParameter(LOGIN)).thenReturn("Kate3005");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("11111");
        when(request.getParameter(GENDER)).thenReturn("man");
        when(request.getParameter(CAPTCHA)).thenReturn("111");
        when(captchaStrategy.getCaptchaId(request)).thenReturn(1);
        when(captchaService.getCaptcha(1)).thenReturn(new Captcha("1111", null, LocalDateTime.now().plusSeconds(600)));

        target.doPost(request, response);

        verify(session, times(2)).setAttribute(anyString(), any());
        verify(response, times(1)).sendRedirect(request.getContextPath() + REGISTRATION);
    }

    @Test
    public void doPostShouldHaveTimeIsOutErrorInValidateCaptcha() throws IOException {
        when(request.getParameter(FIRST_NAME)).thenReturn("alex");
        when(request.getParameter(LAST_NAME)).thenReturn("alekseev");
        when(request.getParameter(EMAIL)).thenReturn("kate@mail.ru");
        when(request.getParameter(LOGIN)).thenReturn("Kate3005");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("11111");
        when(request.getParameter(GENDER)).thenReturn("man");
        when(request.getParameter(CAPTCHA)).thenReturn("1111");
        when(captchaStrategy.getCaptchaId(request)).thenReturn(1);
        when(captchaService.getCaptcha(1)).thenReturn(new Captcha("1111", null, LocalDateTime.now().minusHours(1)));

        target.doPost(request, response);

        verify(session, times(2)).setAttribute(anyString(), any());
        verify(response, times(1)).sendRedirect(request.getContextPath() + REGISTRATION);
    }

    @Test
    public void doPostShouldHaveErrorUserWithLoginAndEmail() throws IOException {
        when(request.getParameter(FIRST_NAME)).thenReturn("alex");
        when(request.getParameter(LAST_NAME)).thenReturn("alekseev");
        when(request.getParameter(EMAIL)).thenReturn("kate@mail.ru");
        when(request.getParameter(LOGIN)).thenReturn("andrey1");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("11111");
        when(request.getParameter(GENDER)).thenReturn("man");
        when(request.getParameter(CAPTCHA)).thenReturn("1111");
        when(captchaStrategy.getCaptchaId(request)).thenReturn(1);
        when(captchaService.getCaptcha(1)).thenReturn(new Captcha("1111", null, LocalDateTime.now().plusSeconds(600)));
        when(userService.checkLogin(anyString())).thenReturn(true);
        when(userService.checkEmail(anyString())).thenReturn(true);

        target.doPost(request, response);

        verify(session, times(2)).setAttribute(anyString(), any());
        verify(response, times(1)).sendRedirect(request.getContextPath() + REGISTRATION);
    }

    @Test
    public void doPostShouldHaveUserWithThisLogin() throws IOException {
        when(request.getParameter(FIRST_NAME)).thenReturn("alex");
        when(request.getParameter(LAST_NAME)).thenReturn("alekseev");
        when(request.getParameter(EMAIL)).thenReturn("kate@mail.ru");
        when(request.getParameter(LOGIN)).thenReturn("andrey1");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("11111");
        when(request.getParameter(GENDER)).thenReturn("man");
        when(request.getParameter(CAPTCHA)).thenReturn("1111");
        when(captchaStrategy.getCaptchaId(request)).thenReturn(1);
        when(captchaService.getCaptcha(1)).thenReturn(new Captcha("1111", null, LocalDateTime.now().plusSeconds(600)));
        when(userService.checkLogin(anyString())).thenReturn(true);
        when(userService.checkEmail(anyString())).thenReturn(false);

        target.doPost(request, response);

        verify(session, times(2)).setAttribute(anyString(), any());
        verify(response, times(1)).sendRedirect(request.getContextPath() + REGISTRATION);
    }

    @Test
    public void doPostShouldHaveUserWithThisEmail() throws IOException {
        when(request.getParameter(FIRST_NAME)).thenReturn("alex");
        when(request.getParameter(LAST_NAME)).thenReturn("alekseev");
        when(request.getParameter(EMAIL)).thenReturn("kate@mail.ru");
        when(request.getParameter(LOGIN)).thenReturn("andrey1");
        when(request.getParameter(PASSWORD)).thenReturn("11111");
        when(request.getParameter(PASSWORD_CONFIRM)).thenReturn("11111");
        when(request.getParameter(GENDER)).thenReturn("man");
        when(request.getParameter(CAPTCHA)).thenReturn("1111");
        when(captchaStrategy.getCaptchaId(request)).thenReturn(1);
        when(captchaService.getCaptcha(1)).thenReturn(new Captcha("1111", null, LocalDateTime.now().plusSeconds(600)));
        when(userService.checkLogin(anyString())).thenReturn(false);
        when(userService.checkEmail(anyString())).thenReturn(true);

        target.doPost(request, response);

        verify(session, times(2)).setAttribute(anyString(), any());
        verify(response, times(1)).sendRedirect(request.getContextPath() + REGISTRATION);
    }
}