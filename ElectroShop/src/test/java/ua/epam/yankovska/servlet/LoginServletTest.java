package ua.epam.yankovska.servlet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.epam.yankovska.controller.LoginServlet;
import ua.epam.yankovska.entity.User;
import ua.epam.yankovska.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoginServletTest {
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";

    @Mock
    private User user;

    @Mock
    private UserService userService;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private HttpSession session;

    @InjectMocks
    private LoginServlet target;

    @Before
    public void setUp() {
        when(request.getSession(anyBoolean())).thenReturn(session);
    }

    @Test
    public void doPostShouldHaveNoUser() throws IOException {
        when(request.getParameter(LOGIN)).thenReturn("alex");
        when(request.getParameter(PASSWORD)).thenReturn("12345");
        target.doPost(request, response);

        verify(session, times(1)).setAttribute(anyString(), anyString());
        verify(response, times(1)).sendRedirect(request.getHeader("referer"));
    }

    @Test
    public void doPostShouldHaveCorrectPassword() throws IOException {
        when(request.getParameter(LOGIN)).thenReturn("alex");
        when(request.getParameter(PASSWORD)).thenReturn("12345");
        when(userService.get("alex")).thenReturn(user);
        when(user.getPassword()).thenReturn("12345");

        target.doPost(request, response);

        verify(session, times(1)).setAttribute(anyString(), anyObject());
        verify(response, times(1)).sendRedirect(request.getHeader("referer"));
    }

    @Test
    public void doPostShouldHaveIncorrectPassword() throws IOException {
        when(request.getParameter(LOGIN)).thenReturn("alex");
        when(request.getParameter(PASSWORD)).thenReturn("12345");
        when(userService.get("alex")).thenReturn(user);
        when(user.getPassword()).thenReturn("123456");

        target.doPost(request, response);

        verify(session, times(1)).setAttribute(anyString(), anyString());
        verify(response, times(1)).sendRedirect(request.getHeader("referer"));
    }
}
