package ua.epam.yankovska.servlet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.epam.yankovska.controller.ProductServlet;
import ua.epam.yankovska.entity.Category;
import ua.epam.yankovska.entity.Producer;
import ua.epam.yankovska.entity.Product;
import ua.epam.yankovska.service.CategoryService;
import ua.epam.yankovska.service.ProducerService;
import ua.epam.yankovska.service.ProductService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ua.epam.yankovska.constants.Constant.JSPWays.STORE_JSP;

@RunWith(MockitoJUnitRunner.class)
public class ProductServletTest {

    @Mock
    private Product product;

    @Mock
    private ProductService productService;

    @Mock
    private ProducerService producerService;

    @Mock
    private CategoryService categoryService;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private RequestDispatcher requestDispatcher;

    @InjectMocks
    private ProductServlet target;

    @Test
    public void doGetShouldDisplayAllOrder() throws ServletException, IOException {
        List<Product> productList = mock(List.class);
        when(productService.countAll(anyObject())).thenReturn(6);
        when(productService.getAll(anyObject())).thenReturn(productList);
        when(request.getRequestDispatcher(STORE_JSP)).thenReturn(requestDispatcher);

        target.doGet(request, response);

        verify(request, times(5)).setAttribute(anyString(), anyObject());
        verify(requestDispatcher, times(1)).forward(request, response);
    }
}
