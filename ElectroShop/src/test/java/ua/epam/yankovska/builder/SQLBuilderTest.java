package ua.epam.yankovska.builder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;
import ua.epam.yankovska.bean.impl.ItemsFilterBean;
import ua.epam.yankovska.util.Converter;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(Parameterized.class)
public class SQLBuilderTest {

    @Mock
    private HttpServletRequest request;

    private static final String SQL_GET_ITEMS = "SELECT producers.id, products.name, description, price, categories.name, " +
            "producers.name FROM products INNER JOIN categories ON " +
            "category_id = categories.id INNER JOIN producers ON producer_id = producers.id";
    private static final String SQL_COUNT_ITEMS = "SELECT COUNT(*) AS COUNT FROM products INNER JOIN categories ON " +
            "category_id = categories.id INNER JOIN producers ON producer_id = producers.id";
    private String name;
    private String[] category;
    private String[] producer;
    private String sorter;
    private String productsPerPage;
    private String priceMin;
    private String priceMax;
    private String page;
    private String expectedListQuery;
    private String expectedCountQuery;

    private SQLBuilder sqlBuilder;

    public SQLBuilderTest(String name, String[] category,
                          String[] producer, String priceMin, String priceMax, String sorter,
                          String productsPerPage, String page,
                          String expectedCountQuery, String expectedListQuery) {
        this.name = name;
        this.category = category;
        this.priceMin = priceMin;
        this.priceMax = priceMax;
        this.producer = producer;
        this.sorter = sorter;
        this.productsPerPage = productsPerPage;
        this.page = page;
        this.expectedCountQuery = expectedCountQuery;
        this.expectedListQuery = expectedListQuery;
    }

    @Before
    public void setUp() {
        initMocks(this);
        when(request.getParameter("item_name")).thenReturn(name);
        when(request.getParameterValues("category")).thenReturn(category);
        when(request.getParameter("price-min")).thenReturn(priceMin);
        when(request.getParameter("price-max")).thenReturn(priceMax);
        when(request.getParameter("sorter")).thenReturn(sorter);
        when(request.getParameter("productsPerPage")).thenReturn(productsPerPage);
        when(request.getParameter("page")).thenReturn(page);
        when(request.getParameterValues("producer")).thenReturn(producer);
    }

    @Test
    public void shouldCreateCountQuery() {
        ItemsFilterBean filterBean = new ItemsFilterBean(request, 1, 1000);
        sqlBuilder = new SQLBuilder();

        String category = new Converter().convertIntArrayToString(filterBean.getCategory());
        String producer = new Converter().convertIntArrayToString(filterBean.getProducer());

        String resultQuery = sqlBuilder.select()
                .count()
                .from("products")
                .join("categories", "category_id", "categories.id")
                .join("producers", "producer_id", "producers.id")
                .where()
                .between("price")
                .constraint("products.name", name)
                .in("category_id", category)
                .in("producer_id", producer)
                .build();
        assertEquals(expectedCountQuery, resultQuery);
    }

    @Test
    public void shouldCreateFilterItemsListQuery() {
        ItemsFilterBean filterBean = new ItemsFilterBean(request, 1, 1000);
        sqlBuilder = new SQLBuilder();

        String category = new Converter().convertIntArrayToString(filterBean.getCategory());
        String producer = new Converter().convertIntArrayToString(filterBean.getProducer());
        String sort = new Converter().convertSort(filterBean.getSorter());

        String resultQuery = sqlBuilder.select("producers.id", "products.name",
                "description", "price", "categories.name", "producers.name")
                .from("products")
                .join("categories", "category_id", "categories.id")
                .join("producers", "producer_id", "producers.id")
                .where()
                .between("price")
                .constraint("products.name", name)
                .in("category_id", category)
                .in("producer_id", producer)
                .orderBy(sort)
                .limit()
                .offset()
                .build();
        assertEquals(expectedListQuery, resultQuery);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"", null, null, "", "", "", "", "",
                        SQL_COUNT_ITEMS + " WHERE price BETWEEN ? AND ?",
                        SQL_GET_ITEMS + " WHERE price BETWEEN ? AND ? ORDER BY products.name ASC LIMIT ? OFFSET ?"
                },
                {"Model1", null, null, "", "", "", "", "",
                        SQL_COUNT_ITEMS + " WHERE price BETWEEN ? AND ? AND products.name = ?",
                        SQL_GET_ITEMS + " WHERE price BETWEEN ? AND ? AND products.name = ? ORDER BY products.name ASC LIMIT ? OFFSET ?"
                },
                {"", null, null, "123", "150", "", "", "",
                        SQL_COUNT_ITEMS + " WHERE price BETWEEN ? AND ?",
                        SQL_GET_ITEMS + " WHERE price BETWEEN ? AND ? ORDER BY products.name ASC LIMIT ? OFFSET ?"
                },
                {"", null, null, "", "", "", "3", "",
                        SQL_COUNT_ITEMS + " WHERE price BETWEEN ? AND ?",
                        SQL_GET_ITEMS + " WHERE price BETWEEN ? AND ? ORDER BY products.name ASC LIMIT ? OFFSET ?"
                },
                {"", new String[]{"1", "2"}, null, "", "", "", "", "",
                        SQL_COUNT_ITEMS + " WHERE price BETWEEN ? AND ? AND category_id IN (1, 2)",
                        SQL_GET_ITEMS + " WHERE price BETWEEN ? AND ? AND category_id IN (1, 2) ORDER BY products.name ASC LIMIT ? OFFSET ?"
                },
                {"", null, new String[]{"1", "2"}, "", "", "", "", "",
                        SQL_COUNT_ITEMS + " WHERE price BETWEEN ? AND ? AND producer_id IN (1, 2)",
                        SQL_GET_ITEMS + " WHERE price BETWEEN ? AND ? AND producer_id IN (1, 2) ORDER BY products.name ASC LIMIT ? OFFSET ?"
                },
                {"", null, null, "", "", "3", "", "",
                        SQL_COUNT_ITEMS + " WHERE price BETWEEN ? AND ?",
                        SQL_GET_ITEMS + " WHERE price BETWEEN ? AND ? ORDER BY products.price DESC LIMIT ? OFFSET ?"
                },
                {"Model2", new String[]{"2", "3"}, new String[]{"3"}, "230", "500", "1", "4", "",
                        SQL_COUNT_ITEMS + " WHERE price BETWEEN ? AND ? AND products.name = ? AND category_id IN (2, 3)" +
                                " AND producer_id IN (3)",
                        SQL_GET_ITEMS + " WHERE price BETWEEN ? AND ? AND products.name = ? AND category_id IN (2, 3) " +
                                "AND producer_id IN (3) ORDER BY products.name DESC LIMIT ? OFFSET ?"
                }
        });
    }
}
