CREATE DATABASE electroShop COLLATE utf8_general_ci;
USE electroShop;

CREATE TABLE users (
	id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    surname VARCHAR(30) NOT NULL,
    gender boolean NOT NULL,
    email VARCHAR(30) NOT NULL UNIQUE,
    login VARCHAR(30) NOT NULL UNIQUE,
    password VARCHAR(200) NOT NULL,
    status ENUM('USER', 'ADMIN') NOT NULL
);

CREATE TABLE categories (
	id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL UNIQUE
);

CREATE TABLE producers (
	id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL UNIQUE
);

CREATE TABLE products (
	id INT PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(30) NOT NULL UNIQUE,
    description VARCHAR(30) NOT NULL,
    price DOUBLE NOT NULL,
    category_id INT NOT NULL,
    producer_id INT NOT NULL,

	FOREIGN KEY (category_id) REFERENCES categories(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (producer_id) REFERENCES producers(id)
    ON DELETE CASCADE ON UPDATE CASCADE
);

insert into categories
	values(default, 'Computer'),
    (default, 'Printer'),
    (default, 'Laptop');

insert into producers
	values(default, 'LG'),
    (default, 'Samsung'),
    (default, 'Canon');

insert into products
	values(default, 'Model1', 'Good computer', 127, 1, 1),
    (default, 'Model2', 'Good computer', 187, 1, 2),
    (default, 'Model3', 'Good printer', 34, 2, 3),
    (default, 'Model4', 'Good printer', 120, 2, 1),
    (default, 'Model5', 'Good laptop', 120, 3, 2),
    (default, 'Model6', 'Good laptop', 120, 3, 1),
    (default, 'Model7', 'Good printer', 120, 2, 3);

CREATE TABLE orders (
	id INT PRIMARY KEY AUTO_INCREMENT,
    status ENUM('ACCEPTED', 'CANCELED', 'FINISHED', 'SENT') NOT NULL,
    details VARCHAR(300) NOT NULL,
    orderDate DATETIME NOT NULL,
    address VARCHAR(100) NOT NULL,
    payment VARCHAR(4) NOT NULL,
    user_id INT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

create table orders_products (
	order_id INT NOT NULL,
    FOREIGN KEY (order_id) REFERENCES orders(id),
    product_id INT NOT NULL,
    FOREIGN KEY (product_id) REFERENCES products(id),
    count INT NOT NULL,
    price DOUBLE NOT NULL
);

insert into users
	values(default, 'Admin', 'Admin', 1, 'admin@gmail.com', 'admin', 12345, 'ADMIN');